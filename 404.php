<?php get_header(); ?>
<section class="full main-404">
	<div class="wrapper-main center">
		<h2>error 404</h2>
		<h3>La página que <br> buscas no existe</h3>
		<a href="/" class="btn-yokomotor">VOLVER AL INICIO</a>		
	</div>
</section>

<?php get_footer(); ?>
