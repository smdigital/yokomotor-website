<?php get_header(); ?>

<section class="banner-general full  clear-fix">
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-cotizador.jpg" alt="">
	</figure>
</section>
<section class="main-cita-taller full clear-fix main-cotizador">
	<div class="wrapper-main-sm center">
		<h1>Cotizador</h1>		
		<div class="leyeng-cotizador">
			<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Officiis, odio a, esse possimus facere, quis alias, et suscipit <br>recusandae dicta aperiam porro libero delectus officia totam impedit itaque obcaecati adipisci?</p>
		</div>
		<section id="tabs-gral-cotizador" class="form-yokomotor full clear-fix">
			<ul class="step-cotizador">
				<!-- La clase para el bullet es .active -->
				<li><a href="#step-1">1</a></li>
				<li><a href="#step-2">2</a></li>
			</ul>
			<!-- PASO 1 -->
			<div id="step-1" class="row-step-cotizador">				
	            <div class="row row-xs main-form-fieldset">
					<div class="col-12 col-sm-12 col-lg-12 col-xl-12">
		            	<div class="row">                	
			                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
			                	<fieldset>
					                <legend>Nombre <span>*</span></legend>
					                <input type="text" id="" name="">
					            </fieldset>
			                </div>
			                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
			                	<fieldset>
					                <legend>Apellido <span>*</span></legend>
					                <input type="text" id="" name="">
					            </fieldset>
			                </div>
			                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
								<fieldset>
					                <legend>Celular <span>*</span></legend>
					                <input type="tel" id="" name="">
					            </fieldset>
			                </div>
		                </div>
	            	</div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Tipo documento <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">CC</option>
			                	<option value="">NIT</option>
			                	<option value="">CE</option>
			                	<option value="">PA</option>
			                	<option value="">TI</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Número de Documento <span>*</span></legend>
			                <input type="number" id="" name="">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
						<fieldset>
			                <legend>Correo electrónico <span>*</span></legend>
			                <input type="email" id="" name="">
			            </fieldset>
	                </div>                         	
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Modelo <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Versión <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Ciudad <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Concesionario <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
						<fieldset>
			                <legend>¿Desea cotizar seguro? <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">Si</option>
			                	<option value="">No</option>
			                </select>
			            </fieldset>
	                </div>
	                <!-- Condicional para seguro -->
	                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
		            	<div class="row">                	
			                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
			                	<fieldset>
					                <legend>Fecha de Nacimiento <span>*</span></legend>
					                <input type="date" id="" name="">
					            </fieldset>
			                </div>
			                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
			                	<fieldset>
				                <legend>Género <span>*</span></legend>
				                <select name="" id="">
				                	<option value="">Selecciona</option>
				                	<option value="">Femenino</option>
				                	<option value="">Masculino</option>
				                </select>
				            </fieldset>
			                </div>
			                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
								<fieldset>
					                <legend>Ciudad de circulacion <span>*</span></legend>
					                <select name="" id="">
					                	<option value="">Selecciona</option>
					                	<option value="">Femenino</option>
					                	<option value="">Masculino</option>
					                </select>
					            </fieldset>
			                </div>
		                </div>
	            	</div>
	            </div>	           
	            <div class="clr"></div>
	            <!-- <input type="submit" value="Siguiente" disabled=""> -->
	            <a href="javascript:;" class="btn-siguiente">Siguiente</a>
			</div>
			<!-- PASO 2 -->
			<div id="step-2" class="row-step-cotizador">
				<h3 class="title-step-2">SIMULADOR DE CRÉDITO</h3>
				<div class="row row-xs main-form-fieldset">
					<div class="col-12 col-sm-12 col-lg-6 col-xl-6">
		            	<div class="row">                	
			                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
			                	<fieldset>
					                <legend>¿Desea financiar el vehiculo?</legend>
					                <select name="" id="">
					                	<option value="">Selecciona</option>
					                	<option value="">Si</option>
					                	<option value="">No</option>		                	
					                </select>
					            </fieldset>
			                </div>
			                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
			                	<fieldset>
					                <legend>Modelo</legend>
					                <input type="text" id="" disabled="" name="" placeholder="LAND CRUISER">
					            </fieldset>
			                </div>
			                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
								<fieldset>
					                <legend>Versión</legend>
					                <input type="text" id="" disabled="" name="" placeholder="LAND CRUISER 79">
					            </fieldset>
			                </div>
		                </div>
	            	</div>
	            	<div class="col-12 col-sm-12 col-lg-6 col-xl-6">
	            		<figure class="car-cotizador">
	            			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/cotizador/LAND-CRUISER-79.jpg" alt="">
	            		</figure>
	            	</div>
	            </div>
	            <!-- Card Cotizador -->
	            <div class="card-cotizador">
	            	<h4>DESDE: <span>$195.300.000 </span></h4>
	            	<div id="tabs-cotizador" class="wrap-tabs-cotizador">
	            		<ul class="title-tabs-cotizador">
					    	<li>
					    		<a href="#plan-renueve">PLAN RENUEVE</a>
					    	</li>
					    	<li>
					    		<a href="#plan-tradicional">PLAN TRADICIONAL</a>
					    	</li>					    
					  	</ul>
					  	<!-- Tab Plan Renueve -->
					  	<div id="plan-renueve" class="content-tabs-cotizador">
					  		<h2>ESCOGE EL ESCENARIO MÁS CÓMODO PARA TI</h2>
						  	<div class="content-range clear-fix">
							  	<div class="row-range">
									<div class="range-slide">
										<h5>CUOTA INICIAL</h5>
										<h6>$19.530.000</h6>
										<div class="clr"></div>
										<h3>10%</h3>
										<h4>50%</h4>
										<div class="slider-range"></div>
									</div>				
								</div>					  		
						  	</div>
						  	<div class="content-range clear-fix">
							  	<div class="row-range">
									<div class="range-slide">
										<h5>PLAZO EN MESES</h5>
										<div class="clr"></div>
										<h3>24</h3>
										<h4>36</h4>
										<div class="slider-range"></div>
									</div>				
								</div>					  		
						  	</div>
						  	<div class="content-range clear-fix">
							  	<div class="row-range">
									<div class="range-slide">
										<h5>CUOTA FINAL</h5>
										<h6>$19.530.000</h6>
										<div class="clr"></div>
										<h3>10%</h3>
										<h4>50%</h4>
										<div class="slider-range"></div>
									</div>				
								</div>					  		
						  	</div>
					  		<div class="clr"></div>
						  	<div class="col-cuota cuota-mensual">
						  		<h5>CUOTA MENSUAL DESDE:</h5>
						  		<h6>$7.539.354</h6>
						  	</div>
						  	<div class="col-cuota cuota-final">
						  		<h5>CUOTA FINAL DESDE:</h5>
						  		<h6>$19.530.000</h6>
						  	</div>	
					  	</div>
					  	<!-- Tab Plan Tradicional -->
					  	<div id="plan-tradicional" class="content-tabs-cotizador">
					  		<h2>ESCOGE EL ESCENARIO MÁS CÓMODO PARA TI</h2>
						  	<div class="content-range clear-fix">
							  	<div class="row-range">
									<div class="range-slide">
										<h5>CUOTA INICIAL</h5>
										<h6>$19.530.000</h6>
										<div class="clr"></div>
										<h3>10%</h3>
										<h4>80%</h4>
										<div class="slider-range"></div>
									</div>				
								</div>					  		
						  	</div>
						  	<div class="content-range clear-fix">
							  	<div class="row-range">
									<div class="range-slide">
										<h5>PLAZO EN MESES</h5>
										<div class="clr"></div>
										<h3>24</h3>
										<h4>72</h4>
										<div class="slider-range"></div>
									</div>				
								</div>					  		
						  	</div>						  	
					  		<div class="clr"></div>
						  	<div class="col-cuota cuota-mensual">
						  		<h5>CUOTA MENSUAL DESDE:</h5>
						  		<h6>$8.264.258</h6>
						  	</div>						  	
						</div>	
	            	</div>
	            </div>
	            <a href="javascript:;" class="volver-step">Volver</a>
	            <div class="clr"></div>
	            <div class="checked-style checked-red">
	                <label for="acepto-terms-cotizador">
	                    <input type="checkbox" id="acepto-terms-cotizador">
	                    <div class="wpcf7-list-item-label">
		                    <div class="check-label"></div>
		                    Autorizo el manejo de mis datos personales <br><a href="https://yokomotor.com.co/toyota/politica-de-tratamiento-de-datos/" target="_blank">Acepto Política de tratamiento de datos</a>
	                    </div>
	                </label>
	            </div>
	            <div class="clr"></div>
	            <input type="submit" value="ENVIAR">
			</div>	
		</section>
	</div>
</section>
<?php get_footer(); ?>

<script>
	// https://jqueryui.com/slider/#rangemin
  	jQuery( function() {
    	jQuery( ".slider-range" ).slider({
      		range: "min",
      		min: 1,
      		max: 50,
      		value: 10,
	     	slide: function( event, ui ) {
	        	jQuery( "#amount" ).val( "$" + ui.value );
	     	}
	    });
    	jQuery( "#amount" ).val( "$" + $( "#slider-range-min" ).slider( "value" ) );
  	});
  </script>