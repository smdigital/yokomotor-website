<?php get_header(); ?>

<section class="banner-general full  clear-fix">
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-cotizador.jpg" alt="">
	</figure>
</section>
<section class="main-cita-taller full clear-fix main-cotizador main-simulador">
	<div class="wrapper-main-sm center">
		<h1>¿ESTÁS VENDIENDO TU AUTO USADO?</h1>
		<div class="leyeng-cotizador">
			<h2>¡NOSOTROS TE LO COMPRAMOS!</h2>		
			<p>Es muy fácil y todo de una manera ágil, porque para nosotros lo más <br> importante es tu tranquilidad durante todo el camino.</p>
		</div>
		<section id="tabs-gral-cotizador" class="form-yokomotor full clear-fix">
			<ul class="step-simulator-tabs clear-fix">
				<li>
					<a href="#step-1">
						<i>1</i>
						<span>Datos personales</span>
					</a>
				</li>
				<li>
					<a href="#step-2">
						<i>2</i>
						<span>Información de tu auto</span>
					</a>
				</li>
				<li>
					<a href="#step-3">
						<i>3</i>
						<span>Fotos de tu auto</span>
					</a>
				</li>
			</ul>
			<!-- PASO 1 -->
			<div id="step-1" class="row-step-cotizador">	
				<h2>Diligencia cada uno de los datos que te solicitamos:</h2>			
	            <div class="row row-xs main-form-fieldset">
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Número de cédula <span>*</span></legend>
			                <input type="number" id="" name="">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Tu nombre completo <span>*</span></legend>
			                <input type="text" id="" name="">
			            </fieldset>
	                </div> 
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Email <span>*</span></legend>
			                <input type="email" id="" name="">
			            </fieldset>
	                </div> 
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Celular <span>*</span></legend>
			                <input type="tel" id="" name="">
			            </fieldset>
	                </div>                         	
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>¿Quieres que te recojamos el auto? <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">Si</option>
			                	<option value="">No</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>¿Dónde quieres ser atendido? <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	            </div>	     
	            <div class="checked-style checked-red">
	                <label for="acepto-WhatsApp">
	                    <input type="checkbox" id="acepto-WhatsApp">
	                    <div class="wpcf7-list-item-label">
		                    <div class="check-label"></div>
		                   Aceptas comunicación por WhatsApp
	                    </div>
	                </label>
	            </div>      
	            <div class="clr"></div>
	            <a href="javascript:;" class="btn-siguiente">Siguiente</a>
			</div>
			<!-- PASO 2 -->
			<div id="step-2" class="row-step-cotizador step-simulador-2">
				<h2>Cuéntanos sobre tu vehículo.</h2>
				<div class="row row-xs main-form-fieldset">
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>¿Cuál es la marca de tu vehículo? <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Seleccione una opción</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Línea de tu vehículo <span>*</span></legend>
			                <select name="" id="">
			                	<option value="">Selecciona</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                	<option value="">-------</option>
			                </select>
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Modelo de tu vehículo <span>*</span></legend>
			                <input type="number" id="" name="">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend>Kilometraje de tu vehículo <span>*</span></legend>
			                <input type="number" id="" name="">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
						<fieldset>
			                <legend>De qué ciudad es la placa de tu vehículo <span>*</span></legend>
			                <input type="text" id="" name="">
			            </fieldset>
	                </div>
	            </div>
	            <div class="clr"></div>
	            <a href="javascript:;" class="volver-step">Volver</a>
	            <a href="javascript:;" class="btn-siguiente">Siguiente</a>
			</div>
			<!-- PASO 3 -->
			<div id="step-3" class="row-step-cotizador step-simulador-3">
				<h2>Envíanos fotos de tu vehículo y cuentanos <br>sobre el estado en qué se encuentra</h2>
				<h3>Sube las fotos de tu vehículo (opcional)</h3>
				<div class="row row-xs main-form-fieldset">
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend><strong>Frente</strong></legend>
			                <input type="file">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend><strong>Lateral</strong></legend>
			                <input type="file">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend><strong>Trasera</strong></legend>
			                <input type="file">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend><strong>Interior</strong></legend>
			                <input type="file">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend><strong>Tablero</strong></legend>
			                <input type="file">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<fieldset>
			                <legend><strong>Motor</strong></legend>
			                <input type="file">
			            </fieldset>
	                </div>
	                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
						<fieldset>
			                <legend><strong>Tu mensaje</strong></legend>
			                <textarea name="" id="" placeholder="Incluye estado, características..."></textarea>
			            </fieldset>
	                </div>
	            </div>
	            <div class="clr"></div>
	            <div class="checked-style checked-red">
	                <label for="acepto-terms-cotizador">
	                    <input type="checkbox" id="acepto-terms-cotizador">
	                    <div class="wpcf7-list-item-label">
		                    <div class="check-label"></div>
		                    Autorizo el manejo de mis datos personales <a href="https://yokomotor.com.co/toyota/politica-de-tratamiento-de-datos/" target="_blank">Acepto Política de tratamiento de datos</a>
	                    </div>
	                </label>
	            </div>      
	            <div class="clr"></div>
	            <a href="javascript:;" class="volver-step">Volver</a>
	            <input type="submit" value="Enviar">
			</div>	
		</section>
	</div>
</section>
<?php get_footer(); ?>

<script>
	// https://jqueryui.com/slider/#rangemin
  	jQuery( function() {
    	jQuery( ".slider-range" ).slider({
      		range: "min",
      		min: 1,
      		max: 50,
      		value: 10,
	     	slide: function( event, ui ) {
	        	jQuery( "#amount" ).val( "$" + ui.value );
	     	}
	    });
    	jQuery( "#amount" ).val( "$" + $( "#slider-range-min" ).slider( "value" ) );
  	});
  </script>