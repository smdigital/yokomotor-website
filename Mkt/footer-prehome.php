		
		<div class="share-icon-yokomotor animate__animated animate__fadeInDown animate__delay-2s">
			<ul class="menu-iconos-flotantes">
				<li class="icon-ws">
					<a href="https://api.whatsapp.com/send?phone=573102648638&text=Hola Yokomotor" target="_blank">
						<span>Whatsapp</span>
						<i></i>
					</a>
				</li>
				<li class="icon-ct">
					<a href="#">
						<span>Cita taller</span>
						<i></i>
					</a>
				</li>
				<li class="icon-td">
					<a href="#">
						<span>Test Drive</span>
						<i></i>
					</a>
				</li>
			</ul>
			<div id="icon-share-mns" class="share-fixed"></div>
		</div>


		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
