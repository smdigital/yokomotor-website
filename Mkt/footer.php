		<footer class="full clear-fix">
			<div class="wrapper-main center">
				<article class="col-nosotros">
					<h2>NOSOTROS</h2>
					<ul>
						<li><a href="">Modelos</a></li>
						<li><a href="">Autómoviles</a></li>
						<li><a href="">SUV</a></li>
						<li><a href="">Camionetas</a></li>
						<li><a href="">Pick ups</a></li>
						<li><a href="">Utilitarios</a></li>		
					</ul>
				</article>
				<article class="col-sedes">
					<h2>SEDES</h2>
					<div id="tabs-sedes" class="sedes-tabs">
						<ul>
							<li><a href="#medellin">Medellín</a></li>
							<li><a href="#bogota">Bogotá</a></li>
						</ul>
						<!-- Tab Medellín -->
						<div id="medellin" class="content-tabs">
							<!-- Accordion Medellín -->
							<div class="sedes-accordion">
								<h3>Palacé</h3>
								<div class="content-accordion">
									Medellín accordion 1
								</div>
								<h3>Guayabal</h3>
								<div class="content-accordion">
									Medellín accordion 2
								</div>
								<h3>CC El Tesoro</h3>
								<div class="content-accordion">
									Medellín accordion 3
								</div>
							</div>
						</div>
						<!-- Tab Bogotá -->
						<div id="bogota" class="content-tabs">
							<!-- Accordion Bogotá -->
							<div class="sedes-accordion">
								<h3>Calle 134</h3>
								<div class="content-accordion">
									Bogotá accordion 1
								</div>
								<h3>Calle 72</h3>
								<div class="content-accordion">
									Bogotá accordion 2
								</div>
								<h3>Hino Fontibón</h3>
								<div class="content-accordion">
									Bogotá accordion 3
								</div>
							</div>
						</div>
					</div>
				</article>
				<article class="col-suscribete">
					<h2>SUSCRÍBETE</h2>
					<div class="cont-input">
						<input type="text" class="icon-usuario" placeholder="NOMBRE Y APELLIDO*">
					</div>
					<div class="cont-input">
						<input type="email" class="icon-email" placeholder="EMAIL*">
					</div>
					<div class="checked-style">
                        <label for="acepto-terms">
                            <input type="checkbox" id="acepto-terms">
                            <div class="check-label"></div>
                            Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>

                        </label>
                    </div>
					<input type="submit" value="ENVIAR">
				</article>
				<article class="col-mapa">
					<h4><a href="">TRABAJA CON <br> NOSOTROS</a></h4>
					<h4><a href="">MAPA DEL SITIO</a></h4>
					<div class="redes">
            			<ul>
            				<li class="icon-fb"><a href="" target="_blank">facebook</a></li>
            				<li class="icon-tw"><a href="" target="_blank">twitter</a></li>
            				<li class="icon-it"><a href="" target="_blank">instagram</a></li>
            				<li class="icon-yt"><a href="" target="_blank">youtube</a></li>
            			</ul>
            		</div>
				</article>
				
			</div>
			<div class="col-terms full clear-fix">
				<ul>
					<li><a href="">Términos y Condiciones </a></li>
					<li><a href="">Políticas de Privacidad</a></li>
					<li><a href="">Política de seguridad SGCS</a></li>
				</ul>
			</div>
			<h6>© 2021 Yokomotor Todos los Derechos Reservados</h6>
		</footer>
		<div class="share-icon-yokomotor animate__animated animate__fadeInDown animate__delay-2s">
			<ul class="menu-iconos-flotantes">
				<li class="icon-ws">
					<a href="https://api.whatsapp.com/send?phone=573102648638&text=Hola Yokomotor" target="_blank">
						<span>Whatsapp</span>
						<i></i>
					</a>
				</li>
				<li class="icon-ct">
					<a href="#">
						<span>Cita taller</span>
						<i></i>
					</a>
				</li>
				<li class="icon-td">
					<a href="#">
						<span>Test Drive</span>
						<i></i>
					</a>
				</li>
			</ul>
			<div id="icon-share-mns" class="share-fixed"></div>
		</div>


		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->
