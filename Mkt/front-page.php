<?php get_header(); ?>
	<!-- Banner Desktop -->
    <section class="banner-home clear-fix">
		<div class="swiper swiper-banner-home">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<figure>
		        		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-home-1.jpg" alt=""> 
					</figure>
					<div class="wrap-banner">
						<article class="animation-caption delay-1">
							<h1>Nueva toyota rav4</h1>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Culpa inventore iste earum.</p>
							<a href="" class="btn-yokomotor-arrow">CTA</a>
						</article>
					</div>
				</div>
				<div class="swiper-slide">
					<figure>
		        		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-home-2.jpg" alt=""> 
					</figure>
					<div class="wrap-banner">
						<article class="animation-caption delay-1">
							<h1>toyota en el camino</h1>
							<p>Lorem ipsum dolor sit amet elit. Culpa inventore iste earum.</p>
							<a href="" class="btn-yokomotor-arrow">CTA</a>
						</article>
					</div>
				</div>
				<div class="swiper-slide">
					<figure>
		        		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-home-3.jpg" alt=""> 
					</figure>
					<div class="wrap-banner">
						<article class="animation-caption delay-1">
							<h1>Toyota hilux 2021</h1>
							<p>Lorem ipsum dolor sit amet elit. Culpa inventore iste earum.</p>
							<a href="" class="btn-yokomotor-arrow">CTA</a>
						</article>
					</div>
				</div>
			</div>
		</div>
		<div class="next-banner button-next next-white"></div>
      	<div class="prev-banner button-prev prev-white"></div>
		<div class="swiper-pagination banner-pagination"></div>
    </section>

    <section class="main-home full clear-fix">
    	<div class="wrapper-main center">
	    	<section class="main-information full clear-fix">
				<div class="row row-xs">
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-information">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-1.jpg" alt="">
								</figure>
								<div class="overflow">
									<h2>Lorem ipsum, dolor sit amet.</h2>
									<div class="btn-yokomotor-arrow">CTA</div>
								</div>
							</a>
						</article>
					</div>
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-information">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-2.jpg" alt="">
								</figure>
								<div class="overflow">
									<h2>Lorem ipsum</h2>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus</p>
									<div class="btn-yokomotor-arrow">CTA</div>
								</div>
							</a>
						</article>
					</div>
				</div>
			</section>
			<section class="main-campaign full clear-fix pt-5 pt-2-xs">
				<h2>CAMPAÑAS</h2>
				<div class="row row-xs">
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-information campaign-small">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-3.jpg" alt="">
								</figure>
								<div class="overflow">
									<h3>Lorem ipsum</h3>
									<p>Lorem ipsum dolor sit amet consectetur.</p>
									<div class="btn-yokomotor-arrow">CTA</div>
								</div>
							</a>
						</article>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-information campaign-small">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-4.jpg" alt="">
								</figure>
								<div class="overflow">
									<h3>Lorem ipsum</h3>
									<p>Lorem ipsum dolor sit amet consectetur.</p>
									<div class="btn-yokomotor-arrow">CTA</div>
								</div>
							</a>
						</article>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-information campaign-small">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-5.jpg" alt="">
								</figure>
								<div class="overflow">
									<h3>Lorem ipsum</h3>
									<p>Lorem ipsum dolor sit amet consectetur.</p>
									<div class="btn-yokomotor-arrow">CTA</div>
								</div>
							</a>
						</article>
					</div>
				</div>				
			</section>
			<section class="main-car full clear-fix relative pt-5 pt-2-xs">
				<div class="swiper swiper-car-full">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<article class="card_car-details">
								<figure class="animation-car delay-0">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/car-rav-4.jpg" alt="">
								</figure>
								<h2>RAV4</h2>
								<a href="" class="btn-yokomotor-arrow">CTA</a>
								<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Itaque quia facilis inventore enim tenetur magni maiores.</p>
							</article>
						</div>
						<div class="swiper-slide">
							<article class="card_car-details">
								<figure class="animation-car delay-0">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/car-rav-4.jpg" alt="">
								</figure>
								<h2>RAV4</h2>
								<a href="" class="btn-yokomotor-arrow">CTA</a>
								<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Itaque quia facilis inventore enim tenetur magni maiores.</p>
							</article>
						</div>
						<div class="swiper-slide">
							<article class="card_car-details">
								<figure class="animation-car delay-0">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/car-rav-4.jpg" alt="">
								</figure>
								<h2>RAV4</h2>
								<a href="" class="btn-yokomotor-arrow">CTA</a>
								<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Itaque quia facilis inventore enim tenetur magni maiores.</p>
							</article>
						</div>
					</div>
				</div>
				<div class="next-car button-next next-black"></div>
      			<div class="prev-car button-prev prev-black"></div>
				<div class="pagination-square pagination-car swiper-pagination"></div>
			</section>
    	</div>
    </section>
    <section class="main-blog-home full clear-fix pt-3-xs">
    	<div class="wrapper-main center">
    		<h2>BLOG</h2>
    		<div class="row row-xs">
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-blog" 
					data-aos="fade-up" 
					data-aos-duration="600" 
					data-aos-delay="300">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-1.jpg" alt="">
							</a>
						</figure>
						<div class="details-blog">
							<h4>Lorem</h4>
							<h3>Lorem ipsum dolor sit amet, consectetur</h3>
							<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
							<a href="" class="btn-yokomotor-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-blog"
					data-aos="fade-up" 
					data-aos-duration="600" 
					data-aos-delay="500">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-2.jpg" alt="">
							</a>
						</figure>
						<div class="details-blog">
							<h4>Lorem</h4>
							<h3>Lorem ipsum dolor sit amet, consectetur</h3>
							<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse sint cupiditate dolorem esse.</p>
							<a href="" class="btn-yokomotor-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-blog"
					data-aos="fade-up" 
					data-aos-duration="600" 
					data-aos-delay="700">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-3.jpg" alt="">
							</a>
						</figure>
						<div class="details-blog">
							<h4>Lorem</h4>
							<h3>Lorem ipsum dolor sit amet, consectetur</h3>
							<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
							<a href="" class="btn-yokomotor-light">CTA</a>
						</div>
					</article>
				</div>
			</div>
			<a href="" class="btn-yokomotor">CTA</a>
    	</div>
    </section>

<?php get_footer(); ?>