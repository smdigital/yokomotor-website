<?php get_header(); ?>
<div class="bar-return">
	<div class="wrapper-main center">
		<a href="" class="btn-return">Regresar</a>
	</div>
</div>

<div class="banner-accesorios-detailed wrapper-main center">
	<h1>H1 Title headins HILUX</h1>
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/banner-partes.jpg" alt="">
	</figure>
</div>
<section class="accesorios-detailed-shop full clear-fix">
	<div class="wrapper-main center">
		<h2>H2 Title headins cropisa ipsum dolor sit amet</h2>
		<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam</p>
		<h3>H3 Title headins cropisa ipsum dolor sit amet</h3>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing, elit. Eligendi esse labore in modi, dolor perspiciatis corrupti numquam minima aperiam illum deserunt nesciunt minus alias eius inventore impedit voluptates veniam ipsa?</p>
		<h4>H4 Title headins cropisa ipsum dolor sit amet</h4>
		<p>Lorem ipsum dolor, sit amet consectetur, adipisicing elit. Explicabo voluptatem adipisci officiis non laborum? Laboriosam quas labore enim perspiciatis reiciendis, quod porro doloribus veniam id commodi officia, provident qui optio! </p>
		<a href="" class="btn-whatsapp">PREGUNTAR</a>
		
	</div>
</section>



<?php get_footer(); ?>