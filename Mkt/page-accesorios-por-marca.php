<?php get_header(); ?>
<section class="banner-accesorios-marca full clear-fix">
	<figure>
		<!-- Las clases Images Desktop -->
		<img class="hidden-xs" src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorio-hilux.jpg" alt="">
		<!-- Las clases Images Movile -->
		<img class="hidden-lg" src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorio-hilux-xs.jpg" alt="">
	</figure>
</section>
<section class="main-accesorios-marcas full clear-fix">
	<div class="wrapper-main center">
		<hr>
		<h2>accesorios</h2>
		<h1>Hilux</h1>
		<div class="clr"></div>
		<div class="row-card-5">
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-1.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-2.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-3.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-4.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-5.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-6.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-4.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-3.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-2.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			<!-- Item --> 
			<div class="col-cards">
				<article class="card-marca-accesorios">
					<figure>
						<a href="accesorios-detalle">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/accesorios/partes-1.png" alt="">
						</a>
					</figure>
					<h6>Lorem ipsum</h6>
					<div class="figcaption">
						<a href="accesorios-detalle" class="btn-arrow-light">Ver más</a>
					</div>
				</article>
			</div>
			
			
		</div>
		
	</div>	
</section>


<?php get_footer(); ?>