<?php get_header(); ?>
<div class="bar-return">
	<div class="wrapper-main center">
		<a href="" class="btn-return">Regresar</a>
	</div>
</div>
<!-- Banner Desktop -->
<section class="banner-home main-banner-accesorios clear-fix">
	<div class="swiper swiper-banner-accesorio">
		<div class="swiper-wrapper">
			<div class="swiper-slide">
				<figure>
	        		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-accesorios.jpg" alt=""> 
				</figure>
				<div class="wrap-banner">
					<article class="animation-caption delay-1">
						<h1>Accesorios</h1>
					</article>
				</div>
			</div>
			<div class="swiper-slide">
				<figure>
	        		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-accesorios-2.jpg" alt=""> 
				</figure>
				<div class="wrap-banner">
					<article class="animation-caption delay-1">
						<h1>NUEVOS Accesorios</h1>
					</article>
				</div>
			</div>
			<div class="swiper-slide">
				<figure>
	        		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-accesorios-3.jpg" alt=""> 
				</figure>
				<div class="wrap-banner">
					<article class="animation-caption delay-1">
						<h1>LOS MEJORES Accesorios</h1>
					</article>
				</div>
			</div>
		</div>
	</div>
	<div class="next-banner button-next next-white"></div>
  	<div class="prev-banner button-prev prev-white"></div>
	<div class="swiper-pagination banner-pagination"></div>
</section>

<section class="main-car-accesorios full clear-fix">
	<div class="wrapper-main center">
		<div class="title-thums relative">
    		<div thumbsSlider="" class="swiper swiper-thums-accesorio">
			    <div class="swiper-wrapper">
			        <div class="swiper-slide"><h4>Automóviles</h4></div>
			        <div class="swiper-slide"><h4>SUV</h4></div>
			        <div class="swiper-slide"><h4>Camionetas</h4></div>
			        <div class="swiper-slide"><h4>Pick Ups</h4></div>
			        <div class="swiper-slide"><h4>Utilitarios</h4></div>
			        <div class="swiper-slide"><h4>Híbridos</h4></div>
			    </div>
			</div>
		    <div class="button-next next-red next-accesorio">next</div>
		    <div class="button-prev prev-red prev-accesorio"> prev</div>
		</div>	
    	<div class="swiper swiper-accesorio">
		    <div class="swiper-wrapper">
		    	<!-- Item 1 -->
		        <div class="swiper-slide">
		        	<div class="row row-xs">
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="accesorios-por-marca/">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-1.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-2.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-3.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-4.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-5.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-6.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-7.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-8.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-9.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-10.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		            </div>
		        </div>
		        <!-- Item 2 -->
		        <div class="swiper-slide">
		        	<div class="row row-xs">
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-3.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-4.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-1.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-2.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        	</div>
		        </div>
		        <!-- Item 3 -->
		        <div class="swiper-slide">
		        	<div class="row row-xs">
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-5.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-6.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-7.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-8.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        	</div>	
		        </div>
		        <!-- Item 4 -->
		        <div class="swiper-slide">
		        	<div class="row row-xs">
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-9.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-10.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-8.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-7.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-10.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        	</div>
		        </div>
		        <!-- Item 5 -->
		        <div class="swiper-slide">
		        	<div class="row row-xs">
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-4.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-5.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-6.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-7.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        	</div>
		        </div>
		        <!-- Item 6 -->
		        <div class="swiper-slide">
		        	<div class="row row-xs">
		        		<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-7.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-8.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-9.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		                <div class="col-6 col-sm-3 col-lg-3 col-xl-3">
				    		<article class="card-accesorios">
				    			<figure>
				    				<a href="">
				    					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/watermark-10.png" alt=""> 
				    				</a>
				    			</figure>
				    		</article>
		                </div>
		        	</div>
		        </div>
		    </div>
		</div>
	</div>
</section>

    
    

<?php get_footer(); ?>