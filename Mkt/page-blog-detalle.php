<?php get_header(); ?>
<div class="bar-return">
	<div class="wrapper-main center">
		<a href="" class="btn-return">Regresar</a>
	</div>
</div>
<section class="banner-blog full clear-fix">
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-blog.jpg" alt="">
	</figure>
</section>
<section class="main-blog-detalle full clear-fix">
	<div class="wrapper-main center">
		<h6>lorem</h6>
		<h1>Lorem ipsum dolor sit amet,</h1>
		<article class="leyeng-blog">
			<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
		</article>
		<div class="the_content-blog full clear-fix">
			<h2>Lorem ipsum dolor sit amet,</h2>
			<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam <span>nonumy eirmod tempor</span> invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. </p>
			<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor <span>nonumy eirmod tempor</span> invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam  invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/the-content-1.jpg" alt="">
			<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam <span>nonumy eirmod tempor</span> invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. </p>
			
			<hr>
			<div class="cont-share">
					<span>Compartir</span>
					<div class="redes-share">
            			<ul>
            				<li class="icon-fb"><a href="" target="_blank">facebook</a></li>
            				<li class="icon-tw"><a href="" target="_blank">twitter</a></li>
            				<li class="icon-it"><a href="" target="_blank">instagram</a></li>
            			</ul>
            		</div>
				</div>
			
		</div>
		
	</div>
	
</section>


<?php get_footer(); ?>