<?php get_header(); ?>
<div class="bar-return">
	<div class="wrapper-main center">
		<a href="" class="btn-return">Regresar</a>
	</div>
</div>
<!-- <section class="banner-blog-general full  clear-fix">
	<figure>
		<img src="<?php //echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-blog-gral.jpg" alt="">
	</figure>
</section> -->
<section class="main-blog full clear-fix">
	<div class="wrapper-main center">
		<h1>BLOG</h1>
		<article class="leyeng-blog">
			<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
		</article>

		<div id="tabs-tags-blog">
			<ul class="titles-tags">
				<li><a href="#tabs-1">Todos</a></li>
				<li><a href="#tabs-2">Novedades</a></li>
				<li><a href="#tabs-3">Tips</a></li>
				<li><a href="#tabs-4">Otros</a></li>
			</ul>
			<!-- Tab 1 -->
			<div id="tabs-1">
				<div class="row row-xs">
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog" 
						data-aos="fade-up" 
						data-aos-duration="600" 
						data-aos-delay="300">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-1.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Lorem ipsum dolor</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog"
						data-aos="fade-up" 
						data-aos-duration="600" 
						data-aos-delay="500">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-2.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Lorem  sit ame</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog"
						data-aos="fade-up" 
						data-aos-duration="600" 
						data-aos-delay="700">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-3.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Lorem consectetur</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog"
						data-aos="fade-up" 
						data-aos-duration="600" 
						data-aos-delay="700">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-3.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Lorem sint cupiditate</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog"
						data-aos="fade-up" 
						data-aos-duration="600" 
						data-aos-delay="700">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-1.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Sint cupiditate jorem</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog"
						data-aos="fade-up" 
						data-aos-duration="600" 
						data-aos-delay="700">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-3.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Lorem</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
				</div>
			</div>

			<!-- Tab 2 -->
			<div id="tabs-2">
				<div class="row row-xs">
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-1.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Novedades ---</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-3.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Novedades </h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
				</div>
			</div>
			<!-- Tab 3 -->
			<div id="tabs-3">
				<div class="row row-xs">
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-2.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Tips</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
				</div>
			</div>
			<!-- Tab 4 -->
			<div id="tabs-4">
				<div class="row row-xs">
					<div class="col-6 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-blog">
							<figure>
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/blog-1.jpg" alt="">
								</a>
							</figure>
							<div class="details-blog">
								<h4>Otros</h4>
								<h3>Lorem ipsum dolor sit amet, consectetur</h3>
								<p>Lorem, ipsum dolor sit amet, consectetur adipisicing elit. Fuga tenetur sunt, sint cupiditate dolorem esse.</p>
								<a href="" class="btn-yokomotor-light">LEER MÁS</a>
							</div>
						</article>
					</div>
				</div>
			</div>

		</div>
	</div>
	
</section>


<?php get_footer(); ?>