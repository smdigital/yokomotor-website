<?php get_header(); ?>
<div class="main-boutique full clear-fix">
	<div class="wrapper-main center">
		<article class="leyend-boutique">
			<h1>Boutique</h1>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan.</p>					
		</article>
	</div>
	<section class="row-boutique-categorias full clear-fix">
		<div class="wrapper-main center">
			<h2>nuestras categorías</h2>
			<div class="icon-categoria">
				<a href="boutique/ropa">
					<i>
						<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/categoria-1.svg" alt="">
					</i>
					<h6>Ropa</h6>
				</a>
			</div>
			<div class="icon-categoria">
				<a href="accesorios">
					<i>
						<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/categoria-2.svg" alt="">
					</i>
					<h6>Accesorios</h6>	
				</a>
			</div>
			<div class="icon-categoria">
				<a href="">
					<i>
						<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/categoria-3.svg" alt="">
					</i>
					<h6>Souvenir</h6>
				</a>
			</div>
		</div>
	</section>
	<div class="clr"></div>
	<section class="row-productos-destacados full clear-fix">
		<div class="wrapper-main center">
			<h2>Productos Destacados</h2>
			<a href="boutique/ropa/" class="btn-arrow-light">Ver todos</a>
			<div class="row-card-5">
                <div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/gorra.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/gorra.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/gorra.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
				<div class="col-cards">
					<article class="card-producto">
						<a href="">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/gorra.png" alt="">
							</figure>
							<h6>$25.000</h6>
							<div class="figcaption">
								<p>Lorem ipsum</p>
							</div>
						</a>
					</article>
				</div>
			</div>
		</div>
	</section>
</div>

<section class="main_banner-promocional full clear-fix">
	<figure>
		<a href="">
			<!-- Images Desktop -->
			<img class="hidden-xs" src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-promocional-1.jpg" alt="">

			<!-- Images Movile -->
			<img class="hidden-lg" src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-promocional-1-xs.jpg" alt="">
		</a>
	</figure>
</section>

<?php get_footer(); ?>

