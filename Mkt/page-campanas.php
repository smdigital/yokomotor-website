<?php get_header(); ?>
<section class="main-campaign-all full clear-fix pt-5 pt-3-xs">
	<div class="wrapper-main center">	    	
		<h1>TODAS LAS CAMPAÑAS</h1>
		<div class="row row-xs">
			<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
				<article class="card-information campaign-small">
					<a href="">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-3.jpg" alt="">
						</figure>
						<div class="overflow">
							<h3>Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet consectetur.</p>
							<div class="btn-yokomotor-arrow">CTA</div>
						</div>
					</a>
				</article>
			</div>
			<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
				<article class="card-information campaign-small">
					<a href="">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-4.jpg" alt="">
						</figure>
						<div class="overflow">
							<h3>Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet consectetur.</p>
							<div class="btn-yokomotor-arrow">CTA</div>
						</div>
					</a>
				</article>
			</div>
			<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
				<article class="card-information campaign-small">
					<a href="">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-5.jpg" alt="">
						</figure>
						<div class="overflow">
							<h3>Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet consectetur.</p>
							<div class="btn-yokomotor-arrow">CTA</div>
						</div>
					</a>
				</article>
			</div>
			<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
				<article class="card-information campaign-small">
					<a href="">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-5.jpg" alt="">
						</figure>
						<div class="overflow">
							<h3>Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet consectetur.</p>
							<div class="btn-yokomotor-arrow">CTA</div>
						</div>
					</a>
				</article>
			</div>
			<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
				<article class="card-information campaign-small">
					<a href="">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-3.jpg" alt="">
						</figure>
						<div class="overflow">
							<h3>Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet consectetur.</p>
							<div class="btn-yokomotor-arrow">CTA</div>
						</div>
					</a>
				</article>
			</div>
			<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
				<article class="card-information campaign-small">
					<a href="">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/card-information-4.jpg" alt="">
						</figure>
						<div class="overflow">
							<h3>Lorem ipsum</h3>
							<p>Lorem ipsum dolor sit amet consectetur.</p>
							<div class="btn-yokomotor-arrow">CTA</div>
						</div>
					</a>
				</article>
			</div>
		</div>					
	</div>
</section>
<section class="main-after-sales full clear-fix">
	<div class="wrapper-main center">
		<h2>Campañas postventa</h2>
		<p>¿Quieres <strong>conectarte</strong> con las ofertas Yokomotor?, únete al newsletter</p>
		<div class="con-newsletter">
			<fieldset>
                <legend>EMAIL</legend>
                <input type="email" id="" name="">
            </fieldset>
			<div class="checked-style">
                <label for="acepto-newsletter">
                    <input type="checkbox" id="acepto-newsletter">
                    <div class="check-label"></div>
                    Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>

                </label>
            </div>
            <input type="submit">
		</div>
		
	</div>
</section>


    

<?php get_footer(); ?>