<?php get_header(); ?>
<section class="main-modelos-carrousel full clear-fix relative">
	<div class="wrapper-main center">
		<h1>Selecciona un modelo</h1>
		<div class="col-swiper-modelos relative">
			<div class="swiper swiper-modelos-small">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="card-modelo-small">
							<a href="javascript:;">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/yaris.jpg" alt=""> 
								</figure>
								<hr>
								<h3>SEG</h3>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-modelo-small">
							<a href="javascript:;">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/corolla.jpg" alt=""> 
								</figure>
								<hr>
								<h3>XE-1 HYBRID</h3>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-modelo-small">
							<a href="javascript:;">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/corolla2.jpg" alt=""> 
								</figure>
								<hr>
								<h3>XE-1 HÍBRIDO</h3>
							</a>
						</article>
					</div>
										
				</div>
			</div>
	      	<div class="prev-modelo button-prev prev-white"></div>
			<div class="swiper-pagination modelo-pagination"></div>
			<div class="next-modelo button-next next-white"></div>
		</div>
	</div>
</section>

<section class="main-modelos-color full clear-fix relative">
	<div class="wrapper-main center">
		<h2>Corolla</h2>
		<section class="col-modelos-color relative">
			<div class="swiper swiper-car-color">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-1.png" alt=""> 
						</figure>
						<h4 class="name-color">Blanco perlado</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-2.png" alt=""> 
						</figure>
						<h4 class="name-color"> Gris celeste</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-3.png" alt=""> 
						</figure>
						<h4 class="name-color">Marrón metálico</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-4.png" alt=""> 
						</figure>
						<h4 class="name-color">Negro</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-5.png" alt=""> 
						</figure>
						<h4 class="name-color">Plata metálico</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-6.png" alt=""> 
						</figure>
						<h4 class="name-color">Rojo metálico</h4>
					</div>
				</div>
			</div>			
			<div thumbsSlider="" class="swiper swiper-thums-color">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #fff;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #969992;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #59524f;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #000;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #acadaf;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #ba0c2f;"></div>
					</div>
				</div>
			</div>
			<div class="next-color"></div>
			<div class="prev-color"></div>
		</section>
		<div class="clr"></div>
		<div class="col-btns full">
			<a href="javascript:;" class="btn-cotizar">COTIZAR</a>
			<a href="javascript:;" class="btn-ficha">FICHA TÉCNICA</a>
		</div>
	</div>
</section>



<section class="main-widgets-features full clear-fix">
	<article class="icon-features">
		<figure>
			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/caracteristica-1.png" alt="">
		</figure>
		<figcaption>control electrónico de estabilidad</figcaption>
	</article>
	<article class="icon-features">
		<figure>
			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/caracteristica-2.png" alt="">
		</figure>
		<figcaption>alerta de colisión frontal</figcaption>
	</article>
	<article class="icon-features">
		<figure>
			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/caracteristica-3.png" alt="">
		</figure>
		<figcaption>Bolsa de aire ()</figcaption>
	</article>
	<article class="icon-features">
		<figure>
			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/caracteristica-4.png" alt="">
		</figure>
		<figcaption>sistema antibloqueo de frenos</figcaption>
	</article>
	<article class="icon-features">
		<figure>
			<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/caracteristica-5.png" alt="">
		</figure>
		<figcaption>sistema de sujeción infantil</figcaption>
	</article>
</section>





<!-- Sticky solo para mobile -->
<section class="sticky-xs-cotizacion animate__animated animate__fadeInUp animate__delay-5s">
	<ul>
		<li class="icon-cotizador"><a href="">Cotizar</a></li>
		<li class="icon-car"><a href="">Ficha técnica</a></li>
	</ul>
</section>
<section class="full-ball-planes clear-fix">
	<h3>Conoce nuestros planes de financiación</h3>
	<a href="javascript:;" class="btn-yokomotor">CLIC AQUÍ</a>
</section>
<?php get_footer(); ?>