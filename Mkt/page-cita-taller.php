<?php get_header(); ?>

<section class="banner-general full  clear-fix">
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-general-1.jpg" alt="">
	</figure>
</section>
<section class="main-cita-taller full clear-fix">
	<div class="wrapper-main center">
		<h1>Cita taller</h1>
		<hr>
		<div class="details-cita">
			<ul>
				<li>Tarda máximo 1 hora</li>
				<li>3 técnicos trabajan de forma simultanea</li>
				<li>Cambio de aceite y filtros</li>
				<li>Repuestos genuinos Toyota</li>
			</ul>
		</div>
		<hr>
		<section class="form-yokomotor full clear-fix">
			<p>Programa tu cita de taller, envíanos tus datos personales y un asesor te contactará.</p>
            <div class="row row-xs main-form-fieldset">
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Nombre completo</legend>
		                <input type="text" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>EMAIL</legend>
		                <input type="email" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>CELULAR</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Tipo de servicio</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
	                <div class="row">                	
		                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
							<fieldset>
				                <legend>Ciudad</legend>
				                <select name="" id="">
				                	<option value="">Selecciona</option>
				                	<option value="">-------</option>
				                	<option value="">-------</option>
				                	<option value="">-------</option>
				                </select>
				            </fieldset>
		                </div>
		                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
							<fieldset>
				                <legend>Sede</legend>
				                <select name="" id="">
				                	<option value="">Selecciona</option>
				                	<option value="">-------</option>
				                	<option value="">-------</option>
				                	<option value="">-------</option>
				                </select>
				            </fieldset>
		                </div>
	                </div>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Placa</legend>
		                <input type="text" id="" name="">
		            </fieldset>
                </div>
            </div>
            <div class="checked-style checked-red">
                <label for="acepto-terms-citas">
                    <input type="checkbox" id="acepto-terms-citas">
                    <div class="check-label"></div>
                    Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
                </label>
            </div>
            <div class="clr"></div>
            <input type="submit" value="ENVIAR">
		</section>
	</div>
</section>


<?php get_footer(); ?>