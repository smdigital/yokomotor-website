<?php get_header(); ?>
<section class="banner-general banner-etica full  clear-fix">
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-linea-etica.jpg" alt="">
	</figure>
</section>
<section class="main-line-etica full clear-fix">
	<div class="wrapper-main-md center">
		<h1>LÍNEA ÉTICA</h1>	
		<article>
			<p><strong>La línea ética</strong> ha sido creada para que los proveedores, clientes, empleados y grupo de interés de ATC puedan reportar hechos reales o potenciales de corrupción, irregularidades, actos incorrectos o cualquier otro tipo de situaciones que afecten el adecuado clima ético de la compañía.</p>
			<p>Recuerda que la línea ética es un canal donde la información recibida tendrá un tratamiento bajo parámetros estrictos de <strong>confidencialidad y respeto.</strong></p>
			<p><strong>Llama al:</strong> 3809422 <br>
			Buzón habilitado los <strong>7 días</strong> de la semana, <strong>24 horas</strong> al día. <a href="mailto:lineaeticatoyota@toyota.com.co">lineaeticatoyota@toyota.com.co</a><br>
			 </p>
		</article>
		<section class="form-yokomotor full clear-fix">
			<h2>LÍNEA DE ÉTICA</h2>
            <div class="row row-xs main-form-fieldset">
        		<div class="col-12 col-sm-12 col-lg-12 col-xl-12">
					<fieldset>
		                <legend><span>*</span>Tipo de usuario</legend>
		                <select name="" id="">
		                	<option value="">Seleccionar</option>
			                <option value="">Cliente</option>
			                <option value="">Proveedor</option>
			                <option value="">Empleado</option>
			            </select>
		            </fieldset>
                </div>
        		<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Nombre</legend>
		                <input type="text" id="" name="">
		            </fieldset>
        		</div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>APELLIDO</legend>
		                <input type="text" id="" name="">
		            </fieldset>
                </div> 
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Tipo de Documento</legend>
		                <select name="" id="">
										<option value="">Seleccionar</option>
              <option value="">CC</option> 
              <option value="">NIT</option>
              <option value="">CE</option>								 
              <option value="">PA</option>
	      <option value="">TI</option>
			            </select>
		            </fieldset>
                </div> 
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Documento de Identidad</legend>
		                <input type="number" id="" name="">
		            </fieldset>
                </div> 
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><span>*</span>Ciudad</legend>
		                <select name="" id="">
		                	<option value="">Seleccionar</option>
			                <option value="">---------</option>
			                <option value="">---------</option>
			                <option value="">---------</option>
			            </select>
		            </fieldset>
                </div>              		
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Correo electrónico</legend>
		                <input type="email" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Fijo</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>CELULAR</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
					<fieldset>
		                <legend><span>*</span>Tipo de Denuncia</legend>
		                <select name="" id="">
		                	<option value="">Seleccionar</option>
		                	<option value="">Fraude Económico (lavado de dinero, robo de mercancías, bienes o valores, gastos y compras sin autorización, etc.)</option>
						   <option value="">Adulteración de información contable, operativa y financiera, documentos legales y elusión de controles internos de la empresa.</option>
						   <option value="">Malversación de activos (Robo o desviación de los dineros o activos de la Compañía, uso inadecuado de la información y/o activos)</option>
						   <option value="">Corrupción y soborno (acuerdo con proveedores/clientes - dar y/o recibir sobornos, chantajes, tráfico de influencias, pago de comisiones).</option>
						   <option value="">Lavado de activos y/o financiación del terrorismo</option>
						   <option value="">Infracción a la propiedad intelectual (Divulgar o vender información confidencias y de negocio, acceso indebido a la información)</option>
						   <option value="">Comportamientos inadecuados de empleados/colaboradores, supervisores y/o gerentes (incluye abusos de poder, favoritismo, amenaza y mal comportamiento)</option>
						   <option value="">Discriminación y malos tratos al personal (actos en detrimento del sexo, raza, nacionalidad, etc.)</option>
						   <option value="">Consultas y dilemas éticos</option>
						   <option value="">Otros </option>
		                </select>
		            </fieldset>
                </div>
                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
					<fieldset>
		                <legend><span>*</span>Denuncia</legend>
		                <textarea name="" id=""></textarea>
		            </fieldset>
                </div>
                <div class="col-12 col-sm-12 col-lg-12 col-xl-12">
					<fieldset>
		                <legend><span>*</span>Adjuntos</legend>
		                <input type="file">
		            </fieldset>
                </div>
            </div>
            <div class="checked-style checked-red">
                <label for="acepto-terms-citas">
                    <input type="checkbox" id="acepto-terms-citas">
                    <div class="check-label"></div>
                    Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
                </label>
            </div>
            <div class="clr"></div>
            <input type="submit" value="ENVIAR">
		</section>
		<section class="terms-etica full clear-fix">
			<h2>Términos y condiciones</h2>
			<div class="accordion-detalles">
				<h3>Objetivo</h3>
				<div class="terms-accordion">
					<p>A través del <strong>Canal Ético</strong> se podrán reportar eventos o potenciales situaciones <strong>relacionadas con fraude, malas prácticas, corrupción, lavado de activos</strong> y cualquier otra situación irregular.</p>
				</div>
				<h3>Uso de la información reportada</h3>
				<div class="terms-accordion">
					<p>El <strong>Canal Ético de Automotores Toyota Colombia SAS, </strong>se reserva el derecho de determinar cómo actuar o no actuar, con base en la información proporcionada por el reportante, a menos que la ley solicite lo contrario. Además, al Canal Ético no se le solicita divulgar ninguna respuesta(s) o acción(es) concerniente(s) a cualquier información que se suministre.</p>
				</div>
				<h3>Limitación de la responsabilidad</h3>
				<div class="terms-accordion">
					<p>Como reportante reconoce que la única obligación del Canal Ético de ATC será recibir y transmitir la información suministrada en el reporte, para que se dé el trámite apropiado.</p>
				</div>
				<h3>Veracidad del reporte</h3>
				<div class="terms-accordion">
					<p><strong>El Canal Ético de ATC</strong> toma con seriedad los reportes formulados a través de este sitio. Las actividades de transmisión o difusión de rumores sin fundamento, la presentación de reportes o el suministro de información deliberadamente falsa o engañosa no será sujeta a seguimiento ni revisión.</p>
					<p>Usted está en la facultad de <strong>registrar sus datos personales o no,</strong> aunque le recomendamos con el fin de dar mayor celeridad a la atención de su reporte, dejarlos registrados en los espacios correspondientes y en la medida de lo posible, aportar evidencias o información precisa.</p>
				</div>
			</div>				
		</section>
	</div>
</section>
<?php get_footer(); ?>