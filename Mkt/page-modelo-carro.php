<?php get_header(); ?>
<section class="main-modelos-carrousel full clear-fix relative">
	<div class="wrapper-main center">
		<h1>Selecciona un modelo</h1>
		<div class="col-swiper-modelos relative">
			<div class="swiper swiper-modelos-small">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="card-modelo-small">
							<a href="javascript:;">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/yaris.jpg" alt=""> 
								</figure>
								<hr>
								<h3>SEG</h3>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-modelo-small">
							<a href="javascript:;">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/corolla.jpg" alt=""> 
								</figure>
								<hr>
								<h3>XE-1 HYBRID</h3>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-modelo-small">
							<a href="javascript:;">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/corolla2.jpg" alt=""> 
								</figure>
								<hr>
								<h3>XE-1 HÍBRIDO</h3>
							</a>
						</article>
					</div>
										
				</div>
			</div>
	      	<div class="prev-modelo button-prev prev-white"></div>
			<div class="swiper-pagination modelo-pagination"></div>
			<div class="next-modelo button-next next-white"></div>
		</div>
	</div>
</section>

<section class="main-modelos-color full clear-fix relative">
	<div class="wrapper-main center">
		<h2>Corolla Hybrid <span>2021 XE-1</span></h2>
		<section class="col-modelos-color relative">
			<div class="swiper swiper-car-color">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-1.png" alt=""> 
						</figure>
						<h4 class="name-color">Blanco perlado</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-2.png" alt=""> 
						</figure>
						<h4 class="name-color"> Gris celeste</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-3.png" alt=""> 
						</figure>
						<h4 class="name-color">Marrón metálico</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-4.png" alt=""> 
						</figure>
						<h4 class="name-color">Negro</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-5.png" alt=""> 
						</figure>
						<h4 class="name-color">Plata metálico</h4>
					</div>
					<div class="swiper-slide">
						<figure class="img-color">
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/colores/corolla-6.png" alt=""> 
						</figure>
						<h4 class="name-color">Rojo metálico</h4>
					</div>
				</div>
			</div>			
			<div thumbsSlider="" class="swiper swiper-thums-color">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #fff;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #969992;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #59524f;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #000;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #acadaf;"></div>
					</div>
					<div class="swiper-slide">
						<div class="bullet-color" style="background-color: #ba0c2f;"></div>
					</div>
				</div>
			</div>
			<div class="next-color"></div>
			<div class="prev-color"></div>
		</section>
		<div class="clr"></div>
		<div class="col-btns full">
			<a href="javascript:;" class="btn-cotizar">COTIZAR</a>
			<a href="javascript:;" class="btn-ficha">FICHA TÉCNICA</a>
		</div>
	</div>
</section>
<section class="main-features-specs full clear-fix">
	<div class="wrapper-main center">
		<div id="tabs-features-specs" class="features-specs-tabs">
			<ul class="title-tabs-line">
				<li><a href="#features">Características</a></li>
				<li><a href="#specs">Especificaciones</a></li>
			</ul>
			<!-- Item Características -->
			<div id="features" class="content-features">
				<div class="wrapper-main-sm center">
					<h2>Diseño</h2>
					<div id="tabs-disenos">
						<ul class="title-tabs-line title-tabs-small">
							<li><a href="#exterior">Exterior</a></li>
							<li><a href="#interior">Interior</a></li>
							<li><a href="#video-carro">Video</a></li>
						</ul>
						<!-- Item Exterior -->
						<div id="exterior" class="content-exterior">
							<div class="swiper swiper-exterior">
								<div class="swiper-wrapper">
									<div class="swiper-slide">
										<figure>
											<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/esterior-1.jpg" alt=""> 
										</figure>
									</div>
									<div class="swiper-slide">
										<figure>
											<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/esterior-2.jpg" alt=""> 
										</figure>
									</div>
									<div class="swiper-slide">
										<figure>
											<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/esterior-3.jpg" alt=""> 
										</figure>
									</div>
								</div>
							</div>
					      	<div class="prev-exterior button-prev prev-white"></div>
							<div class="swiper-pagination pagination-diseno exterior-pagination"></div>
							<div class="next-exterior button-next next-white"></div>
						</div>
						<!-- Item Interios -->
						<div id="interior">
							Slider Car Interior
						</div>
						<!-- Item Video --> 
						<div id="video-carro" class="tab-video">
							<iframe src="https://www.youtube.com/embed/je5GAHNru4k?rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>	
					</div>
				</div>
			</div>	
			<!-- Item Especificaciones -->
			<div id="specs" class="content-specs">
				<div class="wrapper-main-sm center">
					<h2>Detalles técnicos</h2>
					<a href="" download class="download-icon">Descargar brochure</a>
					<div class="accordion-detalles">
						<h3>Características</h3>						
						<div class="content-detalles-tabs">
							<p>Conten peso Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni expedita culpa deserunt laborum eligendi distinctio et voluptate iusto, esse ducimus, est provident debitis, molestias. Nihil, quod nostrum quasi pariatur non!</p>
						</div>
						<h3>peso</h3>
						<div class="content-detalles-tabs">
							<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea</p>
							<ol>
								<li>PESO VACÍO (Kg)<span>1.370</span></li>
								<li>PESO BRUTO VEHICULAR (Kg)<span>1.760</span></li>
								<li>CAPACIDAD DE CARGA (Kg)<span>390</span></li>
							</ol>
						</div>
						<h3>motor</h3>
						<div class="content-detalles-tabs">
							<p>Conten motor Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni expedita culpa deserunt laborum eligendi distinctio et voluptate iusto, esse ducimus, est provident debitis, molestias. Nihil, quod nostrum quasi pariatur non!</p>
						</div>
						<h3>transmisión y suspensión</h3>
						<div class="content-detalles-tabs">
							<p>Conten transmisión y suspensión Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni expedita culpa deserunt laborum eligendi distinctio et voluptate iusto, esse ducimus, est provident debitis, molestias. Nihil, quod nostrum quasi pariatur non!</p>
						</div>
						<h3>dirección y llantas</h3>
						<div class="content-detalles-tabs">
							<p>Conten dirección y llantas Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni expedita culpa deserunt laborum eligendi distinctio et voluptate iusto, esse ducimus, est provident debitis, molestias. Nihil, quod nostrum quasi pariatur non!</p>
						</div>
						<h3>seguridad activa</h3>
						<div class="content-detalles-tabs">
							<p>Conten seguridad activa Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni expedita culpa deserunt laborum eligendi distinctio et voluptate iusto, esse ducimus, est provident debitis, molestias. Nihil, quod nostrum quasi pariatur non!</p>
						</div>

						<h3>seguridad pasiva</h3>
						<div class="content-detalles-tabs">
							<p>Conten seguridad pasiva Lorem ipsum dolor sit amet, consectetur adipisicing elit. Magni expedita culpa deserunt laborum eligendi distinctio et voluptate iusto, esse ducimus, est provident debitis, molestias. Nihil, quod nostrum quasi pariatur non!</p>
						</div>
					</div>
				</div>
			</div>	
		</div>
	</div>
</section>
<div class="col-360 full clear-fix">
	<a href="" class="btn-360">Tour 360°</a>
</div>
<section class="main-destacado-car full clear-fix">
	<div class="wrapper-main center">
		<h2>Destacados del corolla</h2>
		<div id="tabs-destacados">
			<ul class="title-tabs-line title-tabs-small">
				<li><a href="#diseno">Diseño</a></li>
				<li><a href="#tecnologia">Tecnología</a></li>
				<li><a href="#seguridad">Seguridad</a></li>
			</ul>
			<!-- Item Diseño-->
			<div id="diseno" class="content-destacados">
				<div class="row row-xs">
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-destacado">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/destacada-1.jpg" alt="">
							</figure>
							<h5>DISEÑO INTERIOR</h5>
							<p>Cámara de reversa, papeles al día, impuestos al día, asegurado. Poco uso, técnico mecánica hasta enero del 2022, soat hasta enero 2022, recién porcelanizado. Retrovisores abatibles. Cámara de retroceso, sensores, bloqueo central de alarma. Papeles e impuestos al día</p>
						</article>
					</div>
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-destacado">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/destacada-2.jpg" alt="">
							</figure>
							<h5>Modern Style</h5>
							<p>Cámara de reversa, papeles al día, impuestos al día, asegurado. Poco uso, técnico mecánica hasta enero del 2022, soat hasta enero 2022, recién porcelanizado.</p>
						</article>
					</div>
				</div>
			</div>
			<!-- Item tecnologia-->
			<div id="tecnologia" class="content-destacados">
				<div class="row row-xs">
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-destacado">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/destacada-3.jpg" alt="">
							</figure>
							<h5>Tecnologia</h5>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus ut qui dolore exercitationem, Architecto adipisci voluptas, ab reiciendis sequi ratione eligendi, nesciunt odit vitae porro. Architecto adipisci enim nobis maiores nihil illo enim nobis maiores nihil illo.</p>
						</article>
					</div>
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-destacado">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/destacada-4.jpg" alt="">
							</figure>
							<h5>Modern Style</h5>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur sed eaque doloremque, dolorem animi perferendis culpa minima quisquam nostrum nam natus quod ab dolor inventore dignissimos nihil atque dolores eius?</p>
						</article>
					</div>
				</div>
			</div>
			<!-- Item seguridad-->
			<div id="seguridad" class="content-destacados">
				<div class="row row-xs">
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-destacado">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/destacada-5.jpg" alt="">
							</figure>
							<h5>Seguridad</h5>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus ut qui dolore exercitationem, voluptas, ab reiciendis sequi ratione eligendi, nesciunt odit vitae porro. Architecto adipisci enim nobis maiores nihil illo  dolor sit amet consectetur adipisicing elit.</p>
						</article>
					</div>
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
						<article class="card-destacado">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/destacada-6.jpg" alt="">
							</figure>
							<h5>Lorem ipsum dolor</h5>
							<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur sed eaque doloremque, dolorem animi perferendis culpa minima quisquam nostrum nam natus quod ab dolor inventore dignissimos nihil atque dolores eius?</p>
						</article>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="sticky-xs-cotizacion animate__animated animate__fadeInUp animate__delay-5s">
	<ul>
		<li class="icon-cotizador"><a href="">Cotizar</a></li>
		<li class="icon-car"><a href="">Ficha técnica</a></li>
	</ul>
</section>
<section class="full-ball-planes clear-fix">
	<h3>Conoce nuestros planes de financiación</h3>
	<a href="javascript:;" class="btn-yokomotor">CLIC AQUÍ</a>
</section>
	



<?php get_footer(); ?>