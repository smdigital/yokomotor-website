<?php get_header('prehome'); ?>
<section class="main-prehome disabled">
	<article class="content-prehome prehome-toyota">
		<a href="/">
			<div class="content-details">
				<figure class="animate__animated animate__fadeInDown animate__delay-1s">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-yokomotor-toyora.png" alt="">
				</figure>
				<div class="clr"></div>
				<div class="btn-ir animate__animated animate__fadeInUp animate__delay-1s">Ir</div>
			</div>
		</a>
		<div class="shadow-clip-path"></div>
	</article>
	<article class="content-prehome prehome-hino">
		<a href="/">
			<div class="content-details">
				<figure class="animate__animated animate__fadeInDown animate__delay-1s">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-yokomotor-hino.png" alt="">
				</figure>
				<div class="clr"></div>
				<div class="btn-ir animate__animated animate__fadeInUp animate__delay-1s">Ir</div>
			</div>			
		</a>
		<div class="shadow-clip-path"></div>
	</article>
	
</section>			

<?php get_footer('prehome'); ?>