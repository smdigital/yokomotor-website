<?php get_header(); ?>
<div class="bar-return">
	<div class="wrapper-main center">
		<a href="" class="btn-return">Regresar</a>
	</div>
</div>
<div class="wrapper-main-sm center main-details-shop">
	<!-- Este h1 y h2 Aplica para mobile  -->
	<article class="hidden-lg">
		<h1>Camiseta Toyota r1</h1>
		<h2>ropa</h2>		
	</article>
	<div class="row row-xs">
        <div class="col-12 col-sm-6 col-lg-6 col-xl-6 thumbnail-clothes">
        	<div class="row-swiper-ropa">
				<div class="swiper swiper-ropa">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-1.png" alt="">
							</figure>
						</div>
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-1.png" alt="">
							</figure>
						</div>
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-1.png" alt="">
							</figure>
						</div>
					</div>
				</div>    
				<div class="button-prev prev-white prev-ropa"></div>
				<div class="swiper-pagination pagination-ropa"></div>
				<div class="button-next next-white next-ropa"></div> 
        	</div>			
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-6 clothes-details">
			<article class="details-shop-ropa">
				<h1>Camiseta Toyota r1</h1>
				<h2>ropa</h2>
				<h3>$29.000</h3>
				<div class="input-shop input-cantidad">
					<label>Cantidad</label>
					<button type="button" class="minus qib-button">-</button>
					<input type="text" value="1">
					<button type="button" class="plus qib-button">+</button>
				</div>
				<div class="input-shop input-talla">
					<label>Talla</label>
					<select name="" id="">
						<option value="">M</option>
						<option value="">L</option>
						<option value="">XL</option>
					</select>
				</div>
				<div class="clr"></div>
				<a href="" class="btn-whatsapp">PREGUNTAR</a>
				<div class="cont-share">
					<span>Compartir</span>
					<div class="redes-share">
            			<ul>
            				<li class="icon-fb"><a href="" target="_blank">facebook</a></li>
            				<li class="icon-tw"><a href="" target="_blank">twitter</a></li>
            				<li class="icon-it"><a href="" target="_blank">instagram</a></li>
            			</ul>
            		</div>
				</div>
			</article>
		</div>
	</div>
</div>

<section class="main-clothes full clear-fix">
	<div class="wrapper-main-sm center">
		<h2>También te puede interesar</h2>
		<div class="row-swiper-clothes">
			<div class="swiper swiper-clothes">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="card-producto">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Lorem ipsum</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-producto">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Lorem ipsum</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-producto">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Lorem ipsum</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-producto">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Lorem ipsum</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-producto">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Lorem ipsum</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card-producto">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Lorem ipsum</p>
								</div>
							</a>
						</article>
					</div>
				</div>
			</div>
			<div class="button-next next-black next-clothes"></div>
			<div class="button-prev prev-black prev-clothes"></div>
			<div class="swiper-pagination pagination-clothes"></div>
		</div>
	</div>
</section>


<?php get_footer(); ?>