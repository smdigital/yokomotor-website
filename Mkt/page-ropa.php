<?php get_header(); ?>
<div class="main-boutique boutique-ropa full clear-fix">
	<div class="wrapper-main center">
		<h1>Boutique Ropa</h1>			
	</div>	
	<div class="clr"></div>
	<section class="main-boutique-ropa full clear-fix">
		<div class="wrapper-main center">
			<div class="row row-xs">
                <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-ropa">
						<figure>
							<a href="ropa-detalle/">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-1.png" alt="">
							</a>
						</figure>
						<div class="info">
							<h3>Ropa</h3>
							<h4>$29.000</h4>
							<p>Camiseta Toyota R1</p>
							<a href="ropa-detalle/" class="btn-arrow-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-ropa">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
							</a>
						</figure>
						<div class="info">
							<h3>Ropa</h3>
							<h4>$129.000</h4>
							<p>Chaqueta</p>
							<a href="" class="btn-arrow-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-ropa">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-2.png" alt="">
							</a>
						</figure>
						<div class="info">
							<h3>Ropa</h3>
							<h4>$29.000</h4>
							<p>Camiseta Toyota R1</p>
							<a href="" class="btn-arrow-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-ropa">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-2.png" alt="">
							</a>
						</figure>
						<div class="info">
							<h3>Ropa</h3>
							<h4>$29.000</h4>
							<p>Camiseta Toyota R1</p>
							<a href="" class="btn-arrow-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-ropa">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-1.png" alt="">
							</a>
						</figure>
						<div class="info">
							<h3>Ropa</h3>
							<h4>$29.000</h4>
							<p>Camiseta Toyota R1</p>
							<a href="" class="btn-arrow-light">CTA</a>
						</div>
					</article>
				</div>
				<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
					<article class="card-ropa">
						<figure>
							<a href="">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/chaqueta.png" alt="">
							</a>
						</figure>
						<div class="info">
							<h3>Ropa</h3>
							<h4>$129.000</h4>
							<p>Chaqueta</p>
							<a href="" class="btn-arrow-light">CTA</a>
						</div>
					</article>
				</div>
			</div>
			<div class="clr"></div>
			<!-- Pagination por defecto wordpress -->
			<nav class="pagination">
				<ul class="page-numbers">
					<li>
						<span aria-current="page" class="page-numbers current">1</span>
					</li>
					<li>
						<a href="">2</a>
					</li>
					<li>
						<a href="">3</a>
					</li>
				</ul>
			</nav>
		</div>
	</section>
	
</div>



<?php get_footer(); ?>

