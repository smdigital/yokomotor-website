<?php get_header();
if( has_post_thumbnail() ): ?>
	<section class="banner-general banner-bottom full  clear-fix">
		<figure>
			<?php the_post_thumbnail( 'full' ); ?>
		</figure>
	</section>	
<?php endif; ?> ?>
<div class="main-service-workshop main-wrap-financing full clear-fix">
<?php the_title('<h1>', '</h1>'); 
	if( get_the_content() ): ?>
		<article>
			<?php the_content(); ?>
		</article>
	<?php endif; 
	if( function_exists( 'get_field' ) ):
		/**
			 * * ***************
			* ACF Custom fields Página Financiación (page)
			* ***************
			* @param ACF_fields 'yokomotor_dues'
			* 
			*/

			$articlesSection = get_field( 'yokomotor_dues' );

		if( $articlesSection && ($articlesSection['enable_section'] &&count($articlesSection['articles']) > 0) ): $articles = $articlesSection['articles']; ?>
			<div class="row-workshop">
				<?php foreach( $articles as $article ): 
					$image = $article['image']; ?>
					<section class="card-workshop-service full clear-fix">
						<div class="row row-xs center-vertical">
							<div class="col-12 col-sm-6 col-lg-6 col-xl-6 img-featured">
								<?php if( $image ): ?>
									<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
										<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['title']); ?>">
									</figure>
								<?php endif; ?>
							</div>
							<div class="col-12 col-sm-6 col-lg-6 col-xl-6 details-workshop">
								<?php if( !empty($article['title']) ): ?>
									<h2><?php echo $article['title']; ?></h2>
								<?php endif; 
								if( !empty($article['description']) ) {
									echo $article['description'];
								}; ?>
								<div class="text-center-btn">
									<!-- <a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-form-serivicio">PREGUNTAR A UN ASESOR</a> -->
									<div class="btn-tooltip">
				            			<div class="btn-yokomotor icon-whatsapp">PREGUNTAR A UN ASESOR</div>
				            			<ul>
				            				<li>
				            					<a href="https://api.whatsapp.com/send?phone=573002928762&text=Hola,%20quiero%20comunicarme%20con%20Comercial%20Yokomotor" target="_blank">
				            						<strong>Venta Medellín </strong>3102648638
				            					</a>
				            				</li>
				            				<li>
				            					<a href="#" target="_blank">
				            						<strong>Venta Ciudad </strong>3102648638
				            					</a>
				            				</li>
				            			</ul>	
				            		</div>
								</div>
							</div>
							</div>
					</section>
				<?php endforeach; ?>
			</div>
		<?php endif; 
	endif; ?>	
</div>

<!-- Lightbox Preguntar financiación -->
<div id="modal-form-serivicio" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<section class="form-yokomotor lightbox-form full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<hr>
			
			<h1>Pregunta por un Seguro</h1>
		    <div class="row row-xs main-form-fieldset">
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Nombre completo</legend>
		                <input type="text" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Teléfono</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>EMAIL</legend>
		                <input type="email" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>¿Cuándo quieres comprar?</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
		        </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Ciudad</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Sede</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
                </div>  
		    </div>
		    <div class="checked-style checked-red">
		        <label for="acepto-terms-citas">
		            <input type="checkbox" id="acepto-terms-citas">
		            <div class="check-label"></div>
		            Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
		        </label>
		    </div>
		    <div class="clr"></div>
		    <input type="submit" value="ENVIAR">
		</section>
	</div>
</div>
<?php get_footer(); ?>