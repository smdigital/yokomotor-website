<?php get_header(); ?>
<div class="main-service-workshop full clear-fix">
	<h1>Servicios del taller</h1>
	<div class="row-workshop">

		<!-- Item 1 -->
		<section class="card-workshop-service full clear-fix">
			<div class="row row-xs center-vertical">
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 img-featured">
					<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
						<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/servicio-1.jpg" alt="">
					</figure>
	            </div>
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 details-workshop">
	            	<h2>Mantenimiento Express</h2>
	            	<ul>
	            		<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed.</li>
	            		<li>Consetetur sadipscing elitr Lorem ipsum dolor sit amet, consetetur sadipscing, sed.</li>
	            		<li>Mapsum dolor sit amiasd sediantus sed.</li>            		
	            	</ul>
	            	<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
	            	<div class="text-center">
	            		<a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-form-serivicio">Solicitar servicio</a>
	            	</div>
	            </div>
	        </div>
		</section>

		<!-- Item 2 -->
		<section class="card-workshop-service full clear-fix">
			<div class="row row-xs center-vertical">
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 img-featured">
					<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
						<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/servicio-1.jpg" alt="">
					</figure>
	            </div>
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 details-workshop">
	            	<h2>Airlive</h2>
	            	<ul>
	            		<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed.</li>
	            		<li>Consetetur sadipscing elitr Lorem ipsum dolor sit amet, consetetur sadipscing, sed.</li>
	            		<li>Mapsum dolor sit amiasd sediantus sed.</li>            		
	            	</ul>
	            	<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
	            	<div class="text-center">
	            		<a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-form-serivicio">Solicitar servicio</a>
	            	</div>
	            </div>
	        </div>
		</section>

		<!-- Item 3 -->
		<section class="card-workshop-service full clear-fix">
			<div class="row row-xs center-vertical">
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 img-featured">
					<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
						<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/servicio-1.jpg" alt="">
					</figure>
	            </div>
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 details-workshop">
	            	<h2>LATONERÍA Y PINTURA</h2>
	            	<ul>
	            		<li>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed.</li>
	            		<li>Consetetur sadipscing elitr Lorem ipsum dolor sit amet, consetetur sadipscing, sed.</li>
	            		<li>Mapsum dolor sit amiasd sediantus sed.</li>            		
	            	</ul>
	            	<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
	            	<div class="text-center">
	            		<a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-form-serivicio">Solicitar servicio</a>
	            	</div>
	            </div>
	        </div>
		</section>
	</div>	
</div>

<!-- Lightbox Solicitar servicio -->
<div id="modal-form-serivicio" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<section class="form-yokomotor lightbox-form full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<hr>
			<h3>Servicio</h3>
			<h1>mantenimiento express</h1>
		    <div class="row row-xs main-form-fieldset">
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Nombre completo</legend>
		                <input type="text" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Teléfono</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>EMAIL</legend>
		                <input type="email" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>¿Cuándo quieres comprar?</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
		        </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Ciudad</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Sede</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
                </div>  
		    </div>
		    <div class="checked-style checked-red">
		        <label for="acepto-terms-citas">
		            <input type="checkbox" id="acepto-terms-citas">
		            <div class="check-label"></div>
		            Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
		        </label>
		    </div>
		    <div class="clr"></div>
		    <input type="submit" value="ENVIAR">
		</section>
	</div>
</div>




<?php get_footer(); ?>