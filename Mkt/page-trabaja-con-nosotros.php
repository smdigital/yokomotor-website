<?php get_header(); ?>

<section class="banner-general full  clear-fix">
	<figure>
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/trabaja.jpg" alt="">
	</figure>
</section>
<section class="main-job full clear-fix">
	<div class="wrapper-main center">
		<h1>Trabaja con nosotros</h1>
		<a href="javascript:;" class="btn-yokomotor btn-vacante">VACANTES</a>
		<article>
			<p>Lorem ipsum dolor sit amet, consetetur sadipscing <br> elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna</p>
		</article>
		<section class="form-yokomotor full clear-fix">
            <div class="row row-xs main-form-fieldset">
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
                	<div class="row">
                		<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
							<fieldset>
				                <legend>Nombre</legend>
				                <input type="text" id="" name="">
				            </fieldset>
                		</div>
		                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
							<fieldset>
				                <legend>APELLIDO</legend>
				                <input type="text" id="" name="">
				            </fieldset>
		                </div>                		
                	</div>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>EMAIL</legend>
		                <input type="email" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>CELULAR</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>VACANTE A LA QUE APLICA</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">--------</option>
		                	<option value="">--------</option>
		                </select>
		            </fieldset>
                </div>
            </div>
            <div class="checked-style checked-red">
                <label for="acepto-terms-citas">
                    <input type="checkbox" id="acepto-terms-citas">
                    <div class="check-label"></div>
                    Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
                </label>
            </div>
            <div class="clr"></div>
            <input type="submit" value="ENVIAR">
		</section>
	</div>
</section>


<?php get_footer(); ?>