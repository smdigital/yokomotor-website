<?php get_header(); ?>
<section class="main-yokomotor-banner full clear-fix">
	<article>
		<h1>yokomotor</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>		
	</article>
</section>
<section class="toyota-what-is full clear-fix">
	<div class="wrapper-main center">
		<div class="row row-xs center-vertical">
            <div class="col-12 col-sm-5 col-lg-5 col-xl-5">
				<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/toyoya-alta-calidad.png" alt="">
				</figure>
            </div>
            <div class="col-12 col-sm-7 col-lg-7 col-xl-7">
				<article>
					<p>La palabra <strong>“Toyota”</strong> se originó a partir del apellido del fundador, Kiichiro Toyoda. En sus inicios, los vehículos producidos por la empresa eran vendidos originalmente con el emblema “Toyoda”. El logo tenía los colores de la bandera japonesa rojo y blanco y la katakana de Toyota.</p>
				</article>		
            </div>
        </div>
	</div>
</section> 		
<section class="time-line-toyota full clear-fix">
	<div class="accordion-detalles">
		<h3>La Marca</h3>	
		<div class="cont-time">
			<h2>TOYOTA EN COLOMBIA</h2>
		</div>
		<h3>1959</h3>
		<div class="cont-time">
			<p><strong>1959</strong> Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Vero, repudiandae, omnis reiciendis ipsum eaque dolorum debitis, voluptatibus atque dolore placeat eum. Itaque odio libero culpa, nihil amet, qui delectus voluptate?</p>
		</div>
		<h3>1967</h3>
		<div class="cont-time">
			<p><strong>1967</strong> Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Vero, repudiandae, omnis reiciendis ipsum eaque dolorum debitis, voluptatibus atque dolore placeat eum. Itaque odio libero culpa, nihil amet, qui delectus voluptate?</p>
		</div>
		<h3>1992</h3>
		<div class="cont-time">
			<p><strong>1992</strong> Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Vero, repudiandae, omnis reiciendis ipsum eaque dolorum debitis, voluptatibus atque dolore placeat eum. Itaque odio libero culpa, nihil amet, qui delectus voluptate?</p>
		</div>
		<h3>2008</h3>
		<div class="cont-time">
			<p><strong>2008</strong> Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Vero, repudiandae, omnis reiciendis ipsum eaque dolorum debitis, voluptatibus atque dolore placeat eum. Itaque odio libero culpa, nihil amet, qui delectus voluptate?</p>
		</div>
		<h3>2014</h3>
		<div class="cont-time">
			<p><strong>2014</strong> Lorem ipsum dolor sit amet consectetur, adipisicing, elit. Vero, repudiandae, omnis reiciendis ipsum eaque dolorum debitis, voluptatibus atque dolore placeat eum. Itaque odio libero culpa, nihil amet, qui delectus voluptate?</p>
		</div>
	</div>	
</section>


<?php get_footer(); ?>