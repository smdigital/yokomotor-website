<?php get_header(); ?>
<div class="bar-return">
	<div class="wrapper-main center">
		<a href="" class="btn-return">Regresar</a>
	</div>
</div>
<section class="wrapper-main-sm center main-shop-cart-old">
	<!-- Nota: 
		Este h1 y modelo y KM 
		Aplica para mobile  -->
	<article class="hidden-lg">
		<h1>KIA CERATO PRO 1.6L</h1>
		<ul class="modelo-km">
			<li>2015</li>
			<li>53.000KM</li>
		</ul>		
	</article>
	<div class="row row-xs">
        <div class="col-12 col-sm-6 col-lg-6 col-xl-6 thumbnail-clothes">
        	<div class="row-swiper-old">
				<div class="swiper swiper-ropa">
					<div class="swiper-wrapper">
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/kia-1.jpg" alt="">
							</figure>
						</div>
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/kia-2.jpg" alt="">
							</figure>
						</div>
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/kia-3.jpg" alt="">
							</figure>
						</div>
						<div class="swiper-slide">
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/kia-4.jpg" alt="">
							</figure>
						</div>						
					</div>
				</div>    
				<div class="button-prev prev-white prev-ropa"></div>
				<div class="swiper-pagination pagination-ropa"></div>
				<div class="button-next next-white next-ropa"></div> 
        	</div>			
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-6 clothes-details">
			<article class="details-cart-old">
				<h1>KIA CERATO PRO 1.6L</h1>
				<ul class="modelo-km">
					<li>2015</li>
					<li>53.000KM</li>
				</ul>				
				<h3>$41.500.000</h3>
				<div class="clr"></div>
				<a href="javascript:;" class="btn-red-lg" data-toggle="modal" data-target="#modal-form-usados">PREGUNTAR</a>
				<a href="" class="btn-whatsapp-white">WHATSAPP</a>
				<div class="clr"></div>
				<!-- Compartir -->
				<div class="cont-share">
					<span>Compartir</span>
					<div class="redes-share">
            			<ul>
            				<li class="icon-fb"><a href="" target="_blank">facebook</a></li>
            				<li class="icon-tw"><a href="" target="_blank">twitter</a></li>
            				<li class="icon-it"><a href="" target="_blank">instagram</a></li>
            			</ul>
            		</div>
				</div>
			</article>
		</div>
	</div>
</section>
<section class="caracteristicas-cart-old full clear-fix">
	<div class="wrapper-main-sm center">
		<h6>Características principales</h6>
		<div class="row-old-table">
			<div class="tr">
				<div class="td">Marca</div>
				<div class="td">Kia</div>
			</div>
			<div class="tr">
				<div class="td">Modelo</div>
				<div class="td">CERATO PRO</div>
			</div>
			<div class="tr">
				<div class="td">Año</div>
				<div class="td">2015</div>
			</div>
			<div class="tr">
				<div class="td">Color</div>
				<div class="td">Blanco</div>
			</div>
			<div class="tr">
				<div class="td">Tipo de combustible</div>
				<div class="td">Gasolina</div>
			</div>
			<div class="tr">
				<div class="td">Puertas</div>
				<div class="td">4</div>
			</div>
			<div class="tr">
				<div class="td">Transmisión</div>
				<div class="td">Mecánica</div>
			</div>
			<div class="tr">
				<div class="td">Motor</div>
				<div class="td">1600</div>
			</div>
			<div class="tr">
				<div class="td">Tipo de Carrocería</div>
				<div class="td">Sedán</div>
			</div>
			<div class="tr">
				<div class="td">Kilómetros</div>
				<div class="td">53.000 km</div>
			</div>
		</div>
		<div class="text-center">
			<a href="javascript:;" class="btn-arrow-right" data-toggle="modal" data-target="#modal-caracteristicas">Ver más características</a>
		</div>
	</div>	
</section>	
<section class="main-mns-car full clear-fix">
	<div class="wrapper-main-sm center">
		<article>
			<h4>DESCRIPCIÓN GENERAL</h4>
			<p>Cámara de reversa, papeles al día, impuestos al día, asegurado. Poco uso, técnico mecánica hasta enero del 2022, soat hasta enero 2022, recién porcelanizado. Retrovisores abatibles. Cámara de retroceso, sensores, bloqueo central de alarma. Papeles e impuestos al día</p>			
		</article>
	</div>
</section>
<a href="javascript:;" class="btn-fixed-mns" data-toggle="modal" data-target="#modal-form-usados">
	<span>Preguntar al vendedor</span>	
</a>
<!-- Nota: 
	carros relacionados de la misma
	 marca, modelo, o valor -->
<section class="main-usados-destacados relacionados-car full clear-fix">
	<div class="wrapper-main-sm center">
		<h3>TAMBIÉN TE PUEDE INTERESAR</h3>		
		<div class="row-swiper-clothes">
			<div class="swiper swiper-clothes">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-1.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>KIA Cerato Pro 1.6L</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-2.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Mazda CX-5 Grand Touring 4x4 2.5AT</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-1.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>KIA Cerato Pro 1.6L</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-2.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Mazda CX-5 Grand Touring 4x4 2.5AT</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-2.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Mazda CX-5 Grand Touring 4x4 2.5AT</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-1.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>KIA Cerato Pro 1.6L</p>
								</div>
							</a>
						</article>
					</div>
				</div>
			</div>
			<div class="button-next next-black next-clothes"></div>
			<div class="button-prev prev-black prev-clothes"></div>
			<div class="swiper-pagination pagination-clothes"></div>
		</div>		
	</div>
</section>




<!-- Lightbox Vehiculos usados -->
<div id="modal-form-usados" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<section class="form-yokomotor lightbox-form full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<h1>PREGUNTA AL VENDEDOR</h1>
			<h6>Completa tus datos de contacto</h6>
			<hr>
		    <div class="row row-xs main-form-fieldset">
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Nombre completo</legend>
		                <input type="text" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Teléfono</legend>
		                <input type="tel" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>EMAIL</legend>
		                <input type="email" id="" name="">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>¿Cuándo quieres comprar?</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
		        </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend>Ciudad</legend>
		                <select name="" id="">
		                	<option value="">Selecciona</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
                </div>                  
		    </div>
		    <div class="checked-style checked-red">
		        <label for="acepto-terms-citas">
		            <input type="checkbox" id="acepto-terms-citas">
		            <span class="wpcf7-list-item-label">
		            	<div class="check-label"></div>
		            	Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
		            </span>	
		        </label>
		    </div>
		    <div class="clr"></div>
		    <input type="submit" value="ENVIAR">
		</section>
	</div>
</div>


<!-- Modal Caracteristicas Carro --> 
<div id="modal-caracteristicas" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox modal-center-xs">
		<section class="lightbox-usados full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<div class="modal-caracteristicas">
				<h3>DESCRIPCIÓN GENERAL</h3>
				<ul>
					<li>Dirección: Asistida</li>
					<li> Frenos ABS: Sí</li>
					<li>Aire acondicionado: Sí</li>
					<li>Alarma: Sí </li>
					<li>Bluetooth: Sí</li>
					<li>Retrovisores eléctricos: Sí</li>
					<li>GPS: Sí</li>
					<li>Reproductor de MP3: Sí</li>
					<li>Airbag para conductor y pasajero: Sí</li>
					<li>Entrada USB: Sí</li>
				</ul>
				<h3>CONFORT Y CONVENIENCIA</h3>
				<ul>
					<li>DVD: Sí</li>
					<li>Regulación de altura del volante: Sí</li>
					<li>Sensor de parqueo: Sí</li>
					<li>Vidrios eléctricos: Sí</li>
					<li>Asiento trasero abatible: Sí</li>
					<li>Apertura remota de baúl: Sí</li>
					<li>Asientos eléctricos: Sí </li>
				</ul>
				<h3>SEGURIDAD</h3>
				<ul>
					<li>Faros antinieblas delanteros: Sí</li>
					<li>Luces con regulación automática: Sí</li>
					<li>Faros antinieblas traseros: Sí </li>
					<li> Desempañador trasero: Sí</li>
					<li> Cierre centralizado de puertas: Sí</li>
				</ul>
				<h6>Características principales</h6>
				<div class="row-old-table">
					<div class="tr">
						<div class="td">Marca</div>
						<div class="td">Kia</div>
					</div>
					<div class="tr">
						<div class="td">Modelo</div>
						<div class="td">CERATO PRO</div>
					</div>
					<div class="tr">
						<div class="td">Año</div>
						<div class="td">2015</div>
					</div>
					<div class="tr">
						<div class="td">Color</div>
						<div class="td">Blanco</div>
					</div>
					<div class="tr">
						<div class="td">Tipo de combustible</div>
						<div class="td">Gasolina</div>
					</div>
					<div class="tr">
						<div class="td">Puertas</div>
						<div class="td">4</div>
					</div>
					<div class="tr">
						<div class="td">Transmisión</div>
						<div class="td">Mecánica</div>
					</div>
					<div class="tr">
						<div class="td">Motor</div>
						<div class="td">1600</div>
					</div>
					<div class="tr">
						<div class="td">Tipo de Carrocería</div>
						<div class="td">Sedán</div>
					</div>
					<div class="tr">
						<div class="td">Kilómetros</div>
						<div class="td">53.000 km</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php get_footer(); ?>