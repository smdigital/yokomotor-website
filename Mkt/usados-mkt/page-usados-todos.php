<?php get_header(); ?>
<!-- 	Banner Home web y mobile de Usados
 		Nota : necesario la imagen 
		en las  dos versiones -->

<section class="banner-home banner-usados clear-fix">
	<div class="swiper swiper-banner-home">
		<div class="swiper-wrapper">
			<!-- Slide 1 -->
			<div class="swiper-slide">				
				<figure class="hidden-xs">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-blog-gral.jpg" alt="">				
				</figure>
				<figure class="hidden-lg">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/banner-home-usado-all-xs.jpg" alt=""> 		
				</figure>
				<div class="banner-usados-all">
					<div class="details-banner-usados">				
						<h1>CARRO NOMBRE MODELO</h1>
						<h4>2015 | 53.000KM</h4>
						<a href="#" class="btn-yokomotor transparent">VER MÁS</a>
					</div>
				</div>
			</div>
			<!-- Slide 1 -->
			<div class="swiper-slide">				
				<figure class="hidden-xs">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-blog-gral.jpg" alt="">				
				</figure>
				<figure class="hidden-lg">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/banner-home-usado-all-xs.jpg" alt=""> 		
				</figure>
				<div class="banner-usados-all">
					<div class="details-banner-usados">				
						<h1>MODELO CARRO </h1>
						<h4>20158 | 35.100KM</h4>
						<a href="#" class="btn-yokomotor transparent">VER MÁS</a>
					</div>
				</div>
			</div>												
		</div>
	</div>
	<div class="next-banner button-next next-white"></div>
  	<div class="prev-banner button-prev prev-white"></div>
	<div class="swiper-pagination banner-pagination"></div>
</section>

<section class="main-cart-all full clear-fix">
	<div class="wrapper-main center">
		<div class="row-filter-cart-all full clear-fix">
			<!-- Conten Filtro  Desktop -->
			<div class="filter-cart-left">
				<div id="tabs-orderby" class="orderby-filter-tabs">
					<h3>ORDENAR POR</h3>
					<div class="content-tabs-filter">
						<ul class="items-orderby">
							<li class="active"><a href="javascript:;">Más nuevo</a></li>
							<li><a href="javascript:;">Menor precio</a></li>
							<li><a href="javascript:;">Mayor precio</a></li>
						</ul>
					</div>
				</div>
				<h3>FILTRAR POR</h3>
				<div id="tabs-filter-xl" class="tabs-filter-all">
					<!-- Filtro Ubicación -->
					<h4>Ubicación</h4>
					<div class="info-tabs-filter">
						<ul class="items-orderby">
							<li class="active"><a href="">Bogotá <span>10</span></a></li>
							<li><a href="">Medellín <span>15</span></a></li>
						</ul>
					</div>

					<!-- Filtro Marca -->
					<h4>Marca</h4>
					<div class="info-tabs-filter">
						<select name="" id="">
							<option value="">Selecciona</option>
							<option value="">Toyota</option>
							<option value="">Mazda</option>
							<option value="">Kia</option>
						</select>
					</div>

					<!-- Filtro Modelo -->
					<h4>Modelo</h4>
					<div class="info-tabs-filter">
						<select name="" id="">
							<option value="">Selecciona</option>
							<option value="">2022</option>
							<option value="">2021</option>
							<option value="">2020</option>
							<option value="">2019</option>
							<option value="">2018</option>
						</select>
					</div>

					<!-- Filtro Precio -->
					<h4>Precio</h4>
					<div class="info-tabs-filter">
						<div class="row-range">
							<div class="range-slide">
								<h6 class="val-min">$500.000</h6>
								<h6 class="val-max">$100.000.000</h6>
								<div class="slider-range"></div>
							</div>								
							<input type="text" class="val-min" id="val-min-input" placeholder="$10.000.000">
							<input type="text" class="val-max" id="val-max-input" placeholder="$100.000.000">
							<div class="clr"></div>
							<a href="javascript:;"  class="btn-aplicar">Buscar</a>
						</div>
					</div>

					<!-- Filtro Kilometraje -->
					<h4>Kilometraje</h4>
					<div class="info-tabs-filter">
						<select name="" id="">
							<option value="">Selecciona</option>
							<option value="">0 - 10.000 KM</option>
							<option value="">10.000 - 50.000 KM</option>
							<option value="">50.000 - 100.000 KM</option>
							<option value="">Mayor a 100.000 KM</option>
						</select>
					</div>

					<!-- Filtro Transmisión -->
					<h4>Transmisión</h4>					
					<div class="info-tabs-filter">
						<ul class="items-orderby">
							<li><a href="">Mecánica <span>10</span></a></li>
							<li><a href="">Automática <span>5</span></a></li>
						</ul>
					</div>
				</div>
				<a href="javascript:;"  class="btn-aplicar">Buscar</a>
				<a href="javascript:;" class="btn-clear">Limpiar Filtro</a>				
			</div>
			<!-- Conten Cart Right -->
			<div class="wrap-cart-right">				
				<div class="row row-xs">
		            <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-cart-old">
							<figure>
								<a href="/toyota/usado-vehiculo-detalle/">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/carro-all.jpg" alt="">
								</a>
							</figure>
							<div class="info">
								<h3>MAZDA 3.6 Z6</h3>
								<h4>$29.000.000</h4>
								<ul>
									<li>2015</li>
									<li>53.000km</li>									
								</ul>
								<p>Medellín, antioquia</p>
								<a href="/toyota/usado-vehiculo-detalle/" class="btn-arrow-light">Ver más</a>
							</div>
						</article>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-cart-old">
							<figure>
								<a href="/toyota/usado-vehiculo-detalle/">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/carro-all3.jpg" alt="">
								</a>
							</figure>
							<div class="info">
								<h3>KIA</h3>
								<h4>$29.000.000</h4>
								<ul>
									<li>2015</li>
									<li>53.000km</li>									
								</ul>
								<p>Medellín, antioquia</p>
								<a href="/toyota/usado-vehiculo-detalle/" class="btn-arrow-light">Ver más</a>
							</div>
						</article>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-cart-old">
							<figure>
								<a href="/toyota/usado-vehiculo-detalle/">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/carro-all2.jpg" alt="">
								</a>
							</figure>
							<div class="info">
								<h3>MAZDA 3.6 Z6</h3>
								<h4>$29.000.000</h4>
								<ul>
									<li>2015</li>
									<li>53.000km</li>									
								</ul>
								<p>Medellín, antioquia</p>
								<a href="/toyota/usado-vehiculo-detalle/" class="btn-arrow-light">Ver más</a>
							</div>
						</article>
					</div>
					
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-cart-old">
							<figure>
								<a href="/toyota/usado-vehiculo-detalle/">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/carro-all.jpg" alt="">
								</a>
							</figure>
							<div class="info">
								<h3>MAZDA 3.6 Z6</h3>
								<h4>$29.000.000</h4>
								<ul>
									<li>2015</li>
									<li>53.000km</li>									
								</ul>
								<p>Medellín, antioquia</p>
								<a href="/toyota/usado-vehiculo-detalle/" class="btn-arrow-light">Ver más</a>
							</div>
						</article>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-cart-old">
							<figure>
								<a href="/toyota/usado-vehiculo-detalle/">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/carro-all4.jpg" alt="">
								</a>
							</figure>
							<div class="info">
								<h3>MAZDA 3.6 Z6</h3>
								<h4>$29.000.000</h4>
								<ul>
									<li>2015</li>
									<li>53.000km</li>									
								</ul>
								<p>Medellín, antioquia</p>
								<a href="/toyota/usado-vehiculo-detalle/" class="btn-arrow-light">Ver más</a>
							</div>
						</article>
					</div>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-cart-old">
							<figure>
								<a href="/toyota/usado-vehiculo-detalle/">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/carro-all.jpg" alt="">
								</a>
							</figure>
							<div class="info">
								<h3>MAZDA 3.6 Z6</h3>
								<h4>$29.000.000</h4>
								<ul>
									<li>2015</li>
									<li>53.000km</li>									
								</ul>
								<p>Medellín, antioquia</p>
								<a href="/toyota/usado-vehiculo-detalle/" class="btn-arrow-light">Ver más</a>
							</div>
						</article>
					</div>
				</div>
				<div class="clr"></div>
				<!-- Pagination por defecto wordpress -->
				<nav class="pagination">
					<ul class="page-numbers">
						<li>
							<span aria-current="page" class="page-numbers current">1</span>
						</li>
						<li>
							<a href="">2</a>
						</li>
						<li>
							<a href="">3</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>	
		
	</div>
</section>

<!-- Sticky / Filtro para Mobile -->
<section class="main-filter-usados-xs full clear-fix">
	<div class="wrapper-main center">
		<div class="sticky-bar-filter-red">
			<ul>
				<li id="btn-orderby-xs" class="icon-orderby">
					<a href="javascript:;">Ordenar</a>
				</li>
				<li id="btn-filter-all-xs" class="icon-filter">
					<a href="javascript:;">Filtrar</a>
				</li>
			</ul>
		</div>

		<!-- Filtro Ordenar -->
		<div class="content-tabs-filter modal-orderby-xs">
			<div class="cerrar">x</div>
			<ul class="items-orderby">
				<li class="active"><a href="javascript:;">Más nuevo</a></li>
				<li><a href="javascript:;">Menor precio</a></li>
				<li><a href="javascript:;">Mayor precio</a></li>
			</ul>
		</div>

		<!-- Filtro general accordion completo -->
		<div class="tabs-filter-all modal-filter-all-xs content-tabs-filter">
			<div class="cerrar">x</div>
			<div id="tabs-filter-xs">
				<!-- Filtro Ubicación -->
				<h4>Ubicación</h4>
				<div class="info-tabs-filter">
					<ul class="items-orderby">
						<li class="active"><a href="">Bogotá <span>10</span></a></li>
						<li><a href="">Medellín <span>15</span></a></li>
					</ul>
				</div>

				<!-- Filtro Marca -->
				<h4>Marca</h4>
				<div class="info-tabs-filter">
					<select name="" id="">
						<option value="">Selecciona</option>
						<option value="">Toyota</option>
						<option value="">Mazda</option>
						<option value="">Kia</option>
					</select>
				</div>

				<!-- Filtro Modelo -->
				<h4>Modelo</h4>
				<div class="info-tabs-filter">
					<select name="" id="">
						<option value="">Selecciona</option>
						<option value="">2022</option>
						<option value="">2021</option>
						<option value="">2020</option>
						<option value="">2019</option>
						<option value="">2018</option>
					</select>
				</div>

				<!-- Filtro Precio -->
				<h4>Precio</h4>
				<div class="info-tabs-filter">
					<div class="row-range">
						<div class="range-slide">
							<h6 class="val-min">$500.000</h6>
							<h6 class="val-max">$100.000.000</h6>
							<div class="slider-range"></div>
						</div>								
						<input type="text" class="val-min" id="val-min-input" placeholder="$10.000.000">
						<input type="text" class="val-max" id="val-max-input" placeholder="$100.000.000">
						<div class="clr"></div>
						<a href="javascript:;"  class="btn-aplicar">Aplicar</a>
					</div>
				</div>

				<!-- Filtro Kilometraje -->
				<h4>Kilometraje</h4>
				<div class="info-tabs-filter">
					<select name="" id="">
						<option value="">Selecciona</option>
						<option value="">0 - 10.000 KM</option>
						<option value="">10.000 - 50.000 KM</option>
						<option value="">50.000 - 100.000 KM</option>
						<option value="">Mayor a 100.000 KM</option>
					</select>
				</div>

				<!-- Filtro Transmisión -->
				<h4>Transmisión</h4>					
				<div class="info-tabs-filter">
					<ul class="items-orderby">
						<li><a href="">Mecánica <span>10</span></a></li>
						<li><a href="">Automática <span>5</span></a></li>
					</ul>
				</div>
			</div>
			<a href="javascript:;"  class="btn-aplicar">Buscar</a>
			<a href="javascript:;" class="btn-clear">Limpiar Filtro</a>	
		</div>
	</div>
</section>

<?php get_footer(); ?>
<script>
  jQuery( function() {
    jQuery( ".slider-range" ).slider({
      range: true,
      min: 0,
      max: 80000000,
      values: [ 10000000, 50000000 ],
      slide: function( event, ui ) {
        jQuery( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    jQuery( "#amount" ).val( "$" + jQuery( "#slider-range" ).slider( "values", 0 ) +
      " - $" + jQuery( "#slider-range" ).slider( "values", 1 ) );
  } );
  </script>
