<?php get_header(); ?>
<!-- 	Banner Home web y mobile de Usados
 		Nota : necesario la imagen 
		en las  dos versiones -->

<section class="banner-home banner-usados clear-fix">
	<div class="swiper swiper-banner-home">
		<div class="swiper-wrapper">
			<!-- Slide 1 -->
			<div class="swiper-slide">				
				<figure class="hidden-xs">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-home-1.jpg" alt="">				
				</figure>
				<figure class="hidden-lg">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/banner-home-xs.jpg" alt=""> 		
				</figure>
				<div class="wrap-banner-usados">
					<div class="details-banner-usados">
						<h6>Usados / Medellín</h6>
						<h1>DOLOR SIT LOREM IPSUM </h1>
						<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Vel, perferendis, pariatur. Reiciendis veniam pariatur asperiores aliquam explicabo assumenda.</p>
					</div>
					<a href="#" class="btn-yokomotor transparent">VER MÁS</a>
				</div>
			</div>
			<!-- Slide 2 -->
			<div class="swiper-slide">
				<figure class="hidden-xs">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/banner-home-2.jpg" alt="">				
				</figure>
				<figure class="hidden-lg">
					<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/banner-home-xs-2.jpg" alt=""> 		
				</figure>
				<div class="wrap-banner-usados">
					<div class="details-banner-usados">
						<h6>Usados / Medellín</h6>
						<h1>DOLOR SIT LOREM IPSUM </h1>
						<p>Consectetur adipiscing elit lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
					</div>
					<a href="#" class="btn-yokomotor transparent">VER MÁS</a>
				</div>
			</div>									
		</div>
	</div>
	<div class="next-banner button-next next-white"></div>
  	<div class="prev-banner button-prev prev-white"></div>
	<div class="swiper-pagination banner-pagination"></div>
</section>
<section class="main-categoria-usados full clear-fix">
	<div class="wrapper-main center">
		<h2>NUESTRAS CATEGORÍAS</h2>	
		<div class="row-swiper-usados relative center">
			<div class="swiper swiper-categoria-usados">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<div class="icon-categoria-usados">
							<a href="/toyota/usados-todos/">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/surface1.svg" alt="">
								</i>
								<h6>Automóviles</h6>
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="icon-categoria-usados">
							<a href="#">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/surface2.svg" alt="">
								</i>
								<h6>SUV</h6>
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="icon-categoria-usados">
							<a href="#">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/surface3.svg" alt="">
								</i>
								<h6>PickUps</h6>
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="icon-categoria-usados">
							<a href="#">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/surface4.svg" alt="">
								</i>
								<h6>Utilitarios</h6>
							</a>
						</div>
					</div>
					<div class="swiper-slide">
						<div class="icon-categoria-usados">
							<a href="#">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/surface2.svg" alt="">
								</i>
								<h6>Otros</h6>
							</a>
						</div>
					</div>					
				</div>
			</div>
			<div class="swiper-pagination pagination-bullet pagination-categoria-usados"></div>
		</div>
	</div>
</section>

<section class="main-usados-destacados full clear-fix">
	<div class="wrapper-main-sm center">
		<h3>VEHÍCULOS DESTACADOS</h3>
		<div class="text-center hidden-lg">
			<a href="" class="btn-arrow-right">Ver todos</a>
		</div>
		<div class="row-swiper-clothes">
			<div class="swiper swiper-clothes">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="/toyota/usado-vehiculo-detalle/">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-1.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>KIA Cerato Pro 1.6L</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="/toyota/usado-vehiculo-detalle/">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-2.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Mazda CX-5 Grand Touring 4x4 2.5AT</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="/toyota/usado-vehiculo-detalle/">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-1.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>KIA Cerato Pro 1.6L</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="/toyota/usado-vehiculo-detalle/">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-2.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Mazda CX-5 Grand Touring 4x4 2.5AT</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="/toyota/usado-vehiculo-detalle/">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-2.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>Mazda CX-5 Grand Touring 4x4 2.5AT</p>
								</div>
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="cart-usado-destacado">
							<a href="/toyota/usado-vehiculo-detalle/">
								<figure>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/usado-1.jpg" alt="">
								</figure>
								<h6>$000.000</h6>
								<div class="figcaption">
									<p>KIA Cerato Pro 1.6L</p>
								</div>
							</a>
						</article>
					</div>
				</div>
			</div>
			<div class="button-next next-black next-clothes"></div>
			<div class="button-prev prev-black prev-clothes"></div>
			<div class="swiper-pagination pagination-clothes"></div>
		</div>
		<div class="text-center hidden-xs">
			<a href="" class="btn-arrow-right">Ver todos</a>
		</div>
	</div>
</section>

<section class="main-marcas-usados full clear-fix">
	<div class="wrapper-main-sm center">
		<h3>PRINCIPALES MARCAS</h3>
		<div class="relative carrousel-marcas center">
			<div class="swiper swiper-marcas-circle">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="marcas-circle">
							<a href="">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/marca-toyota.png" alt="">
								</i>				
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="marcas-circle">
							<a href="">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/kia.png" alt="">
								</i>				
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="marcas-circle">
							<a href="">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/Mazda-Logo.png" alt="">
								</i>				
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="marcas-circle">
							<a href="">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/Renault-Logo.png" alt="">
								</i>				
							</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="marcas-circle">
							<a href="">
								<i>
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/chevrolet.png" alt="">
								</i>				
							</a>
						</article>
					</div>
				</div>
			</div>
			<div class="swiper-pagination pagination-bullet pagination-marcas"></div>
		</div>
	</div>
</section>
<section class="main-usados-pqrs full clear-fix">
	<div class="wrapper-main center">
		<h3>PQRS</h3>
		<div class="banner-pqrs full clear-fix" style="background: url('<?php echo get_stylesheet_directory_uri(). '/library/' ?>/images/usados/bg-pqrs.jpg') no-repeat center center;">
			<article>
				<h4>¿TIENES PREGUNTAS, QUEJAS, RECLAMOS O SOLICITUDES?</h4>
				<a href="#" class="btn-yokomotor-arrow">VER MÁS</a>
			</article>
		</div>
	</div>
</section>

<!-- Nota : necesario la imagen 
	en las  dos versiones -->
<section class="main-usados-nuevos full clear-fix">
	<figure class="hidden-xs">
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/vehiculos-nuevos-xl.jpg" alt="">				
	</figure>
	<figure class="hidden-lg">
		<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/usados/vehiculos-nuevos-xs.jpg" alt="">		
	</figure>
	<article>
		<h2>VEHÍCULOS NUEVOS</h2>
		<a href="#" class="btn-yokomotor transparent">VER MÁS</a>
	</article>
</section>

<?php get_footer(); ?>
