[![SM Digital](https://smdigital.com.co/wp-content/themes/sm-digital/library/images/sm-digital-logo.png)](https://smdigital.com.co/)

# Yokomotor Sitio Web

Sitio web a la medida **MULTISITE** construído a modo de plantilla(bones) compactible con el CMS WordPress.

**Colaboradores**

 - **Cristian Marín**: ccmarin@smdigital.com.co  **(Maquetación)**
 - **Cristian Torres**: catorres@smdigital.com.co **(Programación)**
 - **Stiwar Alzate Escobar**: bsalzate@smdigital.com.co **(Programación)**


**Versiones**

* Tags: comments, post, theme, wordpress theme, bones, responsive
* Versión estable: 5.8.2 WP

**Dependencias de desarrollo*
- Bootstrap 4
- NPM (Versión 7+)
- Grunt CLI (Versión 1.3.X+) 

**Dependencias JS**
- jQuery 3.6.0
- jquery.dlmenu.js v1.0.1
- jQuery UI v1.12.1
- Swiper 6.5.4
- Modernizr 3.6.0
- Prefixfree 1.0.7
- AOS js

**Plugins**
- ACF Archive
- Advanced Custom Fields
- Advanced Custom Fields: Repeater Field
- Ajax Load More
- Ajax Load More: Pro
- Branda Pro
- Contact Form 7
- Contact Form CFDB7
- Defender Pro
- Hummingbird Pro
- Smush Pro
- WP Mail SMTP
- WPMU DEV Dashboard


### Instalación

Para su instalación, realice los siguientes pasos:

1. Descargue el repositorio `https://bitbucket.org/smdigital/yokomotor-website/`, con este comando 
git clone git@bitbucket.org:smdigital/yokomotor-website.git --branch development
2. Cambie a la rama `development`.
3. Instale las dependencias de desarrollo ubicadas en el directorio `library/grunt`.
4. Ejecute las siguientes tareas programadas conforme a su necesidad de desarrollo:
    - `grunt watch`: Compila y minifica automáticamente los archivos de estilo y Scripts ubicados en el directorio `library/scss` y `library/js` respectivamente.
    - Los estilos están organizados por archivos ***SCSS*** los cuales vienen con sus importaciones por defecto y definiciones para todo el sitio.
    - Los resultados finales de la compilación y minificación de los scripts se encuentran en `library/css` y `library/js`.

### ¿tiene preguntas?

Recurra a cualquier miembro de los colaboradores listados al inicio de la sección.

<!-- ### Changelog

 - 0.1.0 - (Release) Versión inicial, Release de la plantilla maquetada.
 - 1.0.0 - (Improvement) Programación del home, menú y estructuras básicas generales del sitio.  -->