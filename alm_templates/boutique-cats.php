
<?php 

  
$taxterms = wp_get_post_terms( get_the_ID(), 'boutique_cats', array( 'fields' => 'names' ));

if( function_exists( 'get_field' ) ){
	/**
		 * * ***************
		* ACF Custom fields Boutique (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_color'
		* 
		*/
		$detailsSection = get_field('yokomotor_details', get_the_ID());
}
?>

<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
  <article class="card-ropa">
    <figure>
      <a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>">
        <?php 
        	if ( has_post_thumbnail() ) { 
            the_post_thumbnail( 'yokomotor-thumb-160',[ 'alt' => 'Img boutique '.get_the_title() ] ); 
          }
        ?>
      </a>
    </figure>
    <div class="info">
      <?php  
        $index = 1; 
        if($taxterms && ! empty ( $taxterms )): ?>
          <h3>
            <?php foreach ( $taxterms as $key => $taxterm ): ?>
              <?php echo $taxterm; echo ( $index < count($taxterms) )? ", " : " "; ?>
            <?php $index ++; endforeach; ?> 
          </h3>
			  <?php 
      endif; 
      if( $detailsSection['price'] && !empty($detailsSection['price']) ): ?>
        <h4>$<?php echo $detailsSection['price']; ?></h4>
      <?php endif; 
       the_title('<p>', '</p>'); ?>
      <a href="<?php echo esc_url( get_permalink( get_the_ID() ) ); ?>" class="btn-arrow-light">Adquirir</a>
    </div>
  </article>
</div>