<?php $postid = get_the_ID(); 
$taxterms = wp_get_post_terms( get_the_ID(), 'blog_cats', array('hide_empty' => false, 'fields' => 'names' )); ?>

<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
  <article class="card-blog">
    <figure>
      <a href="<?php echo get_permalink($postid);?>">
        <img src="<?php echo get_the_post_thumbnail_url($postid, 'full');?>" alt="">
      </a>
    </figure>
    <div class="details-blog">
      <?php $index = 1;
        if( $taxterms && !empty($taxterms) ): ?>
          <h4>
            <?php foreach ( $taxterms as $key => $taxterm ): ?>
              <?php echo $taxterm; echo ( $index < count($taxterms) )? ", " : " "; ?>
            <?php $index ++; endforeach; ?> 
          </h4>
        <?php endif; 
        the_title('<h3>', '</h3>');
        if( has_excerpt() ): ?>
					<p><?php the_excerpt(); ?></p>					
				<?php endif; ?>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" class="btn-yokomotor-light">LEER MÁS</a>
    </div>
  </article>
</div>