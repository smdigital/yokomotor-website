<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
	<article class="card-information campaign-small">
		<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
			<figure>
			<?php 
				if ( has_post_thumbnail() ) { 
					the_post_thumbnail( 'yokomotor-thumb-347',[ 'alt' => 'Img campaña '.get_the_title() ] ); 
				}
			 ?>
			</figure>
			<div class="overflow">
				<?php the_title('<h3>', '</h3>');
				if( has_excerpt() ): ?>
					<p><?php the_excerpt(); ?></p>					
				<?php endif; ?>
				<div class="btn-yokomotor-arrow">Ver más</div>
			</div>
		</a>
	</article>
</div>