<?php 
    $post_id = get_the_ID();
    $price	=	get_field('price', $post_id);
    $km		=	get_field('km', $post_id);
    $year	=	get_field('year', $post_id);
    $cityTaxonomies = get_the_terms($post_id, 'city_cats');
?>
<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
    <article class="card-cart-old">
    <?php if(has_post_thumbnail($post_id)): ?>
        <figure>
            <a href="<?php echo get_permalink($post_id);?>">
                <img src="<?php echo esc_url(get_the_post_thumbnail_url($post_id, 'full')) ?>" alt="<?php echo esc_attr(get_the_title($post_id)); ?>">
            </a>
        </figure>
    <?php endif; ?>
        <div class="info">
            <h3><?php echo strtoupper(get_the_title($post_id)); ?></h3>
            <h4>$<?php echo number_format($price, 0, '.', '.');?></h4>
            <ul>
                <li><?php echo $year; ?></li>
                <li><?php echo number_format($km, 0, '.', '.');?>km</li>									
            </ul>
            <p><?php foreach($cityTaxonomies as $city){echo $city->name;} ?></p>
            <a href="<?php echo get_permalink($post_id);?>" class="btn-arrow-light">Ver más</a>
        </div>
    </article>
</div>