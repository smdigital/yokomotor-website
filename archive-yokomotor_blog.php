<?php get_header(); ?>

<section class="main-blog full clear-fix">
	<div class="wrapper-main center">
		<h1><?php _e('BLOG', 'yokomotor');?></h1>
		<?php 
		if( function_exists( 'get_field' ) ):
			/**
				 * * ***************
				* ACF Custom fields Archive Blog
				* ***************
				* @param ACF_fields 'yokomotor_description'
					* @param ACF_subfields 'enable_section'
					* @param ACF_subfields 'description'
				* 
			*/
			$sectionLegend = get_field('yokomotor_description', 'yokomotor_blog');

			if($sectionLegend && $sectionLegend['enable_section']):
		?>
				<article class="leyeng-blog">
					<p><?php echo $sectionLegend['description'];?></p>
				</article>

			<?php 
			endif;
		endif; 
		?>

		<div id="tabs-tags-blog">
			<ul class="titles-tags">
				<li><a href="javascript:;" id="all">Todos</a></li>
				<?php 
					$args = array(
						'taxonomy' => 'blog_cats',
						'hide_empty' => false,
						'orderby' => 'id',
						'order' => 'DESC',
						'view_all_label' => 'all',
						);
					
					$blogs = get_terms( $args );

					if($blogs): 
						foreach($blogs as $blog): 
				?>

				<li><a href="javascript:;" id="<?php echo esc_attr($blog->slug); ?>"><?php echo esc_attr($blog->name); ?></a></li>

				<?php 
						endforeach;
					endif; 
				?>
			</ul>
			<?php echo do_shortcode('[ajax_load_more id="1645724265" loading_style="infinite skype" container_type="div" theme_repeater="template-blogs.php" post_type="yokomotor_blog" posts_per_page="6" taxonomy="blog_cats" taxonomy_terms="" taxonomy_operator="IN" transition_container_classes="row row-xs"]');?>
		</div>
	</div>
	
</section>




<?php get_footer(); ?>