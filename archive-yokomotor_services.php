<?php get_header(); ?>
<div class="main-service-workshop full clear-fix">
	<h1><?php _e('Servicios del taller', 'yokomotor')?></h1>
	<div class="row-workshop">
	<?php 
		$args = [
				'post_type'      => 'yokomotor_services',
				'posts_per_page' => -1,
				'order'          => 'ASC'
			];
			
		$services = get_posts( $args );

		if($services): 
			foreach($services as $service): 
			
	?>
		
		<section class="card-workshop-service full clear-fix">
			<div class="row row-xs center-vertical">
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 img-featured">
					<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
						<img src="<?php echo get_the_post_thumbnail_url($service->ID, 'full'); ?>" alt="">
					</figure>
	            </div>
	            <div class="col-12 col-sm-6 col-lg-6 col-xl-6 details-workshop">
	            	<h2><?php echo get_the_title($service->ID); ?></h2>
					<?php echo get_the_content($service->ID);?>
	            	<div class="text-center">
	            		<a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-form-serivicio" onclick="getServiceName('<?php echo get_the_title($service->ID);?>')"><?php _e('Solicitar servicio', 'yokomotor'); ?></a>
	            	</div>
	            </div>
	        </div>
		</section>

	<?php 
			endforeach; wp_reset_postdata();
		endif; 
	?>
	</div>	
</div>

<!-- Lightbox Solicitar servicio -->
<div id="modal-form-serivicio" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<form action="" id="form-services-lightbox" name="form-services-lightbox" data-parsley-validate>
		<section class="form-yokomotor lightbox-form full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<hr>
			<h3><?php _e('Servicio', 'yokomotor'); ?></h3>
			<h3 id="service_name"></h3>
		    <div class="row row-xs main-form-fieldset">
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><?php _e('Nombre completo', 'yokomotor'); ?></legend>
		                <input type="text" id="fullname" name="fullname" data-parsley-pattern="[a-zA-ZñÑáéíóúÁÉÍÓÚ ]*" required>
		                <input type="hidden" id="service" name="service">
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><?php _e('Teléfono', 'yokomotor'); ?></legend>
		                <input type="tel" id="phone" name="phone" data-parsley-type="digits"  data-parsley-length="[10, 12]"  maxlength="10" minlength="10" onkeyup="this.value=this.value.replace(/[^\d]/,'')" required>
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><?php _e('Email', 'yokomotor'); ?></legend>
		                <input type="email" id="email" name="email" data-parsley-type="email" required>
		            </fieldset>
		        </div>
		        <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><?php _e('¿Cuándo quieres comprar?', 'yokomotor'); ?></legend>
		                <select name="" id="date" required>
		                	<option value="">Selecciona</option>
		                	<option value="algo">-------</option>
		                	<option value="">-------</option>
		                	<option value="">-------</option>
		                </select>
		            </fieldset>
		        </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><?php _e('Ciudad', 'yokomotor'); ?></legend>
		                <select name="cities" id="cities" required>
		                	<option value=""><?php _e('Selecciona', 'yokomotor'); ?></option>
		                	<?php 
							
							$cities = get_field('yokomotor_workshop', 'yokomotor_services');

							if($cities):
								foreach($cities as $city):
									$name = $city['city'];
							?>
							<option value="<?php echo $name;?>"><?php echo $name; ?></option>

							<?php 
								endforeach;
							endif;
							?>
		                </select>
		            </fieldset>
                </div>
                <div class="col-12 col-sm-6 col-lg-6 col-xl-6">
					<fieldset>
		                <legend><?php _e('Sede', 'yokomotor'); ?></legend>
		                <select name="workshop" id="workshop" required>
		                	<option value=""><?php _e('Selecciona', 'yokomotor'); ?></option>
		                </select>
		            </fieldset>
                </div>  
		    </div>
		    <div class="checked-style checked-red">
		        <label for="acepto-terms-citas">
		            <input type="checkbox" id="acepto-terms-citas" value="1" required>
		            <div class="check-label"></div>
		            Autorizo a Yokomotor el manejo de mis datos personales de acuerdo a las <a href="" target="_blank">políticas de tratamientos de información de la empresa.</a>
		        </label>
		    </div>
		    <div class="clr"></div>
		    <input type="submit" id="submit-services" value="ENVIAR">
			<div class="clr"></div>
			<span class="message"></span>
		</section>
		</form>
	</div>
</div>




<?php get_footer(); ?>