		<footer class="full clear-fix">
			<div class="wrapper-main center">
				<?php if ( has_nav_menu('nosotros-links') ):	?>
				<article class="col-nosotros">
					<h2><?php _e('NOSOTROS', 'yokomotor'); ?></h2>
					<?php wp_nav_menu( array(
							'theme_location' => 'nosotros-links',
							'container' => false,
							'items_wrap' => '<ul>%3$s</ul>'
						));
					?>
				</article>
				<?php endif;?>
				<?php $sedesPageId = get_page_by_path('sedes');
				
				if ( $sedesPageId && isset($sedesPageId->ID) ): ?>
					<article class="col-sedes">
						<h2><?php _e('SEDES', 'yokomotor'); ?></h2>
							<div id="tabs-sedes" class="sedes-tabs">
								<ul>
									<?php 
									
										if( function_exists( 'get_fields' ) ):
										/**
										 * * ***************
										* ACF Custom fields Sedes (Page)
										* ***************
										* @param ACF_fields 'yokomotor_workshops'
											* @param ACF_subfields 'workshops' - Repeater
												* @param ACF_subfields 'city'
												* @param ACF_subfields 'workshop_info' - Repeater
													* @param ACF_subfields 'name'
													* @param ACF_subfields 'address'
													* @param ACF_subfields 'phone'
													* @param ACF_subfields 'image'
													* @param ACF_subfields 'url_googlemaps'
													* @param ACF_subfields 'url_waze'
													* @param ACF_subfields 'schedule'
										* 
										*/
										$sedesFields = get_fields($sedesPageId);
										$workshopFields = $sedesFields['yokomotor_workshops'];
										$workshops = $workshopFields['workshops'];
										$noAllowed = array("á", "é", "í", "ó", "ú", "Á", "É", "Í". "Ó", "Ú");
										$allowed = array("a", "e", "i", "o", "u");
										if(($workshops)):
											foreach($workshops as $workshop):
												if(!empty($workshop['city'])):
									?>
										<li><a href="#<?php echo str_replace($noAllowed, $allowed, strtolower($workshop['city']));?>"><?php echo $workshop['city'];?></a></li>
									<?php 
												endif;
											endforeach;
									?>
								</ul>
								<?php  
										foreach($workshops as $workshop): 
											$title = $workshop['city'];
											$workshop_info = $workshop['workshop_info']; 
								?>
								<div id="<?php echo str_replace($noAllowed, $allowed, strtolower($title));?>" class="content-tabs">
									<div class="sedes-accordion">
										<?php 
										foreach($workshop_info as $info): 
											$name = $info['name']; 
											$address = $info['address']; 
											$phone = $info['phone']; 
											if(isset($name)):
										?>
											<h3><?php echo $name;  ?></h3>
										<?php 
											endif;
										?>
											<div class="content-accordion">
												<?php echo (!empty($address)) ? $address.'<br>' : '';?>
												<?php echo (!empty($phone)) ?  'Teléfono: '.$phone : ''; ?>
											</div>
										<?php 
										endforeach;
										?>
									</div>
								</div>	
								<?php 	
										endforeach;
									endif;
								endif; 
								?>
							</div>
					</article>
				<?php endif; ?>
				<article class="col-suscribete">
					<h2><?php _e('SUSCRÍBETE', 'yokomotor'); ?></h2>
						<?php $postq = get_post(); echo $postq->ID != 1 ? do_shortcode('[contact-form-7 id="1622" title="Formulario suscríbete"]') : do_shortcode('[contact-form-7 id="220" title="Formulario suscríbete"]');	?>
				</article>
				<article class="col-mapa">
					<?php 
						if ( has_nav_menu('footer-links') ):
							wp_nav_menu( array(
								'theme_location' => 'footer-links',
								'container' => false,
								'items_wrap'      => '%3$s',
								'walker' => new Footer_Walker_Nav_Menu
							));
						endif;
					?>
					<?php 
						if ( has_nav_menu('social-links') ):
							wp_nav_menu( array(
								'theme_location' => 'social-links',
								'container' => 'div',
								'container_class' => 'redes'
							));
						endif;
					?>
					<div class="new-brand-footer full clear-fix">
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/2.png" alt="">
						</figure>
						<figure>
							<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/marcas/5.png" alt="">
						</figure>
					</div>
				</article>
			</div>
			<?php if ( has_nav_menu('tyc-links') ):	?>
			<div class="col-terms full clear-fix">
				<?php wp_nav_menu( array(
						'theme_location' => 'tyc-links',
						'container' => false,
						'items_wrap' => '<ul>%3$s</ul>'
					));
				?>
			</div>
			<?php endif;?>

			
			<h6> © <?php echo date('Y'); ?> Yokomotor Todos los Derechos Reservados <a href="https://www.smdigital.com.co/" class="pl-1" target="_blank">
				<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/bysmdigital.png" alt="Logo SM Digital">
			</a></h6>
		</footer>
		<?php 
			$blogs = get_current_blog_id();
			switch ($blogs): 
			case '3': switch_to_blog(3);?>

				<?php  if ( has_nav_menu('fixed-button') ):	?>
	       <div class="share-icon-yokomotor animate__animated animate__fadeInDown animate__delay-2s"> 
				<?php 
					 wp_nav_menu( array(
					'theme_location' => 'fixed-button',
					'container' => false,
				 	'link_before' => '<span>',
					'link_after' => '</span>',
				 	'items_wrap'      => '<ul class="menu-iconos-flotantes">%3$s</ul>',
					'walker' => new FixedButton_Walker_Nav_Menu  )); ?>
			
			 <div id="icon-share-mns" class="share-fixed"></div> 
		</div>
		<?php endif; ?>
		<?php endswitch; ?>
		
		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>

	</body>

</html> <!-- end of site. what a ride! -->