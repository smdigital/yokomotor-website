<?php
/*
Author: Eddie Machado
URL: http://themble.com/bones/

This is where you can drop your custom functions or
just edit things like thumbnail sizes, header images,
sidebars, comments, etc.
*/

// LOAD BONES CORE (if you remove this, the theme will break)
require_once( 'library/bones.php' );
// Mobile Detect Library
require_once( 'library/includes/Mobile_Detect.php');
// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
// require_once( 'library/admin.php' );

/*********************
LAUNCH BONES
Let's get everything up and running.
*********************/

function bones_ahoy() {

  //Allow editor style.
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );

  // let's get language support going, if you need it
  load_theme_textdomain( 'bonestheme', get_template_directory() . '/library/translation' );

  // USE THIS TEMPLATE TO CREATE CUSTOM POST TYPES EASILY
  require_once( 'library/custom-post-type.php' );

  // launching operation cleanup
  add_action( 'init', 'bones_head_cleanup' );
  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // remove WP version from RSS
  add_filter( 'the_generator', 'bones_rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'bones_gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
  // ie conditional wrapper

  // launching this stuff after theme setup
  bones_theme_support();

  // adding sidebars to Wordpress (these are created in functions.php)
  add_action( 'widgets_init', 'bones_register_sidebars' );

  // cleaning up random code around images
  add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

// let's get this party started
add_action( 'after_setup_theme', 'bones_ahoy' );


/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
	$content_width = 680;
}

/************* THUMBNAIL SIZE OPTIONS *************/

// Thumbnail sizes
add_image_size( 'bones-thumb-600', 600, 150, true );
add_image_size( 'bones-thumb-300', 300, 100, true );
add_image_size( 'yokomotor-thumb-130', 130, 80, true );
add_image_size( 'yokomotor-thumb-347', 347, 200, true );
add_image_size( 'yokomotor-thumb-495', 495, 215, true );
add_image_size( 'yokomotor-thumb-535', 535, 340, true );
add_image_size( 'yokomotor-thumb-140', 140, 140, true );
add_image_size( 'yokomotor-thumb-160', null, 160, true );
add_image_size( 'yokomotor-thumb-150', 194, 150, true );



/*
to add more sizes, simply copy a line from above
and change the dimensions & name. As long as you
upload a "featured image" as large as the biggest
set width or height, all the other sizes will be
auto-cropped.

To call a different size, simply change the text
inside the thumbnail function.

For example, to call the 300 x 100 sized image,
we would use the function:
<?php the_post_thumbnail( 'bones-thumb-300' ); ?>
for the 600 x 150 image:
<?php the_post_thumbnail( 'bones-thumb-600' ); ?>

You can change the names and dimensions to whatever
you like. Enjoy!
*/

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );

function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'bones-thumb-600' => __('600px by 150px'),
        'bones-thumb-300' => __('300px by 100px'),
        'yokomotor-thumb-130' => __('130px by 80px'),
        'yokomotor-thumb-347' => __('347px by 200px'),
        'yokomotor-thumb-495' => __('495px by 215px'),
        'yokomotor-thumb-535' => __('535px by 340px'),
        'yokomotor-thumb-140' => __('140px by 140px'),
        'yokomotor-thumb-160' => __('auto by 160px'),
        'yokomotor-thumb-150' => __('194px by 150px'),
    ) );
}

/*
The function above adds the ability to use the dropdown menu to select
the new images sizes you have just created from within the media manager
when you add media to your content blocks. If you add more image sizes,
duplicate one of the lines in the array and name it according to your
new image size.
*/

/************* AJAX PHP *********************/

require_once( 'library/ajax.php' );

// Register path ajax file
function theme_enqueue_scripts() {
	wp_localize_script('jquery', 'url', [ 'ajax_url' => admin_url('admin-ajax.php'), 'nonce' => wp_create_nonce( 'ajax_nonce' ), 'carBrands' => get_stylesheet_directory_uri() . '/library/includes/makes.json', 'carModels' => get_stylesheet_directory_uri() . '/library/includes/models.json', 'cities' => get_stylesheet_directory_uri() . '/library/includes/city-CO.json' ] );
	wp_enqueue_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );



/************* THEME CUSTOMIZE *********************/

/* 
  A good tutorial for creating your own Sections, Controls and Settings:
  http://code.tutsplus.com/series/a-guide-to-the-wordpress-theme-customizer--wp-33722
  
  Good articles on modifying the default options:
  http://natko.com/changing-default-wordpress-theme-customization-api-sections/
  http://code.tutsplus.com/tutorials/digging-into-the-theme-customizer-components--wp-27162
  
  To do:
  - Create a js for the postmessage transport method
  - Create some sanitize functions to sanitize inputs
  - Create some boilerplate Sections, Controls and Settings
*/

function bones_theme_customizer($wp_customize) {
  // $wp_customize calls go here.
  //
  // Uncomment the below lines to remove the default customize sections 

  // $wp_customize->remove_section('title_tagline');
  // $wp_customize->remove_section('colors');
  // $wp_customize->remove_section('background_image');
  // $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');
  
  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  // $wp_customize->get_section('background_image')->title = __( 'Images' );
}

add_action( 'customize_register', 'bones_theme_customizer' );


/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
function bones_register_sidebars() {
	 /* register_sidebar(array(
	 	'id' => 'sidebar-logo',
	 	'name' => __( 'Sidebar logos', 'yokomotor' ),
	 	'description' => __( 'The first (primary) sidebar.', 'yokomotor' ),
	 	'before_widget' => '<div id="%1$s" class="logo-yokomotor">',
    'after_widget'  => '</div>',
	 	'before_title' => '',
	 	'after_title' => '',
	 )); */

	/*
	to add more sidebars or widgetized areas, just copy
	and edit the above sidebar code. In order to call
	your new sidebar just use the following code:

	Just change the name to whatever your new
	sidebar's id is, for example:

	register_sidebar(array(
		'id' => 'sidebar2',
		'name' => __( 'Sidebar 2', 'bonestheme' ),
		'description' => __( 'The second (secondary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));

	To call the sidebar in your template, you can just copy
	the sidebar.php file and rename it to your sidebar's name.
	So using the above example, it would be:
	sidebar-sidebar2.php

	*/
} // don't remove this bracket!


/************* COMMENT LAYOUT *********************/

// Comment Layout
function bones_comments( $comment, $args, $depth ) {
   $GLOBALS['comment'] = $comment; ?>
  <div id="comment-<?php comment_ID(); ?>" <?php comment_class('cf'); ?>>
    <article  class="cf">
      <header class="comment-author vcard">
        <?php
        /*
          this is the new responsive optimized comment image. It used the new HTML5 data-attribute to display comment gravatars on larger screens only. What this means is that on larger posts, mobile sites don't have a ton of requests for comment images. This makes load time incredibly fast! If you'd like to change it back, just replace it with the regular wordpress gravatar call:
          echo get_avatar($comment,$size='32',$default='<path_to_url>' );
        */
        ?>
        <?php // custom gravatar call ?>
        <?php
          // create variable
          $bgauthemail = get_comment_author_email();
        ?>
        <img data-gravatar="http://www.gravatar.com/avatar/<?php echo md5( $bgauthemail ); ?>?s=40" class="load-gravatar avatar avatar-48 photo" height="40" width="40" src="<?php echo get_template_directory_uri(); ?>/library/images/nothing.gif" />
        <?php // end custom gravatar call ?>
        <?php printf(__( '<cite class="fn">%1$s</cite> %2$s', 'bonestheme' ), get_comment_author_link(), edit_comment_link(__( '(Edit)', 'bonestheme' ),'  ','') ) ?>
        <time datetime="<?php echo comment_time('Y-m-j'); ?>"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php comment_time(__( 'F jS, Y', 'bonestheme' )); ?> </a></time>

      </header>
      <?php if ($comment->comment_approved == '0') : ?>
        <div class="alert alert-info">
          <p><?php _e( 'Your comment is awaiting moderation.', 'bonestheme' ) ?></p>
        </div>
      <?php endif; ?>
      <section class="comment_content cf">
        <?php comment_text() ?>
      </section>
      <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
    </article>
  <?php // </li> is added by WordPress automatically ?>
<?php
} // don't remove this bracket!

/**
 * Use jQuery to add a word counter to the excerpt box
 *
 * Should attach to all post screens and indicate the number of words just below the #excerpt textarea
 */
function gv_excerpt_word_count_js() {
  echo '
 <script>jQuery(document).ready(function(){
jQuery("#postexcerpt #excerpt").after("Contador de Palabras: <strong><span id=\'excerpt-word-count\'></span></strong> - <span style=\'color:red;\'>Se <strong>RECOMIENDA</strong> textos cortos con palabras claves, para no afectar la maquetación del bloque.</span>");
 jQuery("#excerpt-word-count").html(jQuery("#excerpt").val().split(/\S+\b[\s,\.\'-:;]*/).length - 1);
 jQuery("#excerpt").keyup( function() {
    jQuery("#excerpt-word-count").html(jQuery("#excerpt").val().split(/\S+\b[\s,\.\'-:;]*/).length - 1);
    console.log(jQuery("#excerpt").val().split(/\S+\b[\s,\.\'-:;]*/).length - 1);
 });
});</script>
';
}
add_action( 'admin_head-post.php', 'gv_excerpt_word_count_js');
add_action( 'admin_head-post-new.php', 'gv_excerpt_word_count_js');


/**
 * Modification of "Build a tree from a flat array in PHP"
 *
 * Authors: @DSkinner, @ImmortalFirefly and @SteveEdson
 *
 * @link https://stackoverflow.com/a/28429487/2078474
 */
if( !function_exists( 'yokomotor_build_category_tree' ) ) {
  function yokomotor_build_category_tree( array &$elements, $parentId = 0 ){
    $branch = array();
    foreach ( $elements as &$element ){
      if ( $element->menu_item_parent == $parentId ){
        $children = yokomotor_build_category_tree( $elements, $element->ID );
        if ( $children ) $element->childrens = $children;
        $branch[$element->ID] = $element;
        unset( $element );
      }
    }
    return $branch;
  }
}


/**
 * Modifiy url Post Types
 */
function filter_post_type_link($link, $post){

  $postype = $post->post_type;
  
 switch ($postype) {
    case 'yokomotor_vehicles':
      $taxonomyTerms = 'vehicles_cats';
      $cats = get_the_terms($post->ID, $taxonomyTerms);
    break;
    case 'yokomotor_used':
      $taxonomyTerms = 'used_cats';
      $cats = get_the_terms($post->ID, $taxonomyTerms);
      if(is_array($cats) && count($cats) > 0){foreach($cats as $cat){if($cat->parent == 0){$term = array($cat); $cats = $term;}}}
    break;
    case 'yokomotor_boutique':
      $taxonomyTerms = 'boutique_cats';
      $cats = get_the_terms($post->ID, $taxonomyTerms);
    break;
    case 'yokomotor_accessory':
      $taxonomyTerms = 'accessory_cats';
      $cats = get_the_terms($post->ID, $taxonomyTerms);
    break;
    default:
      return $link; 
    break;
  }

  /*  if( $postype == 'yokomotor_vehicles' || $postype == 'yokomotor_boutique' ) {
    $taxonomyTerms = ($postype === 'yokomotor_vehicles') ? 'vehicles_cats' : 'boutique_cats';
    $cats = get_the_terms($post->ID, $taxonomyTerms);
    var_dump($cats);
  } else {
    return $link; 
  } 

      */
   if ( $cats ) {

    $link = str_replace('%'.$taxonomyTerms.'%', array_pop($cats)->slug, $link);
  } else {
    $link = str_replace('%'.$taxonomyTerms.'%',  '', $link);
  } 

  return $link;
} 
add_filter('post_type_link', 'filter_post_type_link', 10, 2); 

/**
 * 
 * 'yokomotor_get_back_button_main'
 * 
 * Enable user to return tu previous visited URL 
 * @description Gets Last Referred URL with a default redirection 'homepage'
 * 
 * @see It can be changed using 'yokomotor_get_back_button_default_callback' action
 *
 * @return void
 */
if( ! function_exists( 'yokomotor_get_back_button_main' ) ) {
  function yokomotor_get_back_button_main() {

    $prev_url = wp_get_referer();
    
    // Get previous visited page
    //$context = get_queried_object(); 
    
   /*  $prev_url = false;
    if ( is_a( $context, 'WP_Term' ) ) {
      if( !$context->parent == 0 ) {
        $termAncestors = get_ancestors( $context->term_id, $context->taxonomy );
              var_dump($context);
        
        if( is_array( $termAncestors ) && count( $termAncestors ) > 0 && isset( $termAncestors[ ( count( $termAncestors ) - 1 ) ] ) )
          $prev_url = get_term_link( $termAncestors[ ( count( $termAncestors ) - 1 ) ], $context->taxonomy );
      }

    } else if( is_a( $context, 'WP_Post') && $context->post_type == 'product' ) {
      $termAncestors = wp_get_post_terms( $context->ID, 'product_cat', array( 'orderby' => 'parent', 'order' => 'DESC', 'fields' => 'ids' ) );
      if( is_array( $termAncestors ) && count( $termAncestors ) > 0 && isset( $termAncestors[ ( count( $termAncestors ) - 1 ) ] ) )
      $prev_url = get_term_link( $termAncestors[ ( count( $termAncestors ) - 1 ) ], $context->taxonomy );
    }
    
    if( !$prev_url ) {
        $prev_url = site_url();
    } */

    /**
     * Extension - Add custom URL callback
     */
    $url = apply_filters( 'yokomotor_get_back_button_default_callback', $prev_url );
    
    // false for functionality deactivation / or action removal.
    if( $url ) {
      // Print HTML
      printf( '<div class="bar-return">
      <div class="wrapper-main center"><a href="%s" class="btn-return">%s</a></div></div>', 
        esc_url( $url ), 
        __( 'Regresar', 'yokomotor' )
      );
    }
  }
  add_action( 'yokomotor_get_back_button', 'yokomotor_get_back_button_main' );
}

/*
This is a modification of a function found in the
twentythirteen theme where we can declare some
external fonts. If you're using Google Fonts, you
can replace these fonts, change it in your scss files
and be up and running in seconds.
*/

/*function bones_fonts() {
  wp_enqueue_style('googleFonts', '//fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic');
}

add_action('wp_enqueue_scripts', 'bones_fonts');*/

/** 
 * ********************
 * block WP Enumeration
 * ********************
 *
 * Bloquea la Ruta /author y el Query String /?author= que viene por defecto de Wordpress
 * con una redirección 302 al home. Esto con el ánimo de mitigar vulnerabilidades conocidas del CMS.
 * 
 * @see No hacerlo si se desea contar con esta funcionalidad que es orientada a el Blogging.
 */

if ( !is_admin() ) {
  // default URL format
	if ( preg_match('/author=([0-9]*)/i', $_SERVER['QUERY_STRING'] ) || is_author() ) {
    wp_redirect( get_home_url() ); exit;
  }
	add_filter('redirect_canonical', 'smdigital_check_enum', 10, 2);
}

function smdigital_check_enum ($redirect, $request) {
	// permalink URL format
	if (preg_match('/\?author=([0-9]*)(\/*)/i', $request) || is_author() ) {
    wp_redirect( get_home_url() ); exit;
  }
	else return $redirect;
}

/**
 * add walkers to menus. 
 * @see https://developer.wordpress.org/reference/classes/walker_nav_menu/
 */

function register_footer_nav_walker(){
  require_once 'footer-class-walker-nav-menu.php';
}
add_action('after_setup_theme', 'register_footer_nav_walker');


/**
 * Disable Login Form Autocomplete
 * @since 16/09/2020
 */

function festival_autocomplete_login_init() {
  ob_start();
}
add_action('login_init', 'festival_autocomplete_login_init');

function yokomotor_autocomplete_login_form() {
  $content = ob_get_contents();
  ob_end_clean();
  
  $content = str_replace('id="user_pass"', 'id="user_pass" autocomplete="off"', $content);
  
  echo $content;
}
add_action('login_form', 'yokomotor_autocomplete_login_form');


/**
 * Remove the WordPress version number. 
 * @see https://desarrollowp.com/blog/tutoriales/eliminar-codigo-innecesario-del-wp_head/
 */

function yokomotor_remove_headlinks() {

remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}    

add_action( 'init', 'yokomotor_remove_headlinks' );

/**
 * HOOK CUSTOMIZE SIDEBAR
 */


add_filter('dynamic_sidebar_params', 'sidebar_sedes_params');

function sidebar_sedes_params( $params ) {
	
	// get widget vars
	$widget_name = $params[0]['widget_name'];
	$widget_id = $params[0]['widget_id'];
	
	
	// bail early if this widget is not a Text widget
	if( $widget_name != 'Text' ) {
		
		return $params;
		
	}
		
	// add color style to before_widget
	$group = get_field('yokomotor_sedes', 'widget_' . $widget_id);
  
		
	// return
	return $params;

}


/* Eliminamos las <p>*/
add_filter( 'wpcf7_autop_or_not', '__return_false' );

/* Eliminamos los <span> */
/* add_filter('wpcf7_form_elements', function($content) {
    $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

    $content = str_replace('<br />', '', $content);
        
    return $content; 
}); */

/* function email_already_template_landing ( $result, $tags ) {

  $form   = WPCF7_Submission::get_instance();
  $origen = $form->get_posted_data('origen');
  $formID = $_POST['_wpcf7'];
  
    
    $email = $form->get_posted_data('email');
    global $wpdb;
    $tbl = $wpdb->base_prefix.'db7_forms';
  
    $emailValidation = $wpdb->get_results( "SELECT form_post_id, form_value FROM $tbl WHERE form_post_id = '$formID' AND form_value LIKE '%$email%'" );
  
    if (!empty($emailValidation)) {
      $result->invalidate('email','Este correo ya se encuentra registrado');
    }
    

  return $result;
}
 */
function custom_text_validation_filter( $result, $tag ) {
  $re = '/^[a-zA-Z áéíóúÁÉÍÓÚäëïöüÄËÏÖÜ]+$/';

  if ( 'username' == $tag->name ) {
      if (!preg_match($re,  $_POST['username'], $matches))
          $result->invalidate($tag, 'El campo Nombre sólo acepta texto.' );
    
  }else if( 'fullname' == $tag->name ) {
    if (!preg_match($re,  $_POST['fullname'], $matches)) 
      $result->invalidate($tag, 'El campo Nombre completos sólo acepta texto.' );

  } else if( 'city' == $tag->name ) {

    if (!preg_match($re, $_POST['city'], $matches)) 
        $result->invalidate($tag, 'El formato del campo Ciudad no es válido.' );     
  }
  return $result;
}


// add_filter( 'wpcf7_validate', 'email_already_template_landing', 11, 2 ); 
add_filter( 'wpcf7_validate_text*', 'custom_text_validation_filter', 20, 2 );

// to upload images with svg format
function yokomotor_svg_mime_types( $mimes ) {
  
  // New allowed mime types.
  $mimes['svg']  = 'image/svg';
  $mimes['svgz'] = 'image/svg+xml';
  $mimes['doc']  = 'application/msword'; 

  // Optional. Remove a mime type.
  //unset( $mimes['exe'] );

  return $mimes;
}

add_filter( 'upload_mimes', 'yokomotor_svg_mime_types' );


/* DON'T DELETE THIS CLOSING TAG */ ?>