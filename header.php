<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // force Internet Explorer to use the latest rendering engine available ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<title><?php wp_title(''); ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-touch-icon.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">
            <meta name="theme-color" content="#121212">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>
			
		<?php // drop Google Analytics Here ?>
			<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
			new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
			j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
			'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-54HCZ8H');</script> 
			<!-- End Google Tag Manager -->
		<?php // end analytics ?>

	</head>
	<?php $classBody = str_replace(' ', '', get_bloginfo('name')); ?>
	<body <?php body_class('body-site-'.strtolower($classBody)); ?> itemscope itemtype="http://schema.org/WebPage">
		<!-- Google Tag Manager (noscript) -->
 <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-54HCZ8H"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

		<header>
			<div id="nav-toogle">
				<span></span>
				<span></span>
				<span></span>
				<span></span>
      </div>
			<div class="bar-secondary clear-fix">
				<div class="wrapper-main center">
				<?php 
				
				/* $primary_menu = array(
					'theme_location' => 'main-nav',
			);
			wp_nav_menu($primary_menu); */
				if ( has_nav_menu('social-links') ) {
						wp_nav_menu( array(
							'menu'              => 'redes',
							'theme_location'    => 'social-links',
							'container'         => 'div',
							'depth' 						=> 1,
							'container_class'   => 'redes',
							'items_wrap'        => '<ul id="%1s" class="%2s">%3s</ul>',
						) );
					}; ?>
				
					<?php if ( has_nav_menu('top-links') ) {
						wp_nav_menu( array(
							'menu'              => 'nav-top',
							'theme_location'    => 'top-links',
							'container'         => 'div',
							'depth' 						=> 1,
							'container_class'   => 'nav-top',
							'items_wrap'        => '<ul id="%1s" class="%2s">%3s</ul>',
						) );
					}; ?>


<?php 
		$blogs = get_current_blog_id();
		switch ($blogs): 
			case '2': switch_to_blog(2);?>
				
				<?php

					/**
					 * 
					 * Contact Menu Nav
					 * @see Actual Hierarchy for this section is 1 (First Level)
					 * 
					 */

					if ( has_nav_menu('contact-nav') ) {
						wp_nav_menu( array(
							'menu'              => 'contactanos-links',
							'theme_location'    => 'contact-nav',
							'container'         => 'div',
							'depth' 			=> 0,
							'container_class'   => 'nav-contact-ws',
							'items_wrap'        => '<ul id="%1s" class="%2s secondary">%3s</ul>',
						) );
					}

					
				?>
					<script>
					jQuery(document).ready(function() {
						if (jQuery('.nav-contact-ws').length > 0) {
							jQuery('.nav-contact-ws .sub-menu .city').on('click', function() {
								jQuery(this).find('.sub-menu').toggle();
							});
						};

						jQuery('.bar-principal').on('mouseleave', function () {
								jQuery('.thirt-menu').hide();
							});
					})
					</script>
				   <?php 
				
				if ( has_nav_menu('test-nav') ) {
					wp_nav_menu( array(
						'menu'              => 'btn-test',
						'theme_location'    => 'test-nav',
						'container'         => 'div',
						'depth' 			=> 0,
						'container_class'   => 'btn-testdive',
                        'items_wrap'        => '<ul id="%1s" class="%2s">%3s</ul>',

					) );
				};
				?>
				<script>
					jQuery(document).ready(function() {
						if (jQuery('.btn-testdive').length > 0) {
							jQuery('.btn-testdive .sub-menu .menu-item').on('click', function() {
								jQuery(this).find('.sub-menu').toggle();
							});
						};

						jQuery('.bar-principal').on('mouseleave', function () {
								jQuery('.thirt-menu').hide();
							});
					})
					</script>
				<?php restore_current_blog(); break;?>
			<?php case '3': switch_to_blog(3); ?>
			<?php

					/**
					 * 
					 * Contact Menu Nav
					 * @see Actual Hierarchy for this section is 1 (First Level)
					 * 
					 */

					if ( has_nav_menu('contact-nav') ) {
						wp_nav_menu( array(
							'menu'              => 'contactanos-links',
							'theme_location'    => 'contact-nav',
							'container'         => 'div',
							'depth' 			=> 0,
							'container_class'   => 'nav-contact-ws',
							'items_wrap'        => '<ul id="%1s" class="%2s secondary">%3s</ul>',
						) );
					}

					
				?>
				<script>
					jQuery(document).ready(function() {
						if (jQuery('.nav-contact-ws').length > 0) {
							jQuery('.nav-contact-ws .sub-menu .city').on('click', function() {
								jQuery(this).find('.sub-menu').toggle();
							});
						};

						jQuery('.bar-principal').on('mouseleave', function () {
								jQuery('.thirt-menu').hide();
							});
					})
					</script>
				<?php restore_current_blog(); break;?>

			<?php default:  restore_current_blog(); break?>
			
		<?php endswitch; ?>

<?php ?>

<!-- menu con toolptip-->
<!-- <div class="custom-toltip-menu btn-tooltip">
  <div class="btn-yokomotor icon-whatsapp">Contáctenos</div>
  <ul class="tooltip-cities">
    
      <li data-city="city-0">
        <a href="javascript:;">Medellín </strong></a>
      </li>
      <li data-city="city-1">
        <a href="javascript:;"><strong>Bogotá</strong></a>
      </li>
  </ul>	
  <ul class="tooltip-shop" style="display: none;">
    <li data-shop="city-0">
        <a href="https://api.whatsapp.com/send?phone=573024505859&text=Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor'" target="_blank">
      <strong>palace</strong>3020334353545
        </a>
    </li>
    <li data-shop="city-1">
        <a href="https://api.whatsapp.com/send?phone=573024505859&text=Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor'" target="_blank">
      <strong>palace2</strong>3020334353545
        </a>
    </li>
    <li data-shop="city-1">
        <a href="https://api.whatsapp.com/send?phone=573024505859&text=Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor'" target="_blank">
      <strong>palace2</strong>3020334353545
        </a>
    </li>
  </ul>	
  </div>
</div> -->
<!-- end menu con toltip-->
 
					<!-- Opciones menú-->
<!-- Menú con hover lo hice como opción-->

<!-- <div class="nav-contact-ws">
  <ul>
    <li>
      <a href="javascript:;">Contáctanos</a>
      <ul class="sub-menu">
        <li class="city">
          <a href="javascript:;">Medellín</a>
          <ul class="thirt-menu" style="display: none;">
            <li id="" class=""><a href="https://api.whatsapp.com/send?phone=573002928762&amp;text=Hola,%20quiero%20comunicarme%20con%20Comercial%20Yokomotor"><span>Venta Medellín<br>3102648638</span><i></i></a></li>
          </ul>
        </li>
        <li class="city">
          <a href="javascript:;">Bogotá</a>
          <ul class="thirt-menu" style="display: none;">
            <li id="" class=""><a href="https://api.whatsapp.com/send?phone=573002928762&amp;text=Hola,%20quiero%20comunicarme%20con%20Comercial%20Yokomotor"><span>Venta Medellín<br>3102648638</span><i></i></a></li>
            <li id="" class=""><a href="https://api.whatsapp.com/send?phone=573002928762&amp;text=Hola,%20quiero%20comunicarme%20con%20Comercial%20Yokomotor"><span>Venta Medellín<br>3102648638</span><i></i></a></li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</div>

<script>
  jQuery(document).ready(function() {
    if (jQuery('.nav-contact-ws').length > 0) {
      jQuery('.nav-contact-ws .sub-menu .city').on('click', function() {
        jQuery(this).find('.thirt-menu').toggle();
      });
    }
  })
  </script> -->

  <!-- end menu hover -->

  <!-- menu con toolptip-->
<!-- <div class="clase-marin-toltip btn-tooltip">
  <div class="btn-yokomotor icon-whatsapp">Contáctenos</div>
  <ul class="tooltip-cities">
    
      <li data-city="city-0">
        <a href="javascript:;">Medellín </strong></a>
      </li>
      <li data-city="city-1">
        <a href="javascript:;"><strong>Bogotá</strong></a>
      </li>
  </ul>	
  <ul class="tooltip-shop" style="display: none;">
    <li data-shop="city-0">
        <a href="https://api.whatsapp.com/send?phone=573024505859&text=Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor'" target="_blank">
      <strong>palace</strong>3020334353545
        </a>
    </li>
    <li data-shop="city-1">
        <a href="https://api.whatsapp.com/send?phone=573024505859&text=Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor'" target="_blank">
      <strong>palace2</strong>3020334353545
        </a>
    </li>
    <li data-shop="city-1">
        <a href="https://api.whatsapp.com/send?phone=573024505859&text=Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor'" target="_blank">
      <strong>palace2</strong>3020334353545
        </a>
    </li>
  </ul>	
  </div>
</div> -->
<!-- end menu con toltip-->

				</div>            	
			</div>
			<div class="bar-principal full clear-fix">
				<div class="wrapper-main center">
					<!-- <figure class="logo-toyota">
						<a href="<?php //echo esc_url( get_bloginfo( 'url' ) ); ?>">
							<?php /*
								$customLogoID = get_theme_mod( 'custom_logo' );
								$logoImage 		= wp_get_attachment_image_src( $customLogoID , 'full' );
							*/?>
							<img src="<?php //echo($logoImage) ? esc_url($logoImage[0]) : esc_url(get_stylesheet_directory_uri(). '/library/images/logo-yokomotor.png'); ?>" alt="Logo yokomotor">
						</a>
					</figure> -->
					<figure class="logo-yokomotor">
							<a href="/">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-yokomotor.png" alt=""> 
							</a>
						</figure>
						<?php 
						$blogs = get_current_blog_id();

						switch ($blogs): 
							case '2': switch_to_blog(2);?>
							<figure class="logo-toyota">
								<a href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-toyota.png" alt=""> 
								</a>
							</figure>
								<?php restore_current_blog(); break;?>
							<?php case '3': switch_to_blog(3)?>
							<figure class="logo-toyota">
								<a href="<?php echo esc_url( get_bloginfo( 'url' ) ); ?>">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-hino-menu.png" alt=""> 
								</a>
							</figure>
							<?php restore_current_blog(); break;?>
							<?php default:  restore_current_blog(); break?>
						<?php endswitch; ?>
					<?php

					/**
					 * 
					 * Main Menu Nav
					 * @see Actual Hierarchy for this section is 1 (First Level)
					 * Child hierarchy structure defined in page-parts/sections/section-desktop_menu.php
					 * 
					 */

				if ( has_nav_menu('main-nav') ) {
					wp_nav_menu( array(
						'menu'              => 'menu-principal',
						'theme_location'    => 'main-nav',
						'container'         => 'nav',
						'depth' 						=> 0,
						'container_class'   => 'nav-ppal',
						'items_wrap'        => '<ul id="%1s" class="%2s">%3s</ul>',
					) );
				}

				 
				?>
				</div>
					<?php
					/**
					 * 
					 * Page Part Template 'section-desktop_menu.php
					 * @since 1.0.0
					 * 
					 */

					get_template_part( 'page-parts/menus/menu', 'desktop' ); ?>
			</div>
			
	
		</header>
		<?php 	get_template_part( 'page-parts/menus/menu', 'mobile' ); ?>
	
