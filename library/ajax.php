<?php 
/**
 *
 * Ajax Functions
 * ***************
 *
 * @see Yokomotor yokomotor_services
 * @method insert_services_date()
 *
 *
 *
 */

function insert_services_date() {
 
  if( ! check_ajax_referer( 'ajax_nonce', 'nonce' )) wp_send_json_error( 'Err 1: Ha ocurrido un problema, inténtalo de nuevo');
  $errors = [];

  $fullname       = sanitize_text_field( $_POST["fullname"] );
  $servicename    = sanitize_text_field( $_POST["name"] );
  $email          = sanitize_email( $_POST["email"] );
  $phone          = sanitize_text_field( $_POST["phone"] );
  $city           = sanitize_text_field( $_POST["city"] );
  $date           = sanitize_text_field( $_POST["date"] );
  $workshop       = sanitize_text_field( $_POST["workshop"] );
  if($_POST["placa"] && !empty($_POST["placa"])){ $placa = sanitize_text_field( $_POST["placa"] );}else{ $placa = ''; };
  $terms          = sanitize_text_field( $_POST["terms"] );

  if( empty($fullname) ){
    $errors['fullname'] = ['Este valor es requerido'];
  }

  if (!is_email($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $errors['email'] = ['Este valor debe ser un correo válido.'];
  };

  if( empty( $phone ) ) {
    $errors['phone'] =  ['Este valor es requerido'];
  } else if( !is_numeric( $phone ) ) {
    $errors['phone'] = ['El teléfono debe ser un número válido.'];
  }

  if( empty($city) ){
    $errors['city'] = ['Este valor es requerido'];
  }

  if( empty($workshop) ){
    $errors['workshop'] = ['Este valor es requerido'];
  }

  global $wpdb;

  $table = $wpdb->prefix . 'service_required';

  try{
   $result = $wpdb->insert( $table, [
      'fullname'       => ucfirst(mb_strtolower($fullname)),
      'servicename'    => ucfirst(mb_strtolower($servicename)),
      'email'          => $email,
      'phone'          => $phone,
      'city'           => ucfirst(mb_strtolower($city)),
      'workshop'       => ucfirst(mb_strtolower($workshop)),
      'placa'           => ucfirst(mb_strtoupper($placa)),
      'terms'          => $terms,
      'created_at'     => current_time( 'mysql' )
    ]); 

      if($result){
       wp_send_json_success( 'Registro Exitoso', 200 );  
      } else {
        exit( var_dump($wpdb->last_query) );
        wp_send_json_error( "Error al realizar el regístro"  , 401 );
      }
    } catch (Exception $e) {
      wp_send_json_error( $e->getMessage() , 401 );
    }
}

add_action('wp_ajax_insert_services_date', 'insert_services_date');
add_action( 'wp_ajax_nopriv_insert_services_date', 'insert_services_date' );

function getCars(){
  $array = array();
  $args = array(
    'post_type'	=>	'yokomotor_vehicles'
  );
  $cars = get_posts($args);
  foreach($cars as $car){
    $temp = array('ID' => $car->ID, 'title' => $car->post_title);
    array_push($array, $temp);
  }
  wp_send_json_success( $array );
}

add_action('wp_ajax_getCars', 'getCars');
add_action( 'wp_ajax_nopriv_getCars', 'getCars' );

function getInfoCar(){
  $id     = $_POST['id'];
  $array = array();
  $args = array(
    'post_type'	=>	'yokomotor_vehicles'
  );
  $cars = get_posts($args);
  if(count($cars)>0){
      $price 	= get_field('price', $id);
      $img 	  = get_field('thumbnail_calculator', $id);
      $title  = get_the_title($id);
      $temp   = array('price' => number_format($price, 0, '.', '.'), 'img' => $img['url'], 'title' => $title);
      array_push($array, $temp);
  }
  wp_send_json_success( $array );
}

add_action('wp_ajax_getInfoCar', 'getInfoCar');
add_action( 'wp_ajax_nopriv_getInfoCar', 'getInfoCar' );