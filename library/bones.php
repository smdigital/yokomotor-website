<?php
/* Welcome to Bones :)
This is the core Bones file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by: Eddie Machado
URL: http://themble.com/bones/

  - head cleanup (remove rsd, uri links, junk css, ect)
  - enqueueing scripts & styles
  - theme support functions
  - custom menu output & fallbacks
  - related post function
  - page-navi function
  - removing <p> from around images
  - customizing the post excerpt

*/

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function bones_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );

} /* end bones head cleanup */

// A better title
// http://www.deluxeblogtips.com/2012/03/better-title-meta-tag.html
function rw_title( $title, $sep, $seplocation ) {
  global $page, $paged;

  // Don't affect in feeds.
  if ( is_feed() ) return $title;

  // Add the blog's name
  if ( 'right' == $seplocation ) {
    $title .= get_bloginfo( 'name' );
  } else {
    $title = get_bloginfo( 'name' ) . $title;
  }

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );

  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title .= " {$sep} {$site_description}";
  }

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 ) {
    $title .= " {$sep} " . sprintf( __( 'Page %s', 'dbt' ), max( $paged, $page ) );
  }

  return $title;

} // end better title

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
	if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
		remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
	}
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
	}
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
	return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function bones_scripts_and_styles() {

  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  if (!is_admin()) {

			
		wp_register_style( 'yokomotor-bootstrap-css', get_stylesheet_directory_uri() . '/library/includes/widgets/bootstrap-4/css/bootstrap.min.css' );
		wp_register_style( 'yokomotor-animate-css', get_stylesheet_directory_uri() . '/library/css/animate.min.css', array(), '', 'all' );
		wp_register_style( 'yokomotor-swiper-css', get_stylesheet_directory_uri() . '/library/css/swiper.min.css', array(), '', 'all' );
		wp_register_style( 'yokomotor-nav-mobile-css', get_stylesheet_directory_uri() . '/library/css/nav-mobile.css', array(), '', 'all' );
		wp_register_style( 'yokomotor-Aos-css', get_stylesheet_directory_uri() . '/library/css/aos.css', array(), '', 'all' );
		wp_register_script( 'yokomotor-modernizr-js', get_stylesheet_directory_uri() . '/library/js/libs/modernizr.custom.js', array( 'jquery' ), '', true);	
		wp_register_script( 'yokomotor-prefixfree-js', get_stylesheet_directory_uri() . '/library/js/libs/prefixfree.min.js', array( 'jquery' ), '', true );		
		wp_register_script( 'yokomotor-bootstrap-js', get_stylesheet_directory_uri() . '/library/includes/widgets/bootstrap-4/js/bootstrap.min.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-swiper-js', get_stylesheet_directory_uri() . '/library/js/libs/aos.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-AOS-js', get_stylesheet_directory_uri() . '/library/js/libs/swiper.min.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-jquery-ui-js', get_stylesheet_directory_uri() . '/library/js/libs/jquery-ui.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-dlmenu-js', get_stylesheet_directory_uri() . '/library/js/libs/jquery.dlmenu.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-parsley-js', get_stylesheet_directory_uri() . '/library/js/libs/parsley.min.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-parsley-es-js', get_stylesheet_directory_uri() . '/library/js/libs/parsley-es.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-custom-alm-js', get_stylesheet_directory_uri() . '/library/js/libs/custom-alm-filter.min.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-custom-calculator-js', get_stylesheet_directory_uri() . '/library/js/libs/custom-calculator.min.js', array( 'jquery' ), '', true );
		wp_register_script( 'yokomotor-carnow-js', 'https://app.carnow.com/plugin/Ksg3H5LKhuepoHjDyhuAnGLD4cn7lqm1aBo4p05H4GmpGxr6.js', array(), '', true );
		
		// wp_register_script( 'custom', get_stylesheet_directory_uri() . '/library/js/libs/custom.min.js', array( 'jquery' ), '', true );

		// register main stylesheet
		wp_register_style( 'bones-stylesheet', get_stylesheet_directory_uri() . '/library/css/style.css', array(), '', 'all' );

		// ie-only style sheet
		wp_register_style( 'bones-ie-only', get_stylesheet_directory_uri() . '/library/css/ie.css', array(), '' );

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
		  wp_enqueue_script( 'comment-reply' );
    }

		//adding scripts file in the footer

		// enqueue styles and scripts
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'yokomotor-modernizr-js' );
		wp_enqueue_script( 'yokomotor-prefixfree-js' );		
		wp_enqueue_script( 'yokomotor-bootstrap-js' );		
		wp_enqueue_script( 'yokomotor-swiper-js' );
		wp_enqueue_script( 'yokomotor-AOS-js' );
		wp_enqueue_script( 'yokomotor-jquery-ui-js' );
		wp_enqueue_script( 'yokomotor-dlmenu-js' );
		wp_enqueue_script( 'yokomotor-custom-alm-js' );
		wp_enqueue_script( 'yokomotor-custom-calculator-js' );
		// wp_enqueue_script( 'yokomotor-carnow-js' );
		$blogs = get_current_blog_id();
        switch ($blogs): 
        case '2': switch_to_blog(2);
        wp_enqueue_script( 'yokomotor-carnow-js' );
        endswitch;
		
  	// Llamamos la Etiqueda Custom
  	wp_enqueue_script( 'custom' );

		
		wp_enqueue_style( 'yokomotor-animate-css' );
		wp_enqueue_style( 'yokomotor-swiper-css' );		
		wp_enqueue_style( 'yokomotor-nav-mobile-css' );				
		wp_enqueue_style( 'yokomotor-Aos-css' );
		wp_enqueue_style( 'yokomotor-bootstrap-css' );

		wp_enqueue_style( 'bones-stylesheet' );
		wp_enqueue_style( 'bones-ie-only' );

		wp_register_script( 'bones-js', get_stylesheet_directory_uri() . '/library/js/scripts.min.js', array( 'jquery' ), '2.0', true );
		$wp_styles->add_data( 'bones-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

		/*
		I recommend using a plugin to call jQuery
		using the google cdn. That way it stays cached
		and your site will load faster.
		*/
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'bones-js' );
		wp_enqueue_script( 'yokomotor-parsley-js' );
		wp_enqueue_script( 'yokomotor-parsley-es-js' );

	}
}

/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function bones_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',    // background image default
	    'default-color' => '',    // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);

	// rss thingy
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support
	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);

	// wp menus
	add_theme_support( 'menus' );

	// registering wp3+ menus
	register_nav_menus(
		array(
			'main-nav' => __( 'Menú principal', 'bonestheme' ),   // main nav in header
			'nosotros-links' => __( 'Nosotros Links', 'bonestheme' ),  // nav in footer about section "Nosotros"
			'test-nav' => __( 'Test drive Link', 'bonestheme' ),  // Button "Test Drive" in header top 
			'contact-nav' => __( 'Contactanos Links', 'bonestheme' ),  // nav in footer about section "Contact"
			'top-links' => __( 'Menú superior', 'bonestheme' ), // top menu section
			'social-links' => __( 'Menú redes sociales', 'bonestheme' ), // social nav in footer
			'footer-links' => __( 'Footer Links', 'bonestheme' ), // nav in footer about section "Trabaja con nosotros" and "Mapa del sitio"
			'tyc-links' => __( 'TyC Links', 'bonestheme' ), // nav in footer with TyC
			'fixed-button' => __( 'Fixed button', 'bonestheme' ), // nav in float button
			'sedes' => __( 'Sedes', 'bonestheme' ), // List of workshops
		)
	);

	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form'
	) );

	// Custom_logo 
	add_theme_support( 'custom-logo' );

} /* end bones theme support */


/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
	echo '<ul id="bones-related-posts">';
	global $post;
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 5, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		$related_posts = get_posts( $args );
		if($related_posts) {
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
				<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
			<?php endforeach; }
		else { ?>
			<?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'bonestheme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_postdata();
	echo '</ul>';
} /* end bones related posts function */

/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function bones_page_navi() {
  global $wp_query;
  $bignum = 999999999;
  if ( $wp_query->max_num_pages <= 1 )
    return;
  echo '<nav class="pagination">';
  echo paginate_links( array(
    'base'         => str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
    'format'       => '',
    'current'      => max( 1, get_query_var('paged') ),
    'total'        => $wp_query->max_num_pages,
    'prev_text'    => '&larr;',
    'next_text'    => '&rarr;',
    'type'         => 'list',
    'end_size'     => 3,
    'mid_size'     => 3
  ) );
  echo '</nav>';
} /* end page navi */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function bones_filter_ptags_on_images($content){
	return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function bones_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'bonestheme' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', 'bonestheme' ) .'</a>';
}

/**********************
DEFER SCRIPTS BY DEFAULT
***********************/

function mind_defer_scripts( $tag, $handle, $src ) {
	$defer = array( 
		'jquery',
		'jquery-migrate',
		'yokomotor-modernizr-js',
		'yokomotor-prefixfree-js',
		'yokomotor-bootstrap-js',
		'yokomotor-swiper-js',
		'yokomotor-AOS-js',
		'yokomotor-jquery-ui-js',
		'yokomotor-custom-alm-js',
		'yokomotor-dlmenu-js'
	);
	if ( in_array( $handle, $defer ) ) return '<script src="' . $src . '" defer="defer" type="text/javascript"></script>' . "\n";
	
	return $tag;
 } 
 add_filter( 'script_loader_tag', 'mind_defer_scripts', 10, 3 );

// Funcion para ampliar Menu Derecho Footer despues de 40 Segundos sin que cargue en el Prehome

function load_fixedbutton_js() {
	global $post;
	$postid = $post->ID;

		if($postid != 26 ){
			return;		
		}
	wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/library/js/libs/custom.min.js', array( 'jquery' ), '', true );

}
add_action('wp_enqueue_scripts', 'load_fixedbutton_js');

?>