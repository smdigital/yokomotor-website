<?php
/* Bones Custom Post Type Example
This page walks you through creating 
a custom post type and taxonomies. You
can edit this one or copy the following code 
to create another one. 

I put this in a separate file so as to 
keep it organized. I find it easier to edit
and change things if they are concentrated
in their own file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

// Flush rewrite rules for custom post types
add_action( 'after_switch_theme', 'bones_flush_rewrite_rules' );

// Flush your rewrite rules
function bones_flush_rewrite_rules() {
	flush_rewrite_rules();
}

// let's create the function for the custom type
function custom_post_example() { 
	// creating (registering) the custom type 
	//register_post_type( 'custom_type', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		// let's now add all the options for this post type
		//array( 'labels' => array(
			//'name' => __( 'Custom Types', 'bonestheme' ), /* This is the Title of the Group */
			//'singular_name' => __( 'Custom Post', 'bonestheme' ), /* This is the individual type */
			//'all_items' => __( 'All Custom Posts', 'bonestheme' ), /* the all items menu item */
			//'add_new' => __( 'Add New', 'bonestheme' ), /* The add new menu item */
			//'add_new_item' => __( 'Add New Custom Type', 'bonestheme' ), /* Add New Display Title */
			//'edit' => __( 'Edit', 'bonestheme' ), /* Edit Dialog */
			//'edit_item' => __( 'Edit Post Types', 'bonestheme' ), /* Edit Display Title */
			//'new_item' => __( 'New Post Type', 'bonestheme' ), /* New Display Title */
			//'view_item' => __( 'View Post Type', 'bonestheme' ), /* View Display Title */
			//'search_items' => __( 'Search Post Type', 'bonestheme' ), /* Search Custom Type Title */ 
			//'not_found' =>  __( 'Nothing found in the Database.', 'bonestheme' ), /* This displays if there are no entries yet */ 
			//'not_found_in_trash' => __( 'Nothing found in Trash', 'bonestheme' ), /* This displays if there is nothing in the trash */
			//'parent_item_colon' => ''
			//), /* end of arrays */
			//'description' => __( 'This is the example custom post type', 'bonestheme' ), /* Custom Type Description */
			//'public' => true,
			//'publicly_queryable' => true,
			//'exclude_from_search' => false,
			//'show_ui' => true,
			//'query_var' => true,
			//'menu_position' => 8, /* this is what order you want it to appear in on the left hand side menu */ 
			//'menu_icon' => get_stylesheet_directory_uri() . '/library/images/custom-post-icon.png', /* the icon for the custom post type menu */
			//'rewrite'	=> array( 'slug' => 'custom_type', 'with_front' => false ), /* you can specify its url slug */
			//'has_archive' => 'custom_type', /* you can rename the slug here */
			//'capability_type' => 'post',
			//'hierarchical' => false,
			/* the next one is important, it tells what's enabled in the post editor */
			//'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', //'custom-fields', 'comments', 'revisions', 'sticky')
		//) /* end of options */
	//); /* end of register post type */
	
	/* this adds your post categories to your custom post type */
	//register_taxonomy_for_object_type( 'category', 'custom_type' );
	/* this adds your post tags to your custom post type */
	//register_taxonomy_for_object_type( 'post_tag', 'custom_type' );


		/*	
	**
	* 
	* *************
	*  Banners CPT
	* *************
	* 
	* @param banners 'Banners'
	* 
	*/
	register_post_type( 'yokomotor_banners',
		array( 'labels' 	=> array(
			'name'          => _x( 'Banners', 'yokomotor' ),
			'singular_name' => _x( 'Banner', 'yokomotor' ),
			'search_items' 	=> __( 'Buscar Banner', 'yokomotor' ),
			'all_items' 		=> __( 'Todos los Banners', 'yokomotor' ),
			),
			'description' 	=> __( 'Tipo de post personalizado para los banners.', 'yokomotor' ),
			'public' 				=> true,
			'menu_position' => 6, 
			'menu_icon'     => 'dashicons-images-alt',
			'has_archive' 	=> false,
			'rewrite'			  => false, 
			'supports' 			=> array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		)
	);

	/**
	 * 
	 * Campañas CPT
	 */
	register_post_type( 'yokomotor_campaigns',
		array( 'labels' => array(
			'name' => __( 'Campañas', 'yokomotor' ),
			'singular_name' => __( 'Campaña', 'yokomotor' ),
			'search_items' => __( 'Buscar Campaña', 'yokomotor' ),
			'all_items' => __( 'Todos los Campañas', 'yokomotor' ),
			),
			'description' => __( 'Añadir campaña', 'yokomotor' ),
			'public' => true,
			'menu_position' => 7,
			'menu_icon' => 'dashicons-megaphone',
			'has_archive' => true,
			'show_in_nav_menus' => false,
			'rewrite'	=> false,
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		)
	);

	/*
	* 
	* Vehiculos CPT
	*/
 register_post_type( 'yokomotor_vehicles',
	 array( 'labels' => array(
		 'name' => __( 'Vehiculos', 'yokomotor' ),
		 'singular_name' => __( 'Vehiculo', 'yokomotor' ),
		 'search_items' => __( 'Buscar Vehicle', 'yokomotor' ),
		 'all_items' => __( 'Todos los Vehicles', 'yokomotor' ),
		 ),
		 'description' => __( 'Añadir Vehicle', 'yokomotor' ),
		 'public' => true,
		 'menu_position' => 8,
		 'menu_icon' => 'dashicons-car',
		 'has_archive' => true,
		 'show_in_nav_menus' => false,
		 'rewrite'	  => array( 'slug' => 'vehiculo/%vehicles_cats%', 'with_front' => false ),
		 'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	 )
 );


/*
* 
* Boutique CPT
*/
register_post_type( 'yokomotor_boutique',
	array( 'labels' => array(
		'name' => __( 'Boutique', 'yokomotor' ),
		'singular_name' => __( 'Boutique', 'yokomotor' ),
		'search_items' => __( 'Buscar Boutique', 'yokomotor' ),
		'all_items' => __( 'Todos los Boutique', 'yokomotor' ),
		),
		'description' => __( 'Añadir Boutique', 'yokomotor' ),
		'public' => true,
		'menu_position' => 9,
		'menu_icon' => 'dashicons-businesswoman',
		'has_archive' => true,
		'show_in_nav_menus' => false,
		'rewrite'	  => array( 'slug' => 'boutique/%boutique_cats%', 'with_front' => false ),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	)
);


/**
 * 
 * Blog CPT
 */
register_post_type( 'yokomotor_blog',
	array( 'labels' => array(
		'name' => __( 'Blogs', 'yokomotor' ),
		'singular_name' => __( 'Blog', 'yokomotor' ),
		'search_items' => __( 'Buscar Artículo', 'yokomotor' ),
		'all_items' => __( 'Todos los Artículos', 'yokomotor' ),
		),
		'description' => __( 'Añadir artículo', 'yokomotor' ),
		'public' => true,
		'menu_position' => 11,
		'menu_icon' => 'dashicons-welcome-write-blog',
		'has_archive' => true,
		'show_in_nav_menus' => false,
		'rewrite'	  => array( 'slug' => 'blog', 'with_front' => true ),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
	)
);

/**
 * 
 * Used CPT
 */
register_post_type( 'yokomotor_used',
	array( 'labels' => array(
		'name' => __( 'Usados', 'yokomotor' ),
		'singular_name' => __( 'Usado', 'yokomotor' ),
		'search_items' => __( 'Buscar vehículo usado', 'yokomotor' ),
		'all_items' => __( 'Todos los vehículos usados', 'yokomotor' ),
		),
		'description' => __( 'Añadir vehículo usado', 'yokomotor' ),
		'public' => true,
		'menu_position' => 12,
		'menu_icon' => 'dashicons-car',
		'has_archive' => false,
		'show_in_nav_menus' => false,
		'rewrite'	  => array( 'slug' => 'usados/%classification_cats%', 'with_front' => false ),
		'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'sticky')
	)
);



	/*
	* 
	* Accessories CPT
	*/
	register_post_type( 'yokomotor_accessory',
		array( 'labels' => array(
			'name' => __( 'Repuestos', 'yokomotor' ),
			'singular_name' => __( 'Accesorio', 'yokomotor' ),
			'search_items' => __( 'Buscar repuestos', 'yokomotor' ),
			'all_items' => __( 'Todos los repuestos', 'yokomotor' ),
			),
			'description' => __( 'Añadir repuestos', 'yokomotor' ),
			'public' => false,
			'menu_position' => 10,
			'menu_icon' => 'dashicons-block-default',
			'has_archive' => false,
			'exclude_from_search' => true,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'rewrite'	  => array( 'slug' => 'repuestos/%accessory_cats%', 'with_front' => false ),
			'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'sticky')
		)
	);
}

	// adding the function to the Wordpress init
	add_action( 'init', 'custom_post_example');
	
	/*
	for more information on taxonomies, go here:
	http://codex.wordpress.org/Function_Reference/register_taxonomy
	*/
	
	// now let's add custom categories (these act like categories)
	register_taxonomy( 'vehicles_cats', 
		array('yokomotor_vehicles'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categorías de vehiculos', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categoría', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir categoría', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir categoría', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'vehiculo', 'with_front' => true  ),
		)
	);

	register_taxonomy( 'boutique_cats', 
		array('yokomotor_boutique'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categorías de boutique', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categoría', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir categoría', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir categoría', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'boutique' ),
		)
	);

	
	register_taxonomy( 'accessory_cats', 
		array('yokomotor_accessory'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categorías de accesorios', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categoría', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir categoría', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir categoría', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'repuestos' ),
		)
	);

	register_taxonomy( 'blog_cats', 
		array('yokomotor_blog'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categorías de blog', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categoría', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir categoría', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir categoría', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'blog-categoria', 'with_front' => false  ),
		)
	);

	register_taxonomy( 'used_cats', 
		array('yokomotor_used'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Categorías de vehículos usados', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Categoría', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar categorías', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las categorías', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Categoría padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar categoría', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar categoría', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir categoría', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir categoría', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'usados' )
		)
	);

	register_taxonomy( 'city_cats', 
		array('yokomotor_used'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Ciudades', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Ciudad', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar ciudad', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las ciudades', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Ciudad padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar ciudad', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar ciudad', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir ciudad', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir ciudad', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'query_var' => true
		)
	);

	register_taxonomy( 'brand_cats', 
		array('yokomotor_used'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Marcas', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Marca', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar marca', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las marcas', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Marca padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar Marca', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar marca', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir marca', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir marca', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => true,
			'show_ui' => true,
			'query_var' => true
		)
	);

	register_taxonomy( 'classification_cats', 
		array('yokomotor_used'), /* Name of register_post_type */
		array('hierarchical' => true,     /* if this is true, it acts like categories */
			'labels' => array(
				'name' => __( 'Clasificiónes usados', 'yokomotor' ), /* name of the custom taxonomy */
				'singular_name' => __( 'Clasifición', 'yokomotor' ), /* single taxonomy name */
				'search_items' =>  __( 'Buscar clasifición', 'yokomotor' ), /* search title for taxomony */
				'all_items' => __( 'Todas las clasificiones', 'yokomotor' ), /* all title for taxonomies */
				'parent_item' => __( 'Padre', 'yokomotor' ), /* parent title for taxonomy */
				'parent_item_colon' => __( 'Clasifición padre:', 'yokomotor' ), /* parent taxonomy title */
				'edit_item' => __( 'Editar Clasifición', 'yokomotor' ), /* edit custom taxonomy title */
				'update_item' => __( 'Actualizar Clasifición', 'yokomotor' ), /* update title for taxonomy */
				'add_new_item' => __( 'Añadir Clasifición', 'yokomotor' ), /* add new title for taxonomy */
				'new_item_name' => __( 'Añadir Clasifición', 'yokomotor' ) /* name title for taxonomy */
			),
			'show_admin_column' => true, 
			'show_in_nav_menus' => false,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'clasificacion-usados' )
		)
	);



	
	// now let's add custom tags (these act like categories)
	//register_taxonomy( 'custom_tag', 
		//array('custom_type'), /* if you change the name of register_post_type( 'custom_type', then you have to change this */
		//array('hierarchical' => false,    /* if this is false, it acts like tags */
			//'labels' => array(
				//'name' => __( 'Custom Tags', 'bonestheme' ), /* name of the custom taxonomy */
				//'singular_name' => __( 'Custom Tag', 'bonestheme' ), /* single taxonomy name */
				//'search_items' =>  __( 'Search Custom Tags', 'bonestheme' ), /* search title for taxomony */
				//'all_items' => __( 'All Custom Tags', 'bonestheme' ), /* all title for taxonomies */
				//'parent_item' => __( 'Parent Custom Tag', 'bonestheme' ), /* parent title for taxonomy */
				//'parent_item_colon' => __( 'Parent Custom Tag:', 'bonestheme' ), /* parent taxonomy title */
				//'edit_item' => __( 'Edit Custom Tag', 'bonestheme' ), /* edit custom taxonomy title */
				//'update_item' => __( 'Update Custom Tag', 'bonestheme' ), /* update title for taxonomy */
				//'add_new_item' => __( 'Add New Custom Tag', 'bonestheme' ), /* add new title for taxonomy */
				//'new_item_name' => __( 'New Custom Tag Name', 'bonestheme' ) /* name title for taxonomy */
			//),
			//'show_admin_column' => true,
			//'show_ui' => true,
			//'query_var' => true,
		//)
	//);
	
	/*
		looking for custom meta boxes?
		check out this fantastic tool:
		https://github.com/jaredatch/Custom-Metaboxes-and-Fields-for-WordPress
	*/
	

?>
