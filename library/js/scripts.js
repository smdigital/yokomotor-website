/*
 * Bones Scripts File
 * Author: Eddie Machado
 *
 * This file should contain any js scripts you want to add to the site.
 * Instead of calling it in the header or throwing it inside wp_head()
 * this file will be called automatically in the footer so as not to
 * slow the page load.
 *
 * There are a lot of example functions and tools in here. If you don't
 * need any of it, just remove it. They are meant to be helpers and are
 * not required. It's your world baby, you can do whatever you want.
*/


/*
 * Get Viewport Dimensions
 * returns object with viewport dimensions to match css in width and height properties
 * ( source: http://andylangton.co.uk/blog/development/get-viewport-size-width-and-height-javascript )
*/

/* Agregar utm a subscribe form */
const queryString =  window.location.search;
const urlParams = new URLSearchParams(queryString);
var utm_source = urlParams.get('utm_source');
var inputs_utm_source=document.querySelectorAll('#origen_network_input');
var utm_validate =document.querySelectorAll('.utm-validate');
if(utm_validate){
  utm_validate.forEach(function(element){
        element.addEventListener('change', function(e){
          if(inputs_utm_source){
            inputs_utm_source.forEach(function(element){
              element.value= utm_source;
          });  
        }
      });
  });

}
/* Agregar utm a subscribe form */
function updateViewportDimensions() {
  var w = window, d = document, e = d.documentElement, g = d.getElementsByTagName('body')[0], x = w.innerWidth || e.clientWidth || g.clientWidth, y = w.innerHeight || e.clientHeight || g.clientHeight;
  return { width: x, height: y };
}
// setting the viewport width
var viewport = updateViewportDimensions();


/*
 * Throttle Resize-triggered Events
 * Wrap your actions in this function to throttle the frequency of firing them off, for better performance, esp. on mobile.
 * ( source: http://stackoverflow.com/questions/2854407/javascript-jquery-window-resize-how-to-fire-after-the-resize-is-completed )
*/
var waitForFinalEvent = (function () {
  var timers = {};
  return function (callback, ms, uniqueId) {
    if (!uniqueId) { uniqueId = "Don't call this twice without a uniqueId"; }
    if (timers[uniqueId]) { clearTimeout(timers[uniqueId]); }
    timers[uniqueId] = setTimeout(callback, ms);
  };
})();

// how long to wait before deciding the resize has stopped, in ms. Around 50-100 should work ok.
var timeToWaitForLast = 100;


/*
 * Here's an example so you can see how we're using the above function
 *
 * This is commented out so it won't work, but you can copy it and
 * remove the comments.
 *
 *
 *
 * If we want to only do it on a certain page, we can setup checks so we do it
 * as efficient as possible.
 *
 * if( typeof is_home === "undefined" ) var is_home = jQuery('body').hasClass('home');
 *
 * This once checks to see if you're on the home page based on the body class
 * We can then use that check to perform actions on the home page only
 *
 * When the window is resized, we perform this function
 * jQuery(window).resize(function () {
 *
 *    // if we're on the home page, we wait the set amount (in function above) then fire the function
 *    if( is_home ) { waitForFinalEvent( function() {
 *
 *	// update the viewport, in case the window size has changed
 *	viewport = updateViewportDimensions();
 *
 *      // if we're above or equal to 768 fire this off
 *      if( viewport.width >= 768 ) {
 *        console.log('On home page and window sized to 768 width or more.');
 *      } else {
 *        // otherwise, let's do this instead
 *        console.log('Not on home page, or window sized to less than 768.');
 *      }
 *
 *    }, timeToWaitForLast, "your-function-identifier-string"); }
 * });
 *
 * Pretty cool huh? You can create functions like this to conditionally load
 * content and other stuff dependent on the viewport.
 * Remember that mobile devices and javascript aren't the best of friends.
 * Keep it light and always make sure the larger viewports are doing the heavy lifting.
 *
*/

/*
 * We're going to swap out the gravatars.
 * In the functions.php file, you can see we're not loading the gravatar
 * images on mobile to save bandwidth. Once we hit an acceptable viewport
 * then we can swap out those images since they are located in a data attribute.
*/
function loadGravatars() {
  // set the viewport using the function above
  viewport = updateViewportDimensions();
  // if the viewport is tablet or larger, we load in the gravatars
  if (viewport.width >= 768) {
    jQuery('.comment img[data-gravatar]').each(function () {
      jQuery(this).attr('src', jQuery(this).attr('data-gravatar'));
    });
  }
} // end function

var ajaxURL = url;


jQuery('#form-services-lightbox').on('submit', function (e) {
  e.preventDefault();
  var fullnameUser_service = jQuery('#form-services-lightbox #fullname').val();
  var phoneUser_service = jQuery('#form-services-lightbox #phone').val();
  var name_service = jQuery('#form-services-lightbox #service').val();
  var emailUser_service = jQuery('#form-services-lightbox #email').val();
  var date_service = jQuery('#form-services-lightbox #date').val();
  var city_service = jQuery('#form-services-lightbox #cities').val();
  var workshop_service = jQuery('#form-services-lightbox #workshop').val();
  var terms_service = jQuery('#form-services-lightbox #acepto-terms-citas').is(':checked') ? 1 : 0;

  if (fullnameUser_service != '' && phoneUser_service != '' && emailUser_service != '' && date_service != '' && city_service != '' && workshop_service != '') {
    var dataService = {
      fullname: fullnameUser_service,
      phone: phoneUser_service,
      name: name_service,
      email: emailUser_service,
      date: date_service,
      city: city_service,
      workshop: workshop_service,
      terms: terms_service,
      nonce: ajaxURL.nonce,
      action: 'insert_services_date'
    };
    jQuery.ajax({
      url: ajaxURL.ajax_url,
      type: 'POST',
      data: dataService
    })
      .done(function (data) {
        var response = data;
        if (response.success) {
          jQuery('#form-services-lightbox').trigger("reset");
          jQuery('.message').text('Solicitud enviada');
          setTimeout(function () {
            jQuery('.message').hide();
            jQuery('.in').fadeOut();
            jQuery("#modal-form-serivicio").modal('toggle')
          }, 500);
        }
      })
      .fail(function (error) {
        console.log('Failed: ', error)
      })
  } else {
    jQuery('.message').text('Datos incompletos');
  }
})

jQuery('#form-workshop-date').on('submit', function (e) {
  e.preventDefault();
  var fullnameUser_service = jQuery('#form-workshop-date #fullname').val();
  var phoneUser_service = jQuery('#form-workshop-date #phone').val();
  var name_service = jQuery('#form-workshop-date #service').val();
  var emailUser_service = jQuery('#form-workshop-date #email').val();
  var date_service = jQuery('#form-workshop-date #date').val();
  var city_service = jQuery('#form-workshop-date #cities').val();
  var workshop_service = jQuery('#form-workshop-date #workshop').val();
  var placa_service = jQuery('#form-workshop-date #placa').val();
  var terms_service = jQuery('#form-workshop-date #acepto-terms-citas').is(':checked') ? 1 : 0;

  if (fullnameUser_service != '' && phoneUser_service != '' && emailUser_service != '' && date_service != '' && city_service != '' && workshop_service != '') {
    var dataService = {
      fullname: fullnameUser_service,
      phone: phoneUser_service,
      name: name_service,
      email: emailUser_service,
      date: date_service,
      city: city_service,
      workshop: workshop_service,
      placa: placa_service,
      terms: terms_service,
      nonce: ajaxURL.nonce,
      action: 'insert_services_date'
    };
    jQuery.ajax({
      url: ajaxURL.ajax_url,
      type: 'POST',
      data: dataService
    })
      .done(function (data) {
        var response = data;
        if (response.success) {
          jQuery('#form-workshop-date').trigger("reset");
          jQuery('.message').text('Solicitud enviada');
          setTimeout(function () {
            jQuery('.message').hide();
          }, 500);
        }
      })
      .fail(function (error) {
        console.log('Failed: ', error)
      })
  } else {
    jQuery('.message').text('Datos incompletos');
  }
})

/*
 * Put all your regular jQuery in here.
*/
jQuery(document).ready(function ($) {

  /*
   * Let's fire off the gravatar function
   * You can remove this if you don't need it
  */
  loadGravatars();

  AOS.init();

  if (jQuery('.swiper-banner-home').length > 0) {
    var swiper = new Swiper(".swiper-banner-home", {
      spaceBetween: 0,
      effect: "fade",
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".banner-pagination",
        type: "fraction",
      },
      navigation: {
        nextEl: ".next-banner",
        prevEl: ".prev-banner",
      },
    });
  };

  if (jQuery('.swiper-exterior').length > 0) {
    var sliders = jQuery('.swiper-exterior');
    if (sliders && sliders.length > 0) {
      sliders.each(function (index) {
        var $target = jQuery(this);

        sliders.eq(index).addClass('swiper-caracteristicas' + index);
        sliders.eq(index).parent().find('.next-exterior').addClass('next-index-' + index);
        sliders.eq(index).parent().find('.prev-exterior').addClass('prev-index-' + index);
        new Swiper(this, {
          spaceBetween: 0,
          effect: "fade",
          autoplay: {
            delay: 5000,
            disableOnInteraction: false,
          },
          pagination: {
            el: $target.siblings('.exterior-pagination-caracteristicas' + index),
            type: "fraction",
          },
          navigation: {
            nextEl: '.next-index-' + index,
            prevEl: '.prev-index-' + index,
          },
        });
      });
    }
  };

  if (jQuery('.swiper-modelos-small').length > 0) {
    var swiper = new Swiper(".swiper-modelos-small", {
      slidesPerView: 2,
      spaceBetween: 0,
      centeredSlides: true,
      loop: false,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".modelo-pagination",
        type: "fraction",
      },
      navigation: {
        nextEl: ".next-modelo",
        prevEl: ".prev-modelo",
      },
      breakpoints: {
        640: {
          slidesPerView: 2,
          spaceBetween: 0,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 0,
        },
        1024: {
          slidesPerView: 3,
          spaceBetween: 0,
        },
      },
    });
  };

  var swiperColor = new Swiper(".swiper-thums-color", {
    spaceBetween: 5,
    slidesPerView: 5,
    watchSlidesProgress: true,
  });
  var coloresCar = new Swiper(".swiper-car-color", {
    effect: "fade",
    spaceBetween: 0,
    navigation: {
      nextEl: ".next-color",
      prevEl: ".prev-color",
    },
    thumbs: {
      swiper: swiperColor,
    },
  });

  var swiperAccesorio = new Swiper(".swiper-thums-accesorio", {
    spaceBetween: 20,
    slidesPerView: 2,

    watchSlidesProgress: true,
    breakpoints: {
      640: {
        slidesPerView: 4,
        spaceBetween: 10,
      },
      768: {
        slidesPerView: 5,
        spaceBetween: 15,
      },
      1024: {
        slidesPerView: 6,
        spaceBetween: 15,
      },
    },
  });
  var accesorioCar = new Swiper(".swiper-accesorio", {
    effect: "fade",
    spaceBetween: 0,
    autoHeight: true,
    navigation: {
      nextEl: ".next-accesorio",
      prevEl: ".prev-accesorio",
    },
    thumbs: {
      swiper: swiperAccesorio,
    },
  });


  if (jQuery('.swiper-banner-accesorio').length > 0) {
    var swiper = new Swiper(".swiper-banner-accesorio", {
      spaceBetween: 0,
      effect: "fade",
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".banner-pagination",
        type: "fraction",
      },
      navigation: {
        nextEl: ".next-banner",
        prevEl: ".prev-banner",
      },
    });
  };

  if (jQuery('.swiper-ropa').length > 0) {
    var swiper = new Swiper(".swiper-ropa", {
      spaceBetween: 0,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".pagination-ropa",
        type: "fraction",
      },
      navigation: {
        nextEl: ".next-ropa",
        prevEl: ".prev-ropa",
      },
    });
  };


  if (jQuery('.swiper-clothes').length > 0) {
    var swiper = new Swiper(".swiper-clothes", {
      slidesPerView: 2,
      spaceBetween: 20,
      autoplay: {
        delay: 7000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".pagination-clothes",
        type: "fraction",
      },
      navigation: {
        nextEl: ".next-clothes",
        prevEl: ".prev-clothes",
      },
      breakpoints: {
        640: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 4,
          spaceBetween: 30,
        },
      },
    });
  };


  if (jQuery('.swiper-marcas-circle').length > 0) {
    var swiper = new Swiper(".swiper-marcas-circle", {
      slidesPerView: 3,
      spaceBetween: 5,
      autoplay: {
        delay: 7000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".pagination-marcas", 
        clickable: true,       
      },
      
      breakpoints: {
        640: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
      },
    });
  };


  if (jQuery('.swiper-categoria-usados').length > 0) {
    var swiper = new Swiper(".swiper-categoria-usados", {
      slidesPerView: 3,
      spaceBetween: 5,
      autoplay: {
        delay: 7000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".pagination-categoria-usados", 
        clickable: true,       
      },
      
      breakpoints: {
        640: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        768: {
          slidesPerView: 3,
          spaceBetween: 10,
        },
        1024: {
          slidesPerView: 5,
          spaceBetween: 30,
        },
      },
    });
  };

    if (jQuery('#tabs-filter-xl').length > 0) {
        jQuery('#tabs-filter-xl h4').click(function() {
            jQuery(this).toggleClass('open-nav');                        
            jQuery(this).next().slideToggle();
        });
    }

  if (jQuery('.cont-share').length > 0) {
    jQuery('.cont-share').click(function () {
      jQuery(this).toggleClass('open-share');
    });
  }


  if (jQuery('.swiper-car-full').length > 0) {
    var swiper = new Swiper(".swiper-car-full", {
      spaceBetween: 30,
      autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      pagination: {
        el: ".pagination-car",
        clickable: true,
      },
      navigation: {
        nextEl: ".next-car",
        prevEl: ".prev-car",
      },
    });
  };

  if (jQuery('#tabs-sedes').length > 0) {
    jQuery("#tabs-sedes").tabs();
  };

  if (jQuery('#tabs-tags-blog').length > 0) {
    jQuery("#tabs-tags-blog").tabs();
  };

  if (jQuery('#tabs-features-specs').length > 0) {
    jQuery("#tabs-features-specs").tabs();
  };

  if (jQuery('#tabs-disenos').length > 0) {
    jQuery("#tabs-disenos").tabs();
  };

  if (jQuery('#tabs-destacados, #tabs-sedes-all').length > 0) {
    jQuery("#tabs-destacados,  #tabs-sedes-all").tabs();
  };

  if (jQuery('#tabs-cotizador').length > 0) {
    jQuery("#tabs-cotizador").tabs();
  };

  if (jQuery('#tabs-gral-cotizador').length > 0) {
    jQuery("#tabs-gral-cotizador").tabs();
  };

  



  if (jQuery('.sedes-accordion').length > 0) {
    jQuery(".sedes-accordion").accordion({
      heightStyle: "content",
      collapsible: true,
      active: false
    });
  };

  if (jQuery('.accordion-detalles').length > 0) {
    jQuery(".accordion-detalles").accordion({
      heightStyle: "content",
      collapsible: true,
    });
  };

  if (jQuery('#icon-share-mns').length > 0) {
    jQuery('#icon-share-mns').click(function () {
      jQuery(this).toggleClass('active');
      jQuery('.share-icon-yokomotor').toggleClass('open-icons');
    });
  }


   /**
   * Basic Tooltip Menú contactenos
   */

    if (jQuery('.custom-toltip-menu').length > 0) {

      jQuery('.custom-toltip-menu').click(function () {
        // Hidden all tooltips by default
  
        if (jQuery(this).hasClass('open-tooltip')) {
          jQuery(this).find('.tooltip-shop').hide();
          jQuery(this).find('.tooltip-cities').hide('fast');
        }
  
        // Add class toggleClass in cities
  
        jQuery(this).toggleClass('open-tooltip');
        jQuery(this).find('.tooltip-cities').toggleClass('open-date').show();
  
      });
  
      // Click when you choose #tooltip-cities
  
      jQuery('.tooltip-cities').on('click', 'li', function () {
        var $target = jQuery(this);
        var city = $target.data('city');
        
        // Class parent  btn-tooltip
        $target.parent().parent().toggleClass('open-tooltip');
        // Show .tooltip-shop 
        $target.parent().next().toggleClass('open-date');
        $target.parent().next().show();
       
        // Hide .tooltip-shop when you in #tooltip-cities
        $target.parent().hide('fast');
        
        // Iterator items #tooltip-shop li, validating the city
        $target.parent().next().find('li').each(function (key, value) {
          
          jQuery(this).hide();
          if (city == jQuery(this).data('shop')) {
            jQuery(this).show();
          }
        });
      });
    }

  /**
   * Basic Tooltip financiacion(esta commentado en la page)
   */

  if (jQuery('.details-workshop .btn-tooltip, .accesorios-detailed-shop .btn-tooltip').length > 0) {

    jQuery('.details-workshop .btn-tooltip, .accesorios-detailed-shop .btn-tooltip').click(function () {
      // Hidden all tooltips by default

      if (jQuery(this).hasClass('open-tooltip')) {
        jQuery(this).find('.tooltip-shop').hide();
        jQuery(this).find('.tooltip-cities').hide('fast');
      }

      // Add class toggleClass in cities

      jQuery(this).toggleClass('open-tooltip');
      jQuery(this).find('.tooltip-cities').toggleClass('open-date').show();

    });

    // Click when you choose #tooltip-cities

    jQuery('.tooltip-cities').on('click', 'li', function () {
      var $target = jQuery(this);
      var city = $target.data('city');
      
      // Class parent  btn-tooltip
      $target.parent().parent().toggleClass('open-tooltip');
      // Show .tooltip-shop 
      $target.parent().next().toggleClass('open-date');
      $target.parent().next().show();
     
      // Hide .tooltip-shop when you in #tooltip-cities
      $target.parent().hide('fast');
      
      // Iterator items #tooltip-shop li, validating the city
      $target.parent().next().find('li').each(function (key, value) {
        
        jQuery(this).hide();
        if (city == jQuery(this).data('shop')) {
          jQuery(this).show();
        }
      });
    });
  }

  /**
   * Random Tooltip , selecting a number
   */

  if (jQuery('.details-shop-ropa .btn-tooltip').length > 0) {

    jQuery('.details-shop-ropa .btn-tooltip').click(function () {

      // Hidden all tooltips by default

      if (jQuery(this).hasClass('open-tooltip')) {
        jQuery(this).find('.tooltip-shop').hide();
        jQuery(this).find('.tooltip-cities').hide('fast');
      }

      // Add class toggleClass in cities

      jQuery(this).toggleClass('open-tooltip');
      jQuery(this).find('.tooltip-cities').toggleClass('open-date').show();

    });

    // Click when you choose #tooltip-cities

    jQuery('.tooltip-cities').on('click', 'li', function () {
      var $target = jQuery(this);
      var city = $target.data('city');
      var randNumber = [];
      
      // Class parent  btn-tooltip
      $target.parent().parent().toggleClass('open-tooltip');
      // Show #tooltip-shop 
      $target.parent().next().toggleClass('open-date');
      $target.parent().next().show();
       // Hide #tooltip-shop when you in #tooltip-cities
       $target.parent().hide('fast');
       // Iterator items #tooltip-shop li, validating the city
       $target.parent().next().find('li').each(function (key, value) {
        jQuery(this).hide();
         if(city == jQuery(this).data('shop')) {
          randNumber.push(key);
        }
      });

      // Random items and select only one
      var item = randNumber[Math.floor(Math.random() * randNumber.length)];
      $target.parent().next().find('li').eq(item).show();

      // Selected size and unit for whatsaap message boutique single

      var href        = $target.parent().next().find('li').eq(item);
      var url         = href.find('a').attr('href');
      var searchUnit  = url.replace("valorunidades", jQuery('.result').val()); 
      var searchSize  = searchUnit.replace("valortalla", jQuery('.selected-size').val()); 
      var message     = searchSize.replace("message", jQuery('.message').val()); 
      
      href.find('a').attr('href', message);
      
    });
  }

  if (jQuery('.swiper-nav-car').length > 0) {
    jQuery('.swiper-nav-car').each(function (index) {
      var $target = jQuery(this);
      var swiperAddClass = 'swiper-nav-car' + index;
      $target.addClass(swiperAddClass);
      jQuery('.swiper-nav-car').eq(index).parent().find('.nav-pagination').addClass('nav-pagination' + index);

      // Swiper para el mega-menu
      var swiper = new Swiper("." + swiperAddClass, {
        slidesPerView: 6,
        spaceBetween: 0,
        pagination: {
          el: ".nav-pagination" + index,
          clickable: true,
        },
      });
    })
  }

  jQuery(function () {
    jQuery('.main-tabs-car').each(function (index) {
      jQuery(this).addClass('tab-car' + index);
      jQuery('.tab-car' + index).tabs({
        event: "mouseover"
      });
    });

    /* jQuery( ".main-tabs-car" ).tabs({
      beforeActivate: function( event, ui ) {
        jQuery('.max_mega_menu-container .main-tabs-car .conten-car-menu').hide();
        console.log(ui);
      }
    }); */
  });

  jQuery(window).on("load", function () {
    setTimeout(function () {
      jQuery('.main-prehome').removeClass('disabled');
    }, 2000);
  });

  if (jQuery('.nav-ppal .menu').length > 0) {

    var hideParent = jQuery('.max_mega_menu-container .tabs-car-menu');
    hideParent.hide();
    var $mainHeader = jQuery('.bar-principal');

    jQuery($mainHeader).on('mouseover', '.nav-ppal .menu .menu-item', function () {
      var elId = jQuery(this).attr('id') ? jQuery(this).attr('id').replace('menu-item-', '') : false;
      if (!elId) return; // Fallback

      hideParent.hide();

      jQuery('.nav-ppal .menu .menu-item').removeClass('active');
      jQuery(this).addClass('active');

      jQuery(hideParent).each(function (index) {
        var $target = jQuery(this);
        var data = $target.data('menu-parent');
        if (data == elId) {
          $target.show();
          return false;
        }
      });


      if (jQuery(this).hasClass('active') && (jQuery(this).hasClass('menu-item-has-children'))) {
        console.log(elId);
        jQuery('.wrap-mega-menu').removeClass('visible_mega-menu');
        /**jQuery('.max_mega_menu-container').hasClass('item-max-megamenu-145');
*/
        $megamenu = jQuery('.max_mega_menu-container');
        if ($megamenu.hasClass('item-max-megamenu-' + elId)) {
          $megamenu.show();
          jQuery('.item-max-megamenu-' + elId + ' .wrap-mega-menu').addClass('visible_mega-menu');

        } else {
          jQuery('#menu-menu-principal .sub-menu').hide();
          jQuery('#menu-menu-principal #menu-item-' + elId).find('.sub-menu').show();
        }


        /* jQuery('.max_mega_menu-container').data('item', elId).find('.wrap-mega-menu').addClass('visible_mega-menu'); */

      } else {
        jQuery('.wrap-mega-menu').removeClass('visible_mega-menu');

      }

    });

    jQuery($mainHeader).on('mouseleave', function () {
      jQuery('.btn-nuevos').removeClass('active');
      jQuery('.nav-ppal .menu .menu-item').removeClass('active');
      jQuery('.wrap-mega-menu').removeClass('visible_mega-menu');
      jQuery('#menu-menu-principal .sub-menu').hide();
    });
  }

  if (jQuery('.dl-menuwrapper button').length > 0) {
    jQuery('.dl-menuwrapper button').click(function () {
      jQuery('body').toggleClass('shadow-body');
    });
  }

    //*********** Filtro para mobile *********//
    if (jQuery('#btn-orderby-xs').length > 0) {
            jQuery('#btn-orderby-xs').click(function () {
            jQuery('body').toggleClass('shadow-body-orderby');            
            jQuery('.modal-orderby-xs').toggleClass('open-orderby');

            jQuery('.modal-filter-all-xs').removeClass('open-orderby');
            jQuery('body').removeClass('shadow-body-filter-all');
        });
    }
    if (jQuery('.modal-orderby-xs .cerrar').length > 0) {
            jQuery('.modal-orderby-xs .cerrar').click(function () {
            jQuery('body').toggleClass('shadow-body-orderby');            
            jQuery('.modal-orderby-xs').toggleClass('open-orderby');
        });
    }

    if (jQuery('#btn-filter-all-xs').length > 0) {
            jQuery('#btn-filter-all-xs').click(function () {
            jQuery('body').toggleClass('shadow-body-filter-all');            
            jQuery('.modal-filter-all-xs').toggleClass('open-orderby');

            jQuery('.modal-orderby-xs').removeClass('open-orderby');
            jQuery('body').removeClass('shadow-body-orderby'); 
        });
    }
    if (jQuery('.modal-filter-all-xs .cerrar').length > 0) {
            jQuery('.modal-filter-all-xs .cerrar').click(function () {
            jQuery('body').toggleClass('shadow-body-filter-all');            
            jQuery('.modal-filter-all-xs').toggleClass('open-orderby');
        });
    }


    if (jQuery('#tabs-filter-xs').length > 0) {
        jQuery("#tabs-filter-xs").accordion({
          heightStyle: "content",
          collapsible: true,
          active: false
        });
    };

    

  jQuery('#tabs-tags-blog ul.titles-tags li a').click(function () {
    var category = jQuery(this).attr('id');
    var optionSelected = [];
    if (category == 'all') {
      var options = jQuery('#tabs-tags-blog ul.titles-tags li a');
      options.each(function () {
        optionSelected.push(jQuery(this).attr('id'));
      });
    } else {
      optionSelected.push(category);
    }

    var data = { target: "1645724265", taxonomyTerms: optionSelected.toString() };
    ajaxloadmore.filter('fade', '300', data);
  })


  jQuery(function () {
    jQuery('#dl-menu').dlmenu();
  });

  /**
  * Contact form selects
  */

  if (jQuery('.form-yokomotor').length > 0) {

    $choiceCity = jQuery('.form-yokomotor #city');
    $choicePlate = jQuery('.form-yokomotor .select-email');
    jQuery('select').each(function () {
      $strReplace = (jQuery(this).find('option').eq(0).text() === '---') ? jQuery(this).find('option').eq(0).text('Selecciona') : false;
    });

    $choiceCity.on('change', function () {
      var city = jQuery(this).val();

      switch (city) {
        case "Medellín":
          jQuery('.form-yokomotor .select-bogota').hide();
          jQuery('.form-yokomotor .select-default').hide();
          jQuery('.form-yokomotor .select-medellin').show();
          break;
        case "Bogotá":
          jQuery('.form-yokomotor .select-default').hide();
          jQuery('.form-yokomotor .select-medellin').hide();
          jQuery('.form-yokomotor .select-bogota').show();
          break;
        default:
          jQuery('.form-yokomotor .select-bogota').hide();
          jQuery('.form-yokomotor .select-medellin').hide();
          jQuery('.form-yokomotor .select-default').show();
          break;
      }
    });

    $choicePlate.on('change', function () {
      var plate = jQuery(this).val();
      jQuery('.form-yokomotor #choice_plate').val(plate);
    });
  };

  jQuery(".btn-form-asesor").on('click', function(){
    var email = jQuery("#email-asesor").val()
    var sede = jQuery("#sede-asesor").val()
    jQuery("#sede_asesor").val(sede)
    jQuery("#email_asesor").val(email)

  })

  jQuery(".cta-service").on('click', function(){
        var div = this.parentElement;
        var serviceName = div.querySelector('.serviceName').value;
        
        setTimeout(function(){
          document.querySelector('#choice_service').value = serviceName;
          if(!jQuery('#modal-form-serivicio').is(':visible')){
            jQuery('#modal-form-serivicio').modal('show')
          }
        },200)
      });
    


   /**
  * Modal and Contact form page financiación
  */

    if (jQuery('.main-service-workshop').length > 0) {
      jQuery('.card-workshop-service').on('click', '.btn-yokomotor', function() {

        var $target = jQuery(this).parent().parent().find('h2').text();
        console.log($target);
        jQuery('.modal-financiacion').find('form #plan').val($target);
      })
    }
  

  /**
  * Modal and Contact form selects Taxonomy accesories
  */

  if (jQuery('.main-accesorios-marcas').length > 0) {
    jQuery('.col-cards').on('click', '.card-marca-accesorios', function () {
      var $accesoryTitle = jQuery(this).find('h6').text();
      console.log($accesoryTitle)
      var $formModal = jQuery('#modal-form-accesories .form-yokomotor')
      $formModal.find('h1 span').text($accesoryTitle);
      $formModal.find('form #accesory').val($accesoryTitle);
	  jQuery('#modal-form-accesories').modal('toggle')
    });
  }

  /**
* Modal and Contact form selects Button vehicles
*/

  if (jQuery('#modal-form-vehicles').length > 0) {
    jQuery('#modal-form-vehicles').on('shown.bs.modal', function () {
      var $vehicleTitle = jQuery(this).find('h1 span').text();
      jQuery(this).find('.form-yokomotor #vehicle').val($vehicleTitle);
    });
  }

  /**
   * Boutique button min max
   */

   if (jQuery('.details-shop-ropa').length > 0) {
     
    jQuery('.plus').on('click', function() {
      $inputResult = parseInt(jQuery('.result').val()); 
      jQuery('.result').val($inputResult + 1);
    });

    jQuery('.minus').on('click', function() {
      $inputResult = parseInt(jQuery('.result').val()); 
      jQuery('.result').val($inputResult - 1);
    })
  }

  /**
   * Contact form  events
   * 
   */
   if (jQuery('.form-yokomotor').length > 0) {

    var wpcf7Elm        = document.querySelector( '.form-yokomotor .wpcf7' );
    var wpcf7ElmTitle   = document.querySelector( '.form-yokomotor h1' );
    var wpcf7ElmRow     = document.querySelector( '.form-yokomotor .row' );
    var wpcf7ElmChecked = document.querySelector( '.form-yokomotor .checked-red' );
    var wpcf7ElmSubmit  = document.querySelector( '.form-yokomotor .wpcf7-submit' );
    var wpcf7ElmSpinner = document.querySelector( '.form-yokomotor .wpcf7-spinner' );

    if(wpcf7Elm){
      wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
        if(wpcf7ElmTitle){wpcf7ElmTitle.style.display = 'none';}
        if(wpcf7ElmRow){wpcf7ElmRow.style.display = 'none';}
        if(wpcf7ElmChecked){wpcf7ElmChecked.style.display = 'none';}
        if(wpcf7ElmSubmit){wpcf7ElmSubmit.style.display = 'none';}
        if(wpcf7ElmSpinner){wpcf7ElmSpinner.style.display = 'none';}

        /* wpcf7Elm.innerHTML='<p>¡Gracias por contactarte con nosotros! Nos pondremos en contacto contigo muy pronto.</p>'; */

      }, false );
    }
   }

}); /* end of as page load scripts */