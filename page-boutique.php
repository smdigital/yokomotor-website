    <?php get_header(); the_post();

    $customType = 'yokomotor_boutique';
    $taxonomy = 'boutique_cats';
    $taxterms = wp_get_post_terms( get_the_ID(), $taxonomy, array( 'hide_empty' => false, 'fields' => 'ids' ));


    /**	
         * 
         * Get Section Banner
         * @source 'page-parts/sections/' 'section-feature-banner.php'
         * 
         */
        get_template_part( 'page-parts/sections/section-feacture', 'banner'); ?>


    <section class="main-usados-destacados full clear-fix">
        <div class="wrapper-main-sm center">
            <h3>Boutique</h3>
            
            <div class="row-swiper-clothes">
                <div class="swiper swiper-clothes">
                    <div class="swiper-wrapper">
                        <?php 
                            /**
                             * * ***************
                            * ACF Custom fields Usados (post-type)
                            * ***************
                            * @param ACF_fields 'price'
                            * @param ACF_fields 'highlight_car'
                            * 
                            */
                            $featured = get_posts( array( 'post_type' => 'yokomotor_boutique', 'numberposts' => 100,) );
                            foreach($featured as $feature):
                        ?>
  
                                <div class="swiper-slide">
                                    <article class="scart-usado-destacado">
                                    <a data-toggle="modal" data-target="#exampleModal<?php echo $feature->ID; ?>"> 
                                            <?php if(has_post_thumbnail($feature->ID)): ?>
                                                <figure>
                                                    <img src="<?php echo esc_url(get_the_post_thumbnail_url($feature->ID, 'full')) ?>" alt="<?php echo esc_attr($feature->post_name); ?>" width="">
                                                </figure>
                                            <?php endif; ?>
                                            
                                        
                                        </a>
                                    </article>
                                </div>


                             
                        <?php 	
                            endforeach;
                        ?>

                    </div>
                </div>
                <div class="button-next next-black next-clothes"></div>
                <div class="button-prev prev-black prev-clothes"></div>
                <div class="swiper-pagination pagination-clothes"></div>
            </div>
        
        </div>
        <?php 
                            /**
                             * * ***************
                            * ACF Custom fields Usados (post-type)
                            * ***************
                            * @param ACF_fields 'price'
                            * @param ACF_fields 'highlight_car'
                            * 
                            */
                            $featured = get_posts( array( 'post_type' => 'yokomotor_boutique', 'numberposts' => 100,) );
                            foreach($featured as $feature):
        ?>


 <div class="modal fade" id="exampleModal<?php echo $feature->ID; ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <img src="<?php echo esc_url(get_the_post_thumbnail_url($feature->ID, 'full')) ?>" alt="<?php echo esc_attr($feature->post_name); ?>" >
      </div>    
    </div>
  </div>
</div>

<?php 	
                            endforeach;
                        ?>
</section>
  
 <?php get_footer(); ?>