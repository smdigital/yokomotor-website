<?php get_header(); the_post();?>
<section class="main-campaign-all full clear-fix pt-5 pt-3-xs">
	<div class="wrapper-main center">	    	
		<?php the_title('<h1>', '</h1>'); ?>

		<?php echo do_shortcode('[ajax_load_more id="campaing" loading_style="infinite skype" container_type="div" theme_repeater="template-campaings.php" post_type="yokomotor_campaigns" posts_per_page="6" transition_container_classes="row row-xs" no_results_text="<p><strong>No se encontro resultados.</strong></p>"]');?>	
	</div>
</section>
<?php 
	/**
		 * * ***************
		* @source ACF ARCHIVE PLUGIN
		* ACF Custom fields Campaigns (Option page)
		* ***************
		* @param ACF_fields 'yokomotor_newsletter'
		* 
		*/
		$sectionForm = get_field( 'yokomotor_newsletter', 'yokomotor_campaigns' ); 
		
	if( $sectionForm['enable_section'] && ($sectionForm['choice_form'] && !empty($sectionForm['choice_form'])) ): 
				$form = $sectionForm['choice_form'];?>
		<section class="main-after-sales full clear-fix">
			<div class="wrapper-main center">
				<?php if( $sectionForm['description'] && !empty($sectionForm['description']) ) {
					echo $sectionForm['description'];
				}; 

			  echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); ?>	
			</div>
		</section>
	<?php endif; ?>


    

<?php get_footer(); ?>