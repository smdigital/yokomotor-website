<?php get_header(); 

	/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner');

?>
<section class="main-cita-taller full clear-fix">
	<div class="wrapper-main center">
		<h1><?php the_title(); ?></h1>
		<?php 
		/**
			 * * ***************
			* ACF Custom fields Página Cita Taller (page)
			* ***************
			* @param ACF_fields 'yokomotor_details_cita'
				* @param ACF_subfields 'enable_section'
				* @param ACF_subfields 'details'
			* 
		*/
			$detailsSection = get_field('yokomotor_details_cita'); 

			if($detailsSection && $detailsSection['enable_section']):
				$cf = $detailsSection['cf-cita-taller'];
		?>
		<hr>
		<div class="details-cita">
			<?php echo $detailsSection['details'];?>
		</div>
		<hr>
		
		<section class="form-yokomotor full clear-fix">
			<?php the_content(); ?>
			<?php echo do_shortcode($cf);?>
		</section>
		<?php endif; ?>
	</div>
</section>
<?php
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/buttons/button-rate' 'us.php'
	 * 
	 */
	get_template_part( 'page-parts/buttons/button-rate', 'us');
?>

<?php get_footer(); ?>