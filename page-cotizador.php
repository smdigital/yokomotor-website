<?php get_header(); 

	/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner');
	
	/**
	 * * ***************
	* ACF Custom fields Simulador y cotizador (page)
	* ***************
	* @param ACF_fields 'yokomotor_form'
	* @param ACF_fields 'yokomotor_infopage'
		* @param ACF_subfields 'title'
		* @param ACF_subfields 'text'
	* 
	*/
	$infoPage = get_field('yokomotor_infopage'); 
	$title = $infoPage['title'];
	$text = $infoPage['text'];
?>
<section class="main-cita-taller full clear-fix main-cotizador">
	<div class="wrapper-main-sm center">
		<h1><?php echo $title;?></h1>		
		<div class="leyeng-cotizador">
			<p><?php echo strip_tags($text, '<br>'); ?></p></div>
		<?php 
			$formVehicles = get_field( 'yokomotor_form' ); 
			if( $formVehicles['choice_form'] && !empty($formVehicles['choice_form']) ){
				$form = $formVehicles['choice_form'];
				echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); 
			}
		?>
		
	</div>
</section>
<?php get_footer(); ?>
