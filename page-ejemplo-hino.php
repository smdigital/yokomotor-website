<?php get_header(); ?>
<div class="d-none-">

	<a href="https://supple.live/dqQKvaGdm3qtnPK27" target="_blank" class="btn-calificar">Califícanos</a>

<!-- Lightbox Campañas -->
<a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-form-campanas">Ver campañas</a>

<div id="modal-form-campanas" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<section class="lightbox-generico full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<hr>
			<h1>Lorem ipsum</h1>
			<figure>
				<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/hino/hino-1.png" alt="">
			</figure>
			<article>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non corporis eaque impedit corrupti eos, maiores ex optio, soluta quaerat dolor! Nesciunt, eaque architecto, magnam velit obcaecati facere. Vitae, reprehenderit, modi.</p>
				<p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Numquam facilis quibusdam dolor? Sunt atque dolor, iure pariatur minima animi, asperiores! Perspiciatis et enim nemo doloribus asperiores pariatur, provident consectetur vel.</p>
			</article>
		</section>
	</div>
</div>

<!-- Modelos -->
<section class="modelos-home-hino full clear-fix">
	<div class="wrapper-main center">
		<h2>MODELOS</h2>
		<div class="main-car full clear-fix relative pt-2-xs">
			<div class="swiper swiper-car-full">
				<div class="swiper-wrapper">
					<div class="swiper-slide">
						<article class="card_car-details">
							<h3>LOREM IMSUM DOLOR</h3>
							<h4>$xxx.xxx.xxx</h4>
							<figure class="animation-car delay-0">
								<a href="">									
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/hino/hino-1.png" alt="">
								</a>
							</figure>
							<a href="" class="btn-yokomotor-arrow">VER MÁS</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card_car-details">
							<h3>LOREM DOLOR AMEDD IMSUM </h3>
							<h4>$xxx.xxx.xxx</h4>
							<figure class="animation-car delay-0">
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/hino/hino-1.png" alt="">
								</a>
							</figure>
							<a href="" class="btn-yokomotor-arrow">VER MÁS</a>
						</article>
					</div>
					<div class="swiper-slide">
						<article class="card_car-details">
							<h3>LOREM DOLOR AMEDD IMSUM </h3>
							<h4>$xxx.xxx.xxx</h4>
							<figure class="animation-car delay-0">
								<a href="">
									<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/hino/hino-1.png" alt="">
								</a>
							</figure>
							<a href="" class="btn-yokomotor-arrow">VER MÁS</a>
						</article>
					</div>					
				</div>
			</div>
			<div class="next-car button-next next-black"></div>
  			<div class="prev-car button-prev prev-black"></div>
			<div class="pagination-square bullet-gray pagination-car swiper-pagination"></div>
		</div>
	</div>
</section>

<!-- Hino connect -->
<section class="hino-connect full clear-fix">
	<div class="wrapper-main center">
		<div class="bg-hino-connect full clear-fix">
			<article>
				<h2>HINO CONNECT</h2>
				<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos iste veritatis neque, ad assumenda minima pariatur quaerat repudiandae inventore quam voluptatibus deserunt minima ad, voluptate tempora.</p>
				<a href="" class="btn-yokomotor-arrow">VER MÁS</a>
			</article>
		</div>
	</div>	
</section>


</div>
		
<section class="full main-404">
	<div class="wrapper-main center">
		<h2>error 404</h2>
		<h3>La página que <br> buscas no existe</h3>

		<a href="/" class="btn-yokomotor">VOLVER AL INICIO</a>
		
	</div>
</section>

<?php get_footer(); ?>
