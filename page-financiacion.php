<?php get_header(); 

/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner');
?>


<div class="main-service-workshop main-wrap-secure full clear-fix">
	<?php the_title('<h1>', '</h1>'); 
	if( get_the_content() ): ?>
		<article>
			<?php the_content(); ?>
		</article>
	<?php endif; 
	if( function_exists( 'get_field' ) ):
		/**
			 * * ***************
			* ACF Custom fields Página financing (page)
			* ***************
			* @param ACF_fields 'yokomotor_dues'
			* 
			*/

			$articlesSection = get_field( 'yokomotor_dues' );

		if( $articlesSection && ($articlesSection['enable_section'] &&count($articlesSection['articles']) > 0) ): $articles = $articlesSection['articles']; ?>
			<div class="row-workshop">
				<?php foreach( $articles as $article ): 
					$image = $article['image']; ?>
					<section class="card-workshop-service full clear-fix">
						<div class="row row-xs center-vertical">
							<div class="col-12 col-sm-6 col-lg-6 col-xl-6 img-featured">
								<?php if( $image ): ?>
									<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
										<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['title']); ?>">
									</figure>
								<?php endif; ?>
							</div>
							<div class="col-12 col-sm-6 col-lg-6 col-xl-6 details-workshop">
								<?php if( !empty($article['title']) ): ?>
									<h2><?php echo $article['title']; ?></h2>
								<?php endif; 
								if( !empty($article['description']) ) {
									echo $article['description'];
								}; 
									/**
								 * * ***************
								* ACF Custom fields Página financing (page)
								* ***************
								* @param ACF_fields 'yokomotor_adviser'
								* 
								*/

								//$adviserSection = get_field( 'yokomotor_adviser' );

							/*	if( $adviserSection && ($adviserSection['enable_section'] &&count($adviserSection['contacs']) > 0) ): $advisers = $adviserSection['contacs']; ?>
								<div class="text-center-btn">
									<div class="btn-tooltip">
										<div class="btn-yokomotor icon-whatsapp"><?php echo $adviserSection['title']; ?></div>
										<ul class="tooltip-cities">
											<?php foreach( $advisers as $key => $adviser ): ?>
												<li data-city="city-<?php echo $key; ?>">
													<a href="javascript:;"><strong><?php echo $adviser['city']; ?> </strong></a>
												</li>
											<?php endforeach; ?>
										</ul>	
										<ul class="tooltip-shop" style="display: none;">
											<?php foreach( $advisers as $key => $adviser ): $shops = $adviser['shop']; 
												foreach( $shops as $shop ): ?>
													<li data-shop="city-<?php echo $key; ?>">
														<?php if( $shop['phone'] && !empty($shop['phone']) ): ?>
															<a href="https://api.whatsapp.com/send?phone=57<?php echo esc_html($shop['phone']); ?>&text=<?php echo( $adviserSection['whatsaap']) ? $adviserSection['whatsaap'] : 'Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor';?>" target="_blank">
															<?php echo( $shop['shop'] ) ? '<strong>'.esc_html($shop['shop']).'</strong>': null; ?><?php echo esc_html($shop['phone']); ?>
															</a>
														<?php endif; ?>
													</li>
												<?php endforeach; 
											endforeach; ?>
										</ul>	
										</div>
									</div>
								</div>
							<?php endif; */ ?>
							<div class="text-center">
	            		<a href="" class="btn-yokomotor" data-toggle="modal" data-target="#modal-financiacion">PREGUNTA A UN ASESOR</a>
	            </div>
						</div>
					</section>
				<?php endforeach; ?>
			</div>
		<?php endif; 
	endif; ?>	
</div>
<?php 
	/**
		 * * ***************
		* @source ACF ARCHIVE PLUGIN
		* ACF Custom fields Financiación (Option page)
		* ***************
		* @param ACF_fields 'yokomotor_form'
		* 
		*/
		$formVehicles = get_field( 'yokomotor_form' ); 
	if( $formVehicles['choice_form'] && !empty($formVehicles['choice_form']) ): 
			$form = $formVehicles['choice_form'];?>
		<div id="modal-financiacion" class="modal animate__animated animate__fadeInDown modal-financiacion">
			<div class="flex-lightbox">
				<section class="form-yokomotor lightbox-form full clear-fix">	
					<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
					<hr>
					<h1>Preguntar por financiación</h1>
					<?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); ?>
				</section>
			</div>
		</div>
	<?php endif; ?>
<?php get_footer(); ?>