<?php get_header(); 

/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section', 'banner');
?>

<section class="main-home full clear-fix">
  <div class="wrapper-main center">
		<?php if( function_exists( 'get_field' ) ):
				/**
					 * * ***************
					* ACF Custom fields Página Inicio (page)
					* ***************
					* @param ACF_fields 'yokomotor_rand_articles'
					* 
					*/

					$randArticlesSection = get_field( 'yokomotor_rand_articles' );

			if(  $randArticlesSection && ($randArticlesSection['enable_section'] &&count($randArticlesSection['articles']) > 0) ): $articles = $randArticlesSection['articles'];?>
	    	<section class="main-information full clear-fix">
					<div class="row row-xs">
						<?php foreach( $articles as $article ): 
							$classColumn = (count($articles) > 2 ) ? 'col-12 col-sm-4 col-lg-4 col-xl-4' : 'col-12 col-sm-6 col-lg-6 col-xl-6'; ?>
							<div class="<?php echo $classColumn; ?>">
								<?php if( 'video' != $article['selected'] ): $url =!empty( $article['link'] ) ? $article['link'] : false; ?>
									<article class="card-information">
										<a href="<?php echo($url['url']) ? esc_url( $url['url']) : 'javascript:;'; ?>" target="<?php echo( $url['target'] ) ? esc_html( $url['target'] ) : '_self'; ?>">
											<figure>
												<img src="<?php echo ($article['image']['url']) ? esc_url($article['image']['url']): get_stylesheet_directory_uri(). '/library/images/image-destacados-default.jpg' ?>" alt="<?php echo esc_attr($article['image']['title']); ?>">
											</figure>
											<div class="overflow">
												<?php if( !empty($article['title']) ): ?>
													<h2><?php echo $article['title']; ?></h2>
												<?php endif; 
												if( !empty($article['description']) ): ?>
													<p><?php echo $article['description']; ?></p>
												<?php endif; ?>
												<div class="btn-yokomotor-arrow">LEER MÁS</div>
											</div>
										</a>
									</article>
								<?php else: ?>
									<section class="main-video-home">
										<article>
											<iframe src="https://www.youtube.com/embed/<?php echo $article['video']; ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
										</article>
									</section>
								<?php endif; ?>
							</div>
						<?php endforeach; ?>
					</div>
				</section>
			<?php endif;

			/**
			 * * ***************
			* ACF Custom fields Página Inicio (page)
			* ***************
			* @param ACF_fields 'yokomotor_campaings'
			* 
			*/

			$campaingSection = get_field( 'yokomotor_campaings' );

			if( $campaingSection && $campaingSection['enable_section'] ):
			
				/**	
				 * 
				 * Page parts sections
				 * @source 'page-parts/sections/' 'section-campaing.php'
				 * 
				 */
					get_template_part( 'page-parts/sections/section', 'campaing', array(
							'campaingSection' => $campaingSection
					) );
				
			endif;
		endif; ?>
			
<?php 	
	/**
		* * ***************
		* ACF Custom fields Página Inicio (page)
		* ***************
		* @param ACF_fields 'yokomotor_vehicles'
		* 
		*/
		$vehicleSection = get_field( 'yokomotor_vehicles' );

		if( $vehicleSection && $vehicleSection['enable_section'] ):
		
			/**	
			 * 
			 * Page parts sections
			 * @source 'page-parts/sections/' 'section-vehicles.php'
			 * 
			 */
				get_template_part( 'page-parts/sections/section', 'vehicles', array(
						'vehicleSection' => $vehicleSection
				) );
			
		endif; ?>
  </div>
</section>
<?php $blogs = get_current_blog_id(); 

if( $blogs == 3 ): switch_to_blog(3);
/**
		* * ***************
		* ACF Custom fields Página Inicio (page)
		* ***************
		* @param ACF_fields 'yokomotor_vehicles'
		* 
		*/
		$vehicleSection = get_field( 'yokomotor_vehicles' );

		if( $vehicleSection && $vehicleSection['enable_section'] ):
			$vehicleArticles = $vehicleSection['articles'];

?>
	<section class="modelos-home-hino full clear-fix mt-5">
		<div class="wrapper-main center">
			<h2>MODELOS</h2>
			<div class="main-car full clear-fix relative pt-2-xs">
				<div class="swiper swiper-car-full">
					<div class="swiper-wrapper">
						<?php foreach($vehicleArticles as $post): setup_postdata($post); 
						$price = get_field('price', get_the_ID());
						$image = get_field('thumbnail_feacture', get_the_ID());?>
							<div class="swiper-slide">
								<article class="card_car-details">
									<?php the_title('<h3>', '</h3>'); 
									if( $price && !empty($price) ): ?>
										<h4>$<?php echo $price; ?></h4>
									<?php endif; ?>
									<figure class="animation-car delay-0">
										<a href="<?php the_permalink(); ?>">
											<?php 
											if ( $image ): ?>
												<img src="<?php echo esc_url($image['url']); ?>" alt="blog <?php the_title(); ?>" />
												<?php	else: ?>
												<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/image-vehicle-default.jpg" alt="blog <?php the_title(); ?>" />
											<?php endif; ?>
										</a>
									</figure>
									<a href="<?php the_permalink(); ?>" class="btn-yokomotor-arrow">VER MÁS</a>
								</article>
							</div>
						<?php endforeach; wp_reset_postdata(); ?>
					</div>
				</div>
				<div class="next-car button-next next-black"></div>
					<div class="prev-car button-prev prev-black"></div>
				<div class="pagination-square bullet-gray pagination-car swiper-pagination"></div>
			</div>
		</div>
	</section>
<?php  endif; endif; restore_current_blog(); ?>
<!-- Hino connect -->
<?php 
  /**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/section-banner-hino', 'connect'
	 * 
	 */
	$choicePage = get_field( 'yokomotor_hino_connect' ); 
	get_template_part( 'page-parts/sections/section-banner-hino', 'connect', array('choicePage' => $choicePage)); ?>
<!-- <section class="main-video-home full clear-fix">
	<article>
		<h2>Video HISTORIA YOKOMOTOR</h2>
		<iframe src="https://www.youtube.com/embed/qJv4FUIm3os" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

	</article>
</section>
		 -->
	<?php if( function_exists( 'get_field' ) ):

			/**
			 * * ***************
			* ACF Custom fields Página Inicio (page)
			* ***************
			* @param ACF_fields 'yokomotor_blog'
			* 
			*/
		$blogSection = get_field( 'yokomotor_blog' );
		
		if( $blogSection && $blogSection['enable_section'] ):
			/**	
			 * 
			 * Page parts sections
			 * @source 'page-parts/sections/' 'section-blog.php'
			 * 
			 */
				get_template_part( 'page-parts/sections/section', 'blog', array(
						'blogSection' => $blogSection
				) );
		endif; 
	endif; ?>
<?php get_footer(); ?>