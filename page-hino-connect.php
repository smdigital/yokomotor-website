<?php get_header(); 

/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner');
?>

<section class="accesorios-detailed-shop full clear-fix">
	<div class="wrapper-main center">
    
	<?php 
  the_title('<h1>', '</h1>'); ?>
  <hr />
  <?php if( get_the_content()) {
		echo the_content(); 
	}; ?>
	 <hr />
	<?php	
  /*** * ***************
    * ACF Custom fields Página financing (page)
    * ***************
    * @param ACF_fields 'yokomotor_adviser'
    * 
    */

      //$adviserSection = get_field( 'yokomotor_adviser' );

    /*if( $adviserSection && ($adviserSection['enable_section'] &&count($adviserSection['contacs']) > 0) ): $advisers = $adviserSection['contacs']; ?>
      <div class="text-center-btn">
        <div class="btn-tooltip">
          <div class="btn-yokomotor icon-whatsapp"><?php echo $adviserSection['title']; ?></div>
          <ul class="tooltip-cities">
            <?php foreach( $advisers as $key => $adviser ): ?>
              <li data-city="city-<?php echo $key; ?>">
                <a href="javascript:;"><strong><?php echo $adviser['city']; ?> </strong></a>
              </li>
            <?php endforeach; ?>
          </ul>	
          <ul class="tooltip-shop" style="display: none;">
            <?php foreach( $advisers as $key => $adviser ): $shops = $adviser['shop']; 
              foreach( $shops as $shop ): ?>
                <li data-shop="city-<?php echo $key; ?>">
                  <?php if( $shop['phone'] && !empty($shop['phone']) ): ?>
                    <a href="https://api.whatsapp.com/send?phone=57<?php echo esc_html($shop['phone']); ?>&text=<?php echo( $adviserSection['whatsaap']) ? $adviserSection['whatsaap'] : 'Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor';?>" target="_blank">
                    <?php echo( $shop['shop'] ) ? '<strong>'.esc_html($shop['shop']).'</strong>': null; ?><?php echo esc_html($shop['phone']); ?>
                    </a>
                  <?php endif; ?>
                </li>
              <?php endforeach; 
            endforeach; ?>
          </ul>	
          </div>
        </div>
      </div>
    <?php endif; */?>

  <?php if( function_exists( 'get_field' ) ): 
			/**
			 * * * ***************
				* ACF Custom fields Hino connect (page)
				* ***************
				* @param ACF_fields 'yokomotor_form'
				* 
			*/
			$sectionConnet = get_field( 'yokomotor_form' );  
			if( $sectionConnet && $sectionConnet['enable_section'] ): ?>
				<section class="form-yokomotor full clear-fix">
				<?php if( $sectionConnet['choice_form'] && !empty($sectionConnet['choice_form']) ): 
				$form = $sectionConnet['choice_form'];?>
					<?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); 
				endif;?>
				</section>
			<?php endif; 
		endif; ?>
	</div>
</section>
<?php 
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/buttons/button-rate' 'us.php'
	 * 
	 */
	get_template_part( 'page-parts/buttons/button-rate', 'us'); ?>
<?php get_footer(); ?>