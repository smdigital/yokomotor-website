<?php get_header(); 
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner');
?>

<section class="main-cita-taller full clear-fix py-5">
	<div class="wrapper-main center">
		<h1><?php the_title(); ?></h1>
		<?php 
		/**
			 * * ***************
			* ACF Custom fields page Latonería (page)
			* ***************
			* @param ACF_fields 'yokomotor_details_cita'
				* @param ACF_subfields 'enable_section'
				* @param ACF_subfields 'details'
			* 
		*/
			$detailsSection = get_field('yokomotor_details_cita'); 

			if($detailsSection && $detailsSection['enable_section']):
				$cf = $detailsSection['cf-cita-taller'];
		?>
		<hr>
		<div class="details-cita">
			<?php echo $detailsSection['details'];?>
			<hr>
		<div class="">
							<input type="hidden" name="" class="serviceName" value="<?php echo esc_attr($title); ?>">
							<a href="" class="btn-yokomotor cta-service" data-toggle="modal" data-target="#modal-form-serivicio"><?php _e('Solicitar servicio', 'yokomotor'); ?></a>
						</div>
		<?php endif; ?>
	</div>
		</div>
		
</section>
<!-- Lightbox Solicitar servicio -->
<div id="modal-form-serivicio" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<section class="form-yokomotor lightbox-form full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<hr>
				<?php echo ($cf && !empty($cf)) ? do_shortcode($cf) : 'No hay formulari asignado';?>
		</section>	
	</div>
</div>

<?php
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/buttons/button-rate' 'us.php'
	 * 
	 */
	get_template_part( 'page-parts/buttons/button-rate', 'us');
?>

<?php get_footer(); ?>