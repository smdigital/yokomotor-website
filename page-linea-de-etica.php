<?php get_header(); 

if( has_post_thumbnail() ): ?>
	<section class="banner-general banner-etica full  clear-fix">
		<figure>
			<img src="<?php the_post_thumbnail_url('full'); ?>" alt="banner">
		</figure>
	</section>	
<?php endif; ?>
<section class="main-line-etica full clear-fix">
	<div class="wrapper-main-md center">
		<?php the_title('<h1>', '</h1>'); 
		if( get_the_content() ): ?>	
			<article>
				<?php the_content(); ?>
			</article>
		<?php endif; 
		if( function_exists( 'get_field' ) ): 
			/**
			 * * * ***************
				* ACF Custom fields Soat (page)
				* ***************
				* @param ACF_fields 'yokomotor_form'
				* 
			*/
			$sectionSoat = get_field( 'yokomotor_form' );  
			if( $sectionSoat && $sectionSoat['enable_section'] ): ?>
				<section class="form-yokomotor full clear-fix">
						<h2><?php echo $sectionSoat['title']; ?></h2>
				<?php if( $sectionSoat['choice_form'] && !empty($sectionSoat['choice_form']) ): 
				$form = $sectionSoat['choice_form'];?>
					<?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); 
				endif;?>
				</section>
			<?php endif; 
	

			/**
			 * * ***************
			* ACF Custom fields Accordion (Taxonomy)
			* ***************
			* @param ACF_fields 'seccion_accordion'
			* 
			*/
			$accordionSeccion = get_field( 'seccion_accordion' ); 

			if( $accordionSeccion['accordion'] && (isset($accordionSeccion['accordion'])) && count($accordionSeccion['accordion']) > 0 ): $accordions = $accordionSeccion['accordion']; ?>
				<section class="terms-etica full clear-fix">
					<div class="accordion-detalles">
						<?php foreach( $accordions as $accordion ): ?>
							<?php if( $accordion['title'] ): ?>
								<h3><?php echo $accordion['title']; ?></h3>
							<?php endif; 
							if( $accordion['description'] ): ?>		
								<div class="terms-accordion">
									<?php echo $accordion['description']; ?>
								</div>
							<?php endif; ?>
						<?php endforeach; ?>
					</div>	
				</section>
			<?php endif; 	
		endif; ?>
	</div>
</section>
<?php get_footer(); ?>