<?php get_header(); 
if( has_post_thumbnail() ):?>
	<section class="banner-general full  clear-fix">
		<figure>
			<img src="<?php the_post_thumbnail_url('full'); ?>" alt="banner">
		</figure>
	</section>
<?php endif; ?>

<section class="main-paga-factura full clear-fix">
	<div class="wrapper-main center">
		<?php the_title('<h1>', '</h1>'); 
		if( get_the_content() ): ?>
			<article  class="wrapper-main-sm center">
				<?php the_content(); ?>
			</article>
		<?php endif; ?>
		<!-- <div class="main-form-fieldset center">
            <div class="main-col-paga-factura">
				<fieldset>
	                <legend>NIT O CC</legend>
	                <input type="text" id="" name="">
	            </fieldset>
            </div>
            <div class="main-col-paga-factura-submit">
                <input type="submit" value="CONSULTAR">
            </div>
        </div> -->
        <iframe src="https://yokomotor.com.co/paga-tus-facturas/" id="iframe-facturas"  title="Paga facturas yokomotor"></iframe>
	</div>
</section>



<?php get_footer(); ?>