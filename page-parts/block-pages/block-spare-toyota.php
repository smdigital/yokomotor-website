<?php 

$accesoryNameTerms = 'accessory_cats';
 $accesoryTerms = get_terms( array (
	'taxonomy' 	 => $accesoryNameTerms,
	'hide_empty' => false, 
	'orderby' 	 => 'DESC',
	'fields' 		 => 'all',
) );

if( $accesoryTerms && (isset($accesoryTerms) && count($accesoryTerms) > 0) ): ?>
	<section class="main-car-accesorios full clear-fix">
		<div class="wrapper-main center">
			<div class="title-thums relative">
				<div thumbsSlider="" class="swiper swiper-thums-accesorio">
					<div class="swiper-wrapper">
						<?php foreach( $accesoryTerms as $accesoryTerm ): 
							if( $accesoryTerm->parent == 0 ): ?>
								<div class="swiper-slide"><h4><?php echo $accesoryTerm->name; ?></h4></div>
							<?php endif; 
						endforeach; ?>
					</div>
				</div>
					<div class="button-next next-red next-accesorio">next</div>
					<div class="button-prev prev-red prev-accesorio">prev</div>
			</div>	
				<div class="swiper swiper-accesorio">
					<div class="swiper-wrapper">
					<?php 
						foreach( $accesoryTerms as $category ): 
							if( $category->parent == 0 ): ?>
								<div class="swiper-slide">
									<div class="row row-xs">
										<?php foreach( $accesoryTerms as $subcategory ): 
											if($subcategory->parent == $category->term_id): 										
											$thumbnail = get_field('yokomotor_thumbnail', $subcategory); ?>
												<div class="col-6 col-sm-3 col-lg-3 col-xl-3">
													<article class="card-accesorios">
														<figure>
															<a href="<?php echo get_term_link( $subcategory ); ?>">
																<img src="<?php echo esc_url($thumbnail['image']['url']); ?>" alt=""> 
															</a>
														</figure>
														<h6><?php echo $subcategory->name;?></h6>
													</article>
												</div>
											<?php endif; 
										endforeach; ?>
									</div>
								</div>
							<?php endif;
						endforeach; ?>
				</div>
			</div>
		</div>
	</section>
<?php endif;?>