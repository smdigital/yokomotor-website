<?php 

/**
 * 
 * Page Part Template 'section-desktop_menu.php
 * @since 1.0.0
 * 
 */

$menu_name = 'main-nav';

if ( has_nav_menu( $menu_name ) ):

	$locations = get_nav_menu_locations();
	$menu 		 = wp_get_nav_menu_object( $locations[ $menu_name ] );

	/**
	 * 
	 * Get Menu Items
	 * @param 'menu-principal'
	 * 
	 */
	$menu_items = wp_get_nav_menu_items( $menu );

	/**
	 * 
	 * Build Category Tree
	 * 
	 */
	$items = yokomotor_build_category_tree( $menu_items ); 
	
	$megaMenuOn = true;
	if( $items && is_array( $items ) && count( $items ) > 0 ): 
		foreach ( $items as $parent ): 
			$sliderSections = get_field('yokomotor_menu', $parent->ID); 
			
			if( $sliderSections['choice_menu'] != 'normal_menu'): ?>
				<section class="max_mega_menu-container item-max-megamenu-<?php echo $parent->ID; ?>" style="display: none;">
					<div class="wrap-mega-menu">
						<div class="wrapper-main center">
							<?php	//foreach ( $items as $parent ): ?>
								<?php if(is_array($parent->childrens) || is_object($parent->childrens)): ?>
									<div class="main-tabs-car tabs-car-menu" data-menu-parent="<?php echo $parent->ID; ?>">
										<ul class="titles-nav-car">
											<?php foreach($parent->childrens as $secondChild): ?>
												<li data-menu-parent="<?php echo $secondChild->menu_item_parent; ?>" class="<?php echo $secondChild->post_name; ?>"><a href="#<?php echo $secondChild->post_name; ?>"><?php echo $secondChild->title; ?></a></li> 
											<?php endforeach; ?>
										</ul>
										<?php foreach($parent->childrens as $secondChild): $sliderSections = get_field('yokomotor_menu', $secondChild->ID); ?>
											<div id="<?php echo $secondChild->post_name; ?>" class="conten-car-menu">
												<?php if( $sliderSections['enable_section'] && ($sliderSections['slider'] && count($sliderSections['slider']) > 0) ): ?>
													<div class="swiper swiper-nav-car">
														<div class="swiper-wrapper">
															<?php foreach( $sliderSections['slider'] as $slider): 
																$url =!empty( $slider['link'] ) ? $slider['link'] : false; ?>
																<div class="swiper-slide">
																	<article class="car-nav-yokomotor">
																		<a href="<?php echo($url) ? esc_url(get_term_link( $url )) : 'javascript:;'; ?>">
																			<?php if( $slider['image']['url']): ?>
																				<figure>
																					<img src="<?php echo esc_url( $slider['image']['url']); ?>" alt="">
																				</figure>
																			<?php endif; 
																			if( !empty($slider['title']) ):?>
																				<figcaption><?php echo $slider['title']; ?></figcaption>
																			<?php endif; ?>
																		</a>
																	</article>
																</div>
															<?php endforeach; ?>
														</div>
													</div>
												<?php endif; ?>
												<div class="pagination-square nav-pagination swiper-pagination"></div>
											</div>
											<?php //Third nivel 
											/* if (is_array($secondChild->childrens) || is_object($secondChild->childrens)):
												foreach($secondChild->childrens as $thirdChild):var_dump($thirdChild->title);?>
												<?php endforeach;
											endif;  */?>
										<?php endforeach; ?>
									</div>
								<?php endif; ?>
							<?php //endforeach; ?>
						</div>
					</div>
				</section>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endif; ?>
<?php endif; ?>