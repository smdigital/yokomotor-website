<?php 

/**
 * 
 * Page Part Template 'section-desktop_menu.php
 * @since 1.0.0
 * 
 */

$menu_name = 'main-nav';

if ( has_nav_menu( $menu_name ) ):

	$locations = get_nav_menu_locations();
	$menu 		 = wp_get_nav_menu_object( $locations[ $menu_name ] );

	/**
	 * 
	 * Get Menu Items
	 * @param 'menu-principal'
	 * 
	 */
	$menu_items = wp_get_nav_menu_items( $menu );

	/**
	 * 
	 * Build Category Tree
	 * 
	 */
	$items = yokomotor_build_category_tree( $menu_items ); 

	if( $items && is_array( $items ) && count( $items ) > 0 ): ?>

		<section class="nav-mobile-yokomotor">
			<div class="demo-1">	
				<div class="main clearfix">				
					<div class="column">
						<div id="dl-menu" class="dl-menuwrapper">
							<button class="dl-trigger">Open Menu</button>
							<ul class="dl-menu">
								<?php	foreach ( $items as $parent ): ?>
									<li>
										<a href="<?php echo $parent->url; ?>"><?php echo $parent->title; ?></a>
										<?php if (is_array($parent->childrens) || is_object($parent->childrens)): ?>
											<ul class="dl-submenu ">
												<?php foreach($parent->childrens as $secondChild): ?>
													<li><a href="<?php echo $secondChild->url; ?>"><?php echo $secondChild->title; ?></a>
														<?php 
														if(is_array($secondChild->childrens)):?>
														<ul class="dl-submenu">
															<?php foreach ($secondChild->childrens as $thirdChild):?>
																<li class="<?php foreach($thirdChild->classes as $classChild){ echo $classChild.' ';}?>"><a href="<?php echo $thirdChild->url; ?>"><?php echo $thirdChild->title; ?></a>
															<?php endforeach; ?>
														</ul>
														<?php 
														endif;
														?>
													</li>
												<?php endforeach; ?>
											</ul>
										<?php endif; ?>
									</li>
								<?php endforeach; ?>
							</ul>
						</div><!-- /dl-menuwrapper -->
					</div>
				</div>
			</div>
			<div class="conten-xs-redes">
				<ul>
					<li class="test-dl-submenu"><a href="https://www.testdrive.com.co/yk-home-web" target="_blank">Prueba de ruta</a></li>
					<li class="redes-dl-submenu">
						<div class="redes">
							<span>Redes sociales</span>
							<ul>
								<li class="icon-fb"><a href="https://www.facebook.com/YokomotorColombia/" target="_blank">facebook</a></li>
								<li class="icon-tw"><a href="https://twitter.com/Yokomotor" target="_blank">twitter</a></li>
								<li class="icon-it"><a href="https://www.instagram.com/yokomotor/?hl=es-la" target="_blank">instagram</a></li>
								<li class="icon-yt"><a href="https://www.youtube.com/user/YokomotorColombia" target="_blank">youtube</a></li>
							</ul>
						</div>
					</li>
						<?php $blogs = get_current_blog_id(); 
							switch ($blogs): 
							case '2': switch_to_blog(2); ?>
							<li class="contacto-dl-submenu"><a href="https://stg-yokomotor.smdigitalstage.com/toyota/sedes/">Contacto</a></li>

							<?php restore_current_blog(); break;?>
							<?php case '3': switch_to_blog(3); ?>
							<li class="contacto-dl-submenu">
								<a href="https://api.whatsapp.com/send?phone=573187755174&text=Hola,%20quiero%20comunicarme%20con%20Comercial%20Yokomotor" target="_blank">Contacto</a></li>
							<?php restore_current_blog(); break;?>
							<?php default:  restore_current_blog(); break?>
						<?php endswitch; ?>
					
				</ul>
			</div>	
		</section>	
		<?php endif; ?>
		<?php endif; ?>