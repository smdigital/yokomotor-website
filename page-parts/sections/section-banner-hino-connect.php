<!-- Hino connect -->

<?php 
	/**
	 * * ***************
	* @source ACF ARCHIVE PLUGIN
	* ACF Custom fields Vehicles (Option page)
	* ***************
	* @param ACF_fields 'yokomotor_hino_connect'
	* 
	*/
  $choicePage = (isset( $args, $args['choicePage'])) ? $args['choicePage'] : false; 

if( $choicePage['choice_page'] && !empty($choicePage['choice_page']) ): 
	$page = $choicePage['choice_page'];
		
	$banner = get_field('yokomotor_banner', $page->ID)?>
	<section class="hino-connect full clear-fix">
		<div class="wrapper-main center">
			<div class="bg-hino-connect full clear-fix" style="background: #000 url(<?php echo $banner['image']['url']; ?>) no-repeat center center;">			
				<article>
					<h2><?php echo $page->post_title; ?></h2>
					<?php if( !empty($page->post_content) ) {
						echo $page->post_content;
					}; ?>
					<a href="<?php the_permalink($page->ID); ?>" class="btn-yokomotor-arrow">VER MÁS</a>
				</article>
			</div>
		</div>	
	</section>
<?php endif; ?>