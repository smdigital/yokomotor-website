<?php 
$detect = new Mobile_Detect;
$context = get_queried_object_id();
if ($context) :

/**
	 * * ***************
	* ACF Custom fields Página Inicio (page)
	* ***************
	* @param ACF_fields 'yokomotor_banner'
	* 
*/

$banners = get_field('yokomotor_banner', $context);

$bannerInfo = get_field('yokomotor_banner_info', $banners->ID);
// $bannerStyle = ($args['style'] && isset($args['style']) ) ? $args['style'] : null;
$classBanner = strtolower(str_replace(' ', '-', get_the_title($banners->ID)));
if (!empty($bannerInfo) && count($bannerInfo) > 0) :
?>

	<section class="banner-home <?php echo $classBanner; ?> clear-fix">
		<div class="swiper swiper-banner-home">
			<div class="swiper-wrapper">
				<?php 
          			 foreach ($bannerInfo as $banner) :
						/**
						 * 
						 * Banner Params
						 *  @param ACF_Sub_Field    'image'
						 *  @param ACF_Sub_Field    'imageXs'
						 *  @param ACF_Sub_Field    'title_btn_cta1'
						 *  @param ACF_Sub_Field    'url_cta1'
						 *  @param ACF_Sub_Field    'title_btn_cta2'
						 *  @param ACF_Sub_Field    'url_cta2'
						 */

						$image = isset( $banner['image'] ) && !empty( $banner['image'] ) ? $banner['image'] : false;
						$imageXs = isset( $banner['imageXs'] ) && !empty( $banner['imageXs'] ) ? $banner['imageXs'] : false;
						$url_general = isset( $banner['url_general'] ) && !empty( $banner['url_general'] ) ? $banner['url_general'] : false;
						$btn_cta1 = isset( $banner['title_btn_cta1'] ) && !empty( $banner['title_btn_cta1'] ) ? $banner['title_btn_cta1'] : false;
						$url_cta1 = isset( $banner['url_cta1'] ) && !empty( $banner['url_cta1'] ) ? $banner['url_cta1'] : false;
						$btn_cta2 = isset( $banner['title_btn_cta2'] ) && !empty( $banner['title_btn_cta2'] ) ? $banner['title_btn_cta2'] : false;
						$url_cta2 = isset( $banner['url_cta2'] ) && !empty( $banner['url_cta2'] ) ? $banner['url_cta2'] : false;

				?>
					<?php if ($image && isset($image['url'])) : ?>
					<div class="swiper-slide">
						<figure>
							<a href="<?php echo !empty($url_general) ? $url_general : '#'; ?>"><img src="<?php echo ($detect->isMobile() && !$detect->isTablet()) ? esc_url($imageXs['url']) : esc_url($image['url']); ?>" alt="<?php echo ($detect->isMobile() && !$detect->isTablet()) ? esc_attr($imageXs['title']) : esc_attr($image['title']);  ?>"></a>
						</figure>
						<?php if( !empty($url_cta1) || !empty($btn_cta1) || !empty($url_cta2) || !empty($btn_cta2)): ?>
 							<div class="wrap-banner">
 								<article class="animation-caption delay-1">
 								<?php if( !empty($btn_cta1) && !empty($url_cta1) ): ?>
 										<a href="<?php echo $url_cta1; ?>" class="btn-yokomotor-arrow" target="_blank"><?php echo $btn_cta1; ?></a>
 								<?php endif; ?>
								 <?php if( !empty($btn_cta2) && !empty($url_cta2) ): ?>
 										<a href="<?php echo $url_cta2; ?>" class="btn-yokomotor-arrow" target="_blank"><?php echo $btn_cta2; ?></a>
 								<?php endif; ?>
 								</article>
 							</div>
 						<?php endif; ?>
					</div>
					<?php endif; ?>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="next-banner button-next next-white"></div>
				<div class="prev-banner button-prev prev-white"></div>
		<div class="swiper-pagination banner-pagination"></div>
	</section>

	<?php endif; ?>
<?php endif; ?>