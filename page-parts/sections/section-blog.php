
<?php 
/**
 * 
 * Page Part Template 'sections-blog' php
 * @see front-page.php
 * @since 1.0.0
 * @requires Boostrap
 * 
 */
$postType = 'yokomotor_blog';

if( post_type_exists( $postType ) && function_exists( 'get_field' ) ) :

  $blogSection = (isset( $args, $args['blogSection'])) ? $args['blogSection'] : false; 

  $args = [
    'post_type'      => $postType,
    'posts_per_page' => 3,
    'order'          => 'DESC',
    'orderby'        => 'DESC',
    'post_status'    => array('publish')
  ];
  
  $blogArticles = get_posts( $args );
  if( count($blogArticles) > 0): $aosDelay = 300; ?>

		<section class="main-blog-home full clear-fix pt-3-xs">
			<div class="wrapper-main center">
				<?php if( !empty($blogSection['title']) ): ?>
					<h2><?php echo $blogSection['title']; ?></h2>
				<?php endif; ?>
				<div class="row row-xs">
					<?php foreach ( $blogArticles as $post ): setup_postdata( $post );  ?>
						<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
							<article class="card-blog" data-aos="fade-up" data-aos-duration="600" data-aos-delay="<?php echo $aosDelay; ?>">
								<figure>
									<a href="<?php the_permalink();?>">
										<?php 
											if ( has_post_thumbnail() ):
												the_post_thumbnail( 'yokomotor-thumb-347',[ 'alt' => 'blog '.get_the_title() ] );
											else: ?>
												<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/image-blog-default.jpg" alt="blog <?php the_title(); ?>" />
										<?php endif; ?>
									</a>
								</figure>
								<div class="details-blog">
									
								<?php	
								$taxterms = wp_get_post_terms( get_the_ID(), 'blog_cats', array('hide_empty' => false, 'fields' => 'names' ));
								$index = 1;
									if( $taxterms && !empty($taxterms) ): ?>
									<h4>
										<?php foreach ( $taxterms as $key => $taxterm ): ?>
											<?php echo $taxterm; echo ( $index < count($taxterms) )? ", " : " "; ?>
										<?php $index ++; endforeach; ?> 
									</h4>
								<?php endif; ?>
									<?php the_title('<h3>', '</h3>'); 
									if( has_excerpt() ): ?>
										<p><?php the_excerpt(); ?></p>					
									<?php endif; ?>
									<a href="<?php the_permalink();?>" class="btn-yokomotor-light">LEER MÁS</a>
								</div>
							</article>
					</div>
					<?php $aosDelay += 200; endforeach; wp_reset_postdata(); ?>
				</div>
			<a href="<?php echo site_url('/blog/', 'https'); ?>" class="btn-yokomotor">VER BLOG</a>
			</div>
		</section>

	<?php endif; 
endif; ?>