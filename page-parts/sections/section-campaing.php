
<?php 
/**
 * 
 * Page Part Template 'sections-campaing' php
 * @see front-page.php
 * @since 1.0.0
 * @requires Boostrap
 * 
 */
$postType = 'yokomotor_campaigns';

if( post_type_exists( $postType ) && function_exists( 'get_field' ) ) :

  $campaingSection = (isset( $args, $args['campaingSection'])) ? $args['campaingSection'] : false; 
/* 
  $args = [
    'post_type'      => $postType,
    'posts_per_page' => 3,
    'order'          => 'ASC',
    'orderby'        => 'DESC',
    'post_status'    => array('publish')
  ];
   */
  $campaingArticles = $campaingSection['articles'];
	
  if( $campaingArticles && count($campaingArticles) > 0): ?>
	
		<section class="main-campaign full clear-fix pt-5 pt-2-xs">
			<?php if( !empty($campaingSection['title']) ): ?>
					<h2><?php echo $campaingSection['title']; ?></h2>
			<?php endif; ?>
			<div class="row row-xs">
				<?php foreach($campaingArticles as $post): setup_postdata($post); ?>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-information campaign-small">
							<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
								<figure>
								<?php 
									if ( has_post_thumbnail() ):
										the_post_thumbnail( 'yokomotor-thumb-347',[ 'alt' => 'campaña '.get_the_title() ] );
									else: ?>
										<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/image-campaing-default.jpg" alt="blog <?php the_title(); ?>" />
								<?php endif; ?>
								</figure>
								<div class="overflow">
								<?php the_title('<h3>', '</h3>'); 
									if( get_the_excerpt() ) echo '<p>'.the_excerpt().'</p>'; ?>
									<div class="btn-yokomotor-arrow">CTA</div>
								</div>
							</a>
						</article>
					</div>
				<?php endforeach; wp_reset_postdata(); ?>
			</div>				
		</section>
	<?php endif; 
endif; ?>