<section class="main-categoria-usados full clear-fix">
	<div class="wrapper-main center">
		<h2>NUESTRAS CATEGORÍAS</h2>	
		<div class="row-swiper-usados relative center">
			<div class="swiper swiper-categoria-usados">
				<div class="swiper-wrapper">
				<?php 
					/**
					 * * ***************
					* ACF Custom fields Usados (Taxonomias)
					* ***************
					* @param ACF_fields 'icon'
					* 
					*/
					$used_tax = 'classification_cats';
					$arg = array(
						'taxonomy' => $used_tax,
						'hide_empty' => false
					);
					$usedTaxonomies = get_terms($arg);
					foreach($usedTaxonomies as $taxonomy):
						if($taxonomy->parent == 0):
							$context = $used_tax.'_'.$taxonomy->term_taxonomy_id;
							$icono = get_field('icon', $context);						
				?>
					<div class="swiper-slide">
						<div class="icon-categoria-usados">
							<a href="<?php echo get_term_link($taxonomy); ?>" >
								<i>
									<img src="<?php echo esc_url($icono['url']); ?>" alt="<?php echo esc_attr($icono['title']); ?>">
								</i>
								<h6><?php echo $taxonomy->name; ?></h6>
							</a>
						</div>
					</div>
				<?php 
						endif;
					endforeach; 
				?>
				</div>
			</div>
			<div class="swiper-pagination pagination-bullet pagination-categoria-usados"></div>
		</div>
	</div>
</section>