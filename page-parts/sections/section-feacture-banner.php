<?php 

/**
 * 
 * Library Mobile Detect
 * @source 'library/includes' 'Mobile_Detect.php'
 * */
$detect = new Mobile_Detect;

$class = (isset( $args, $args['class'])) ? $args['class'] : false; 

if( has_post_thumbnail() || get_field('banner_mobile') ): ?>
	<section class="banner-general banner-bottom full  clear-fix <?php echo $class; ?>">
		<figure>
			<?php if( $detect->isMobile() && !$detect->isTablet() ): $img = get_field('banner_mobile'); ?>
				<a href="<?php the_field('link_accion'); ?>"><img src="<?php echo($img) ? $img['url']: the_post_thumbnail_url('full'); ?>" alt="banner"></a>
			<?php else: ?>
				<a href="<?php the_field('link_accion'); ?>"><img src="<?php the_post_thumbnail_url('full'); ?>" alt="banner"></a>
			<?php endif; ?>			
		</figure>
	</section>	
<?php endif; ?>