
<?php 
/**
 * 
 * Page Part Template 'sections-campaing' php
 * @see front-page.php
 * @since 1.0.0
 * @requires Boostrap
 * 
 */
$postType = 'yokomotor_vehicles';

if( post_type_exists( $postType ) && function_exists( 'get_field' ) ) :

  $vehicleSection = (isset( $args, $args['vehicleSection'])) ? $args['vehicleSection'] : false; 
/* 
  $args = [
    'post_type'      => $postType,
    'posts_per_page' => 3,
    'order'          => 'ASC',
    'orderby'        => 'DESC',
    'post_status'    => array('publish')
  ];
   */
  $vehicleArticles = $vehicleSection['articles'];
	
  if( $vehicleArticles && count($vehicleArticles) > 0): 
		//$classBody = str_replace(' ', '', get_bloginfo());
		//var_dump($classBody);	
		$blogs = get_current_blog_id();
		/* if( $blogs = 2 ) {
			switch_to_blog(2);
		} */
		switch ($blogs): 
			case '2': switch_to_blog(2);?>
				<section class="main-car full clear-fix relative pt-5 pt-2-xs">
					<div class="wrapper-main center">
						<div class="swiper swiper-car-full">
							<div class="swiper-wrapper">
								<?php foreach($vehicleArticles as $post): setup_postdata($post); 
									$image = get_field('thumbnail_feacture', get_the_ID());?>
									<div class="swiper-slide">
										<article class="card_car-details">
											<figure class="animation-car delay-0">
												<a href="<?php the_permalink(); ?>">
													<?php 
													if ( $image ): ?>
														<img src="<?php echo esc_url($image['url']); ?>" alt="blog <?php the_title(); ?>" />
													<?php	else: ?>
														<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/image-vehicle-default.jpg" alt="blog <?php the_title(); ?>" />
													<?php endif; ?>
												</a>
											</figure>
											<?php the_title('<h2>', '</h2>'); ?>
											<a href="<?php the_permalink(); ?>" class="btn-yokomotor-arrow">VER MÁS</a>
											<?php if( has_excerpt() ) echo '<p>'.the_excerpt().'</p>';?>
										</article>
									</div>
								<?php endforeach; wp_reset_postdata(); ?>
							</div>
						</div>
						<div class="next-car button-next next-black"></div>
						<div class="prev-car button-prev prev-black"></div>
						<div class="pagination-square pagination-car swiper-pagination"></div>
					</div>
				</section>
				<?php restore_current_blog(); break;?>
			<?php /*case '3': switch_to_blog(3)?>
				<section class="modelos-home-hino full clear-fix mt-5">
					<div class="wrapper-main center">
						<h2>MODELOS</h2>
						<div class="main-car full clear-fix relative pt-2-xs">
							<div class="swiper swiper-car-full">
								<div class="swiper-wrapper">
									<?php foreach($vehicleArticles as $post): setup_postdata($post); 
									$details = get_field('yokomotor_color', get_the_ID());?>
										<div class="swiper-slide">
											<article class="card_car-details">
												<?php the_title('<h3>', '</h3>'); 
												if( $details['price'] && !empty($details['price']) ): ?>
													<h4><?php echo $details['price']; ?></h4>
												<?php endif; ?>
												<figure class="animation-car delay-0">
												<?php 
													if ( has_post_thumbnail() ):
														the_post_thumbnail( 'full',[ 'alt' => 'campaña '.get_the_title() ] );
													else: ?>
														<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/image-vehicle-default.jpg" alt="blog <?php the_title(); ?>" />
												<?php endif; ?>
												</figure>
												<a href="<?php the_permalink(); ?>" class="btn-yokomotor-arrow">VER MÁS</a>
											</article>
										</div>
									<?php endforeach; wp_reset_postdata(); ?>

									<!-- 	<div class="swiper-slide">
										<article class="card_car-details">
											<h3>LOREM DOLOR AMEDD IMSUM </h3>
											<h4>$xxx.xxx.xxx</h4>
											<figure class="animation-car delay-0">
												<a href="">
													<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/hino/hino-1.png" alt="">
												</a>
											</figure>
											<a href="" class="btn-yokomotor-arrow">VER MÁS</a>
										</article>
									</div>
									<div class="swiper-slide">
										<article class="card_car-details">
											<h3>LOREM DOLOR AMEDD IMSUM </h3>
											<h4>$xxx.xxx.xxx</h4>
											<figure class="animation-car delay-0">
												<a href="">
													<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/hino/hino-1.png" alt="">
												</a>
											</figure>
											<a href="" class="btn-yokomotor-arrow">VER MÁS</a>
										</article>
									</div>					 -->
								</div>
							</div>
							<div class="next-car button-next next-black"></div>
								<div class="prev-car button-prev prev-black"></div>
							<div class="pagination-square bullet-gray pagination-car swiper-pagination"></div>
						</div>
					</div>
				</section> -->
				<?php restore_current_blog(); break; */?>
			<?php default:  restore_current_blog(); break?>
		<?php endswitch; ?>
	<?php endif; 
endif; ?>

