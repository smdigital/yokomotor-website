<?php 
	the_post();
	$termArray = [];
  $customType = 'yokomotor_vehicles';
	$taxonomyName = 'vehicles_cats';
  $id = ($args['postID'] && isset($args['postID']) ) ? $args['postID'] : false;
  $categoryID = ($args['categoryID'] && isset($args['categoryID']) ) ? $args['categoryID'] : false;

  if($id) {
    $termlists = wp_get_post_terms($id, $taxonomyName, array('fields' => 'all') );
	
    foreach ($termlists as $key => $termlist) {
      if( $termlist->parent != 0 ) {
        array_push($termArray, $termlist->term_id );
      }
    }
  }else{
    array_push($termArray, $categoryID );
  }


/**
   * Get Related Models
   */
    
    $args = [
      'posts_per_page'   => -1,
      'post_type'        => $customType,
      'suppress_filters' => false,
      'post_status'      => 'publish',
      'order'            => 'ASC',
      'post__not_in' 		 => [$id],
      'tax_query' 			 => [
        'relation' 			 => 'AND',
            [
              'taxonomy' => $taxonomyName,
              'field'    => 'id',
              'terms'    => $termArray
            ]
      ],
    ];

    $relateModels =  get_posts( $args );
if( $relateModels && (isset($relateModels) && count($relateModels) > 0) ): ?>
  <section class="main-modelos-carrousel full clear-fix relative">
    <div class="wrapper-main center">
      <h2>Selecciona un modelo</h2>
      <div class="col-swiper-modelos relative">
        <div class="swiper swiper-modelos-small">
          <div class="swiper-wrapper">
            <?php foreach ( $relateModels as $post ) : setup_postdata( $post ); 
            $imgThumbail = get_field('thumbnail_img', get_the_ID()); ?>
              <div class="swiper-slide">
                <article class="card-modelo-small">
                  <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                    <figure>
                      <img src="<?php echo esc_url( $imgThumbail['url']); ?>" alt="<?php echo esc_attr( $imgThumbail['title']); ?>"> 
                    </figure>
                    <hr>
                    <h3><?php the_title();?></h3>
                  </a>
                </article>
              </div>
            <?php endforeach;  wp_reset_postdata();?>
          </div>
        </div>
            <div class="prev-modelo button-prev prev-white"></div>
        <div class="swiper-pagination modelo-pagination"></div>
        <div class="next-modelo button-next next-white"></div>
      </div>
    </div>
  </section>
<?php endif; ?>