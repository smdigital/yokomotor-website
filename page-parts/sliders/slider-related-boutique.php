<?php 
global $post;
$relateBoutiques = $args;
if( $relateBoutiques && count($relateBoutiques) > 0 ): ?>
    
		<div class="swiper swiper-clothes">
				<div class="swiper-wrapper">
				<?php foreach ( $relateBoutiques as $post ): 
					setup_postdata( $post ); 
					/**
					 * * ***************
						* ACF Custom fields Boutique (post Type)
						* ***************
						* @param ACF_fields 'yokomotor_details'
						* 
						*/
						$detailsSection = get_field('yokomotor_details', get_the_ID());
				?>
						<div class="swiper-slide">
							<article class="card-producto">
								<a href="<?php the_permalink(); ?>">
									<figure>
									<?php 
									if ( has_post_thumbnail() ) { 
										the_post_thumbnail( 'full',[ 'alt' => get_the_title() ] ); 
									}
								?>
									</figure>
								<?php	if( $detailsSection['price'] && !empty($detailsSection['price']) ): ?>
									<h6>$<?php echo $detailsSection['price']; ?></h6>
								<?php endif; ?>
									<div class="figcaption">
									<?php the_title('<p>', '</p>'); ?>
									</div>
								</a>
							</article>
						</div>
					<?php endforeach; wp_reset_postdata();?>
<?php endif; ?>