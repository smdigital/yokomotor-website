<?php get_header(); 
/**
 * Template name: Template paginas sencillas
 */

do_action( 'yokomotor_get_back_button' ); ?>

<section class="main-templete-content full clear-fix">
	<div class="wrapper-main center">
		<h1><?php the_title('<h1>', '</h1>'); ?></h1>
		<?php if( get_the_content() ) {
			the_content();
		}?>
	</div>
	<?php if( function_exists( 'get_field' ) ): 
				/**
				 * * * ***************
					* ACF Custom fields treatment policy (page)
					* ***************
					* @param ACF_fields 'yokomotor_download'
					* 
				*/
				$sectionDownload = get_field( 'yokomotor_download' );  
				if( $sectionDownload && ($sectionDownload['enable_section'] &&count($sectionDownload['download']) > 0) ): $downloads = $sectionDownload['download']; ?>
					<div class="text-center pt-4">
						<?php foreach( $downloads as $download ): ?>
							<a href="<?php echo($download['link']['url']) ? esc_url( $download['link']['url']) : 'javascript:;'; ?>" class="download-icon" download><?php echo $download['title']; ?></a>
							</div>
						<?php endforeach; ?>	
					</div>	
				<?php endif;
			endif; ?>
</section>

<?php get_footer(); ?>