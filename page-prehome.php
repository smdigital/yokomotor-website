<?php get_header('prehome'); 

if( function_exists( 'get_field' ) ):
	/**
		 * * ***************
		* ACF Custom fields Página PreHome (page)
		* ***************
		* @param ACF_fields 'yokomotor_prehome'
		* 
		*/

		$prehomeSection = get_field( 'yokomotor_prehome' );

	if( $prehomeSection && count($prehomeSection['brands']) > 0 ): $brands = $prehomeSection['brands']; ?>

		<section class="main-prehome disabled">
			<?php foreach( $brands as $brand ): 
				$imageBack = $brand['image_back'];
				$logo 		 = $brand['logo']; 
				$url =!empty( $brand['link'] ) ? $brand['link'] : false?>
				<article class="content-prehome prehome-toyota <?php echo($url['url'] == '#') ? 'prehome-none' : null; ?>" style="background: url(<?php echo esc_url($imageBack['url']); ?> )no-repeat center center; background-size: cover;">
					<a href="<?php echo($url['url']) ? esc_url( $url['url']) : 'javascript:;'; ?>" target="<?php echo( $url['target'] ) ? esc_html( $url['target'] ) : '_self'; ?>">
						<div class="content-details">
							<?php if( $brand['logo'] ): ?>
								<figure class="animate__animated animate__fadeInDown animate__delay-1s">
									<img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['title']); ?>">
								</figure>
							<?php endif; ?>
							<div class="clr"></div>
							<div class="btn-ir animate__animated animate__fadeInUp animate__delay-1s"><?php echo $url['title']; ?></div>
						</div>
					</a>
					<div class="shadow-clip-path"></div>
				</article>
			<?php endforeach; ?>
				<!-- <article class="content-prehome prehome-toyota">
					<a href="<?php echo site_url( '/toyota/','https');?>">
						<div class="content-details">
							<figure class="animate__animated animate__fadeInDown animate__delay-1s">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-yokomotor-toyora.png" alt="">
							</figure>
							<div class="clr"></div>
							<div class="btn-ir animate__animated animate__fadeInUp animate__delay-1s">Ir</div>
						</div>
					</a>
					<div class="shadow-clip-path"></div>
				</article>
				<article class="content-prehome prehome-hino">
					<a href="<?php echo site_url( '/hino/','https');?>">
						<div class="content-details">
							<figure class="animate__animated animate__fadeInDown animate__delay-1s">
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/logo-yokomotor-hino.png" alt="">
							</figure>
							<div class="clr"></div>
							<div class="btn-ir animate__animated animate__fadeInUp animate__delay-1s">Ir</div>
						</div>			
					</a>
					<div class="shadow-clip-path"></div>
				</article>
			 -->
		</section>			
	<?php endif; 
endif; ?>

<?php get_footer('prehome'); ?>