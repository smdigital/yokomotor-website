<?php get_header(); 
if( get_the_content() || get_the_post_thumbnail_url() ): ?>
	<section class="banner-responsabilidad full clear-fix" style="background: #000 url(<?php the_post_thumbnail_url( 'full' );?>)no-repeat center center; background-size: cover;">
		<article>
			<?php if( get_the_content() ) {
				the_content();
			}; ?>	
		</article>
	</section>
<?php endif; 

if( function_exists( 'get_field' ) ): 
	/**
	 * * * ***************
		* ACF Custom fields Responsability social (page)
		* ***************
		* @param ACF_fields 'yokomotor_social'
		* 
	*/
	$sectionSocial = get_field( 'yokomotor_social' );  
	if( $sectionSocial && $sectionSocial['enable_section'] ): ?>
	<section class="main-social full clear-fix">
		<div class="wrapper-main-md center">
			<?php if( $sectionSocial['title'] && !empty($sectionSocial['title']) ): ?>
				<h2><?php echo $sectionSocial['title']; ?></h2>
				<?php endif; ?>
			<hr>
			<?php if( !empty($sectionSocial['description']) || $sectionSocial['image'] ): ?>
				<div class="row row-xs">
					<?php if( !empty($sectionSocial['description']) ): ?>
						<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
							<article>
								<?php echo $sectionSocial['description']; ?>
							</article>
						</div>
					<?php endif; 
					if( $sectionSocial['image'] ): $image =  $sectionSocial['image']; ?>
						<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
							<figure data-aos="fade-up" 
									data-aos-duration="600" 
									data-aos-delay="300">
								<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['title']); ?>">
							</figure>
						</div>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
	</section>
	<?php endif; 
endif;?>

<?php get_footer(); ?>