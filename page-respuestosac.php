<?php

/* Template Name: Accesorios */


get_header(); the_post();

$customType = 'yokomotor_accessory';
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner'); 

?>


<section class="main-usados-destacados full clear-fix">
        <div class="wrapper-main-sm center">
            <h3>Accesorios</h3>
            <div class="row row-xs">
                    <?php 
                        /**
                             * * ***************
                            * ACF Custom fields Accesorio (post-type)
                            * ***************
                            * @param ACF_fields 'pdf_sm'
                            * 
                        */

                    

                        $featured = get_posts( array( 'post_type' => 'yokomotor_accessory',  'numberposts' => 100,) );
                        foreach($featured as $feature):
                    ?>

            <div class="col-12 col-sm-4 col-lg-4 col-xl-4">
                   <img width="300" src="<?php echo esc_url(get_the_post_thumbnail_url($feature->ID, 'full')) ?>" >
                <?php if( get_field('pdf_sm', $feature->ID) ): ?>
                    <a target="_back" href="<?php the_field('pdf_sm', $feature->ID); ?>" >Ver Accesorios</a>
                <?php endif; ?>

                </div>

                   <?php endforeach; ?>
                            
                        </div>
                        </div>
                    </section>
                  
<?php get_footer(); ?>