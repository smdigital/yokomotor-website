<?php get_header();  ?>

<section class="main-sedes-toyomotor full clear-fix">
	<div class="wrapper-main center">
		<?php the_title('<h1>', '</h1>'); 
	
			if( function_exists( 'get_field' ) ):
			/**
			 * * ***************
			* ACF Custom fields Sedes (Page)
			* ***************
			* @param ACF_fields 'yokomotor_workshops'
				* @param ACF_subfields 'text_page'
				* @param ACF_subfields 'workshops' - Repeater
					* @param ACF_subfields 'city'
					* @param ACF_subfields 'workshop_info' - Repeater
						* @param ACF_subfields 'name'
						* @param ACF_subfields 'address'
						* @param ACF_subfields 'phone'
						* @param ACF_subfields 'image'
						* @param ACF_subfields 'url_googlemaps'
						* @param ACF_subfields 'url_waze'
						* @param ACF_subfields 'schedule'
			* 
			*/
			$sedesSection = get_field('yokomotor_workshops');
			$noAllowed = array("á", "é", "í", "ó", "ú", "Á", "É", "Í". "Ó", "Ú");
			$allowed = array("a", "e", "i", "o", "u");
				if($sedesSection):
					$title = $sedesSection['text_page'];
					$sedes = $sedesSection['workshops'];
					if($title && !empty($title)):
			?>

			<div class="leyend-sedes">
			<?php if( get_the_content() ):?>
			<article>
				<?php the_content(); ?>
			</article>
		<?php endif;?>
			</div>

			<?php 	endif;	?>

			<div id="tabs-sedes-all" class="row-sedes-all clear-fix">
			<ul class="titles-tags">

			<?php
					if($sedes):
						foreach($sedes as $sede):
							$city = $sede['city'];
							if(!empty($city)):
			?>
				<li><a href="#sede-<?php echo str_replace($noAllowed, $allowed, strtolower($city));?>"><?php echo $city;?></a></li>
			<?php 
							endif;
						endforeach;
			?>
						
			</ul>

			<?php 
						foreach($sedes as $sede):
							$city = $sede['city'];
							$sedesInfo = $sede['workshop_info'];
							if(!empty($city) && $sedesInfo):
			?>
			<div id="sede-<?php echo str_replace($noAllowed, $allowed, strtolower($city));?>" class="cont-concesionarios">
				<?php if( !empty($city) ): ?>
					<h2> <?php echo $title.' '.$city;?></h2>
				<?php endif; ?>
				<div class="row row-xs">
					<?php 
							foreach($sedesInfo as $info):
								$name = $info['name'];
								$address = $info['address'];
								$phone = $info['phone'];
								$image = $info['image'];
								$url_googlemaps = $info['url_googlemaps'];
								$url_waze = $info['url_waze'];
								$schedule = $info['schedule'];
								$id_googlemaps = $info['id_tag_manager_google_maps'];
								$id_wizer = $info['id_tag_manager_waze'];
					?>
					<div class="col-12 col-sm-4 col-lg-4 col-xl-4">
						<article class="card-sedes">
							<?php if(isset($image)): ?>
							<figure>
								<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']);?>">
							</figure>
							<?php endif; ?>
							<div class="info">
								<?php if(!empty($name)): ?>
								<h4><?php  echo $name; ?></h4>
								<?php endif; ?>
								<ul>
									<?php if($address && !empty($address)):?>
									<li><strong>Dirección:</strong> <?php echo $address; ?></li>
									<?php 
										endif;
										if($phone && !empty($phone)):
									?>
									<li><strong>Telefono:</strong> <?php echo $phone; ?></li>
									<?php 
										endif;
										if($schedule && !empty($schedule)):
									?>
									<li><strong>Lunes a viernes:</strong> <?php echo strip_tags($schedule, '<strong>'); ?></li>
									<?php endif; ?>
								</ul>
							</div>
							<hr>
							<?php if($url_googlemaps && !empty($url_googlemaps)):?>
							<a href="<?php echo esc_url($url_googlemaps);?>" id="<?php echo $id_googlemaps; ?>" class="view-map" target="_blank">Ver mapa</a>
							<?php 
								endif;
								if($url_waze && !empty($url_waze)):
							?>
							<a href="<?php echo esc_url($url_waze);?>" class="view-waze" id="<?php echo $id_wizer; ?>" target="_blank">Waze</a>
							<?php endif; ?>
						</article>
					</div>
					<?php endforeach; ?>
				</div>
					<?php endif; ?>
			</div>	
			<?php 
						endforeach;
					endif;
				endif; 
			endif;
			?>
	</div>
</section>
<?php get_footer(); ?>