<?php get_header();
/**	
 * 
 * Get Section Banner
 * @source 'page-parts/sections/' 'section-feature-banner.php'
 * 
 */
get_template_part( 'page-parts/sections/section-feacture', 'banner');

?>
<div class="main-service-workshop full clear-fix">
	<?php the_title('<h1>', '</h1>'); ?>
	<div class="row-workshop">
	<?php 
		/**
		 * 
		 * Library Mobile Detect
		 * @source 'library/includes' 'Mobile_Detect.php'
		 * */
		$detect = new Mobile_Detect;

		if( function_exists( 'get_field' ) ):
			/**
			 * * ***************
			* ACF Custom fields Página Servicios de taller (page)
			* ***************
			* @param ACF_fields 'yokomotor_services'
				* @param ACF_subfields 'service_cf'
				* @param ACF_subfields 'enable_section'
				* @param ACF_subfields 'services' - Repeater
					* @param ACF_subfields 'service_name'
					* @param ACF_subfields 'service_image'
					* @param ACF_subfields 'service_info'
			* 
			*/
			$servicesSection = get_field('yokomotor_services');

			if($servicesSection && $servicesSection['enable_section']): 
				$services 	= 	$servicesSection['services'];
				$cf			=	$servicesSection['service_cf'];
					foreach($services as $service): 
						$img 	  = $service['service_image'];
						$imgXs 	= $service['service_image_xs'];
						$title 	= $service['service_name'];
						$info 	= $service['service_info'];
		?>
			<section class="card-workshop-service full clear-fix">
				<div class="row row-xs center-vertical">
					<div class="col-12 col-sm-12 col-lg-12 col-xl-12 img-featured">
						
						<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
							<?php if( $detect->isMobile() && !$detect->isTablet() ): ?>
								<img src="<?php echo($imgXs['url']) ? $imgXs['url'] : $img['url']?>" alt="<?php echo($imgXs['url']) ? $imgXs['title'] : $img['title']?>">
								<?php else: ?>
								<img src="<?php echo esc_url($img['url']) ?>" alt="<?php echo esc_attr($img['title']);?>">
							<?php endif; ?>
						</figure>
					</div>
					<div class="col-12 col-sm-12 col-lg-12 col-xl-12 details-workshop">
						<?php if(!empty($title)):?>
							<h2><?php echo $title; ?></h2>
						<?php endif; ?>
						<?php echo (!empty($info)) ? $info : '' ; ?>
						<div class="text-center">
							<input type="hidden" name="" class="serviceName" value="<?php echo esc_attr($title); ?>">
							<a href="" class="btn-yokomotor cta-service" data-toggle="modal" ><?php _e('Solicitar servicio', 'yokomotor'); ?></a>
						</div>
					</div>
				</div>
			</section>
	<?php 
					endforeach; 
			endif; 
		endif; 
	?>
	</div>	
</div>

<!-- Lightbox Solicitar servicio -->
<div id="modal-form-serivicio" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox">
		<section class="form-yokomotor lightbox-form full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<hr>
				<?php echo ($cf && !empty($cf)) ? do_shortcode($cf) : 'No hay formulari asignado';?>
		</section>	
	</div>
</div>


<?php
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/buttons/button-rate' 'us.php'
	 * 
	 */
	get_template_part( 'page-parts/buttons/button-rate', 'us');
?>

<?php get_footer(); ?>