<?php get_header(); 
if( has_post_thumbnail() ): ?>
	<section class="banner-general full  clear-fix">
		<figure>
			<img src="<?php the_post_thumbnail_url('full'); ?>" alt="banner">
		</figure>
	</section>	
<?php endif; ?>

<section class="main-wrap-soat full clear-fix">
	<div class="wrapper-main center">
		<?php the_title('<h1>', '</h1>'); 
		if( get_the_content() ): ?>
			<article>
				<?php the_content(); ?>
			</article>
		<?php endif; 
		if( function_exists( 'get_field' ) ): 
			/**
			 * * * ***************
				* ACF Custom fields Soat (page)
				* ***************
				* @param ACF_fields 'yokomotor_form'
				* 
			*/
			$sectionSoat = get_field( 'yokomotor_form' );  
			if( $sectionSoat && $sectionSoat['enable_section'] ): ?>
				<section class="form-yokomotor full clear-fix">
				<?php if( $sectionSoat['choice_form'] && !empty($sectionSoat['choice_form']) ): 
				$form = $sectionSoat['choice_form'];?>
					<?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); 
				endif;?>
				</section>
			<?php endif; 
		endif; ?>
	</div>
</section>


<?php get_footer(); ?>