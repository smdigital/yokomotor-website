<?php get_header(); 

	/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner');

?>
<section class="main-test-drive full clear-fix">
	<div class="wrapper-main center">
		<h1><?php the_title(); ?></h1>
		<?php 
		/**
			 * * ***************
			* ACF Custom fields Página Test Drive (page)
			* ***************
			* @param ACF_fields 'yokomotor_details_test'
				* @param ACF_subfields 'enable_section'
				* @param ACF_subfields 'details'
			* 
		*/
			$testSection = get_field('yokomotor_details_test'); 

			if($testSection && $testSection['enable_section']):
				$cftest = $testSection['cf-test-drive'];
		?>
		<hr>
		<div class="details-test">
			<?php echo $testSection['details'];?>
		</div>
		<hr>
		
		<section class="form-yokomotor full clear-fix">
			<?php the_content(); ?>
			<?php echo do_shortcode($cftest);?>
		</section>
		<?php endif; ?>
	</div>
</section>
<?php
/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/buttons/button-rate' 'us.php'
	 * 
	 */
	get_template_part( 'page-parts/buttons/button-rate', 'us');
?>

<?php get_footer(); ?>