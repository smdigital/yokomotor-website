<?php get_header(); ?>

<?php if( has_post_thumbnail() ): ?>
	<section class="banner-general full  clear-fix">
		<figure>
			<img src="<?php the_post_thumbnail_url('full'); ?>" alt="banner">
		</figure>
	</section>	
<?php endif; ?>
<section class="main-job full clear-fix">
	<div class="wrapper-main center">
		<?php the_title('<h1>', '</h1>');?>
		<article>
			<?php if( function_exists( 'get_field' ) ): 
				/**
				 * * * ***************
					* ACF Custom fields work with us (page)
					* ***************
					* @param ACF_fields 'yokomotor_job'
					* 
				*/
				$sectionWorks = get_field( 'yokomotor_job' );  
				if( $sectionWorks && ($sectionWorks['enable_section'] &&count($sectionWorks['btn_links']) > 0) ): $works = $sectionWorks['btn_links']; ?>
					<div class="text-center btns-live">
						<?php foreach( $works as $work ): $url =!empty( $work['link'] ) ? $work['link'] : false;  ?>
						<a href="<?php echo($url['url']) ? esc_url( $url['url']) : 'javascript:;'; ?>" target="<?php echo( $url['target'] ) ? esc_html( $url['target'] ) : '_self'; ?>" class="btn-yokomotor btn-vacante"><?php echo $url['title']; ?></a>
						<?php endforeach; ?>
						<!-- <a href="javascript:;" class="btn-yokomotor btn-vacante">VACANTES</a>			 -->	
					</div>	
				<?php endif;
			endif;
			if( get_the_content() ) {
				the_content();
			}; ?>
		</article>
		
	</div>
</section>
<?php get_footer(); ?>