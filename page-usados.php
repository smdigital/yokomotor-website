<?php get_header(); 

	/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section', 'banner');

	if( function_exists( 'get_field' ) ):
		/**
		* * ***************
		* ACF Custom fields Usados (Page)
		* ***************
		* @param ACF_fields 'yokomotor_highlights'
		* @param ACF_subfields 'enable_section'
		* @param ACF_fields 'yokomotor_brands'
		* @param ACF_subfields 'enable_section'
		* @param ACF_fields 'yokomotor_pqrs'
		* @param ACF_subfields 'enable_section'
		* @param ACF_fields 'yokomotor_new_cars'
		* @param ACF_subfields 'enable_section'
		* 
		*/

		$highlightsSection = get_field('yokomotor_highlights');
		$brandsSection = get_field('yokomotor_brands');
		$pqrsSection = get_field('yokomotor_pqrs');
		$newCarsSection = get_field('yokomotor_new_cars');

?>
              <!--Seccion Nuestras Categorias Usados!-->

			  <?php  get_template_part('page-parts/sections/section-categories-used')?> 

			  <!--Fin Seccion Nuestras Categorias Usados!-->

<?php if($highlightsSection && $highlightsSection['enable_section']): ?>
<section class="main-usados-destacados full clear-fix">
	<div class="wrapper-main-sm center">
		<h3>VEHÍCULOS DESTACADOS</h3>
		<div class="text-center hidden-lg">
			<a href="ver-todos/" class="btn-arrow-right">Ver todos</a>
		</div>
		<div class="row-swiper-clothes">
			<div class="swiper swiper-clothes">
				<div class="swiper-wrapper">
					<?php 
						/**
						 * * ***************
						* ACF Custom fields Usados (post-type)
						* ***************
						* @param ACF_fields 'price'
						* @param ACF_fields 'highlight_car'
						* 
						*/
						$featured = get_posts( array( 'post_type' => 'yokomotor_used') );
						foreach($featured as $feature):
							$price = get_field('price', $feature->ID);
							$isFeatured = get_field('highlight_car', $feature->ID);
							if($isFeatured):
					?>
							<div class="swiper-slide">
								<article class="cart-usado-destacado">
									<a href="<?php echo get_permalink($feature->ID);?>">
										<?php if(has_post_thumbnail($feature->ID)): ?>
											<figure>
												<img src="<?php echo esc_url(get_the_post_thumbnail_url($feature->ID, 'full')) ?>" alt="<?php echo esc_attr($feature->post_name); ?>">
											</figure>
										<?php endif; ?>
										<?php if($price && !empty($price)): ?>
											<h6>$<?php echo number_format($price, 0, '.', '.');?></h6>
										<?php endif; ?>
										<div class="figcaption">
											<p><?php echo strtoupper(get_the_title($feature->ID)); ?></p>
										</div>
									</a>
								</article>
							</div>
					<?php 	
							endif;
						endforeach;
					?>
				</div>
			</div>
			<div class="button-next next-black next-clothes"></div>
			<div class="button-prev prev-black prev-clothes"></div>
			<div class="swiper-pagination pagination-clothes"></div>
		</div>
		<div class="text-center hidden-xs">
			<a href="ver-todos/" class="btn-arrow-right">Ver todos</a>
		</div>
	</div>
</section>
<?php endif; ?>
<?php if($brandsSection && $brandsSection['enable_section']): ?>
<!-- <section class="main-marcas-usados full clear-fix">
	<div class="wrapper-main center">
		<h3>PRINCIPALES MARCAS</h3>
		<div class="row row-xs">
			<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
				<a href="">					
					<div class="banner-marcas-ppal full clear-fix" style="background: url('<?php echo get_stylesheet_directory_uri(). '/library/' ?>/images/usados/bg-banner-usados-toyota.jpg') no-repeat center center;">
						<article>
							<h3>TOYOTA</h3>
							<div class="btn-yokomotor-arrow">VER MÁS</div>			
						</article>						
					</div>
				</a>
			</div>
			<div class="col-12 col-sm-6 col-lg-6 col-xl-6">
				<a href="">
					<div class="banner-marcas-ppal full clear-fix" style="background: url('<?php echo get_stylesheet_directory_uri(). '/library/' ?>/images/usados/bg-banner-usados-otros.jpg') no-repeat center center;">
					<article>
						<h3>OTRAS MARCAS</h3>
						<div class="btn-yokomotor-arrow">VER MÁS</div>			
					</article>
					</div>					
				</a>
			</div>
		</div>
	</div>
</section> -->
<?php endif; ?>
<?php if($pqrsSection && $pqrsSection['enable_section']): ?>
<!-- <section class="main-usados-pqrs full clear-fix">
	<div class="wrapper-main center">
		<h3>Retoma de vehÍculos</h3>
		<a href="">
			<div class="banner-pqrs full clear-fix" style="background: url('<?php echo get_stylesheet_directory_uri(). '/library/' ?>/images/usados/bg-pqrs.jpg') no-repeat center center;">						
					<div class="btn-yokomotor-arrow">VER MÁS</div>			
			</div>			
		</a>
	</div>
</section> -->
<?php endif; ?>

<?php endif; get_footer(); ?>
