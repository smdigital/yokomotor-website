<?php get_header(); 
/**
 * 
 * Library Mobile Detect
 * @source 'library/includes' 'Mobile_Detect.php'
 * */
$detect = new Mobile_Detect; 
if( function_exists( 'get_field' ) ): 
	/**
	 * * ***************
	* ACF Custom fields Yokocomotor (page)
	* ***************
	* @param ACF_fields 'yokomotor_banner'
	* 
	*/
	$bannerSection = get_field( 'yokomotor_banner' ); 
	if( ($bannerSection && $bannerSection['enable_section']) && ($bannerSection['banner_xs']['url'] || $bannerSection['banner']['url']) ): ?>
		<section class="main-yokomotor-banner full clear-fix" style="background: #000 url(<?php echo( $detect->isMobile() && !$detect->isTablet() ) ? $bannerSection['banner_xs']['url'] : $bannerSection['banner']['url'] ; ?>)no-repeat center center; background-size: cover;">
			<article>
				<?php the_title('<h1>', '</h1>'); 
				if( get_the_content() ) {
					the_content();
				}; ?>
			</article>
		</section>
	<?php endif; 
endif; 

/**
	 * * ***************
	* ACF Custom fields Logo (Taxonomy)
	* ***************
	* @param ACF_fields 'yokomotor_logo'
	* 
	*/
	$logoSeccion = get_field( 'yokomotor_logo' ); 

if( $logoSeccion && $logoSeccion['enable_section'] ): 
	$logo = $logoSeccion['logo'];
?>
	<section class="toyota-what-is full clear-fix">
		<div class="wrapper-main center">
				<?php 
				/**
					 * * ***************
					* ACF Custom fields Yokomotor (Page)
					* ***************
					* @param ACF_fields 'yokomotor_descripcion'
					* 
					*/
					$descriptionSeccion = get_field( 'yokomotor_description' ); 

				if( $descriptionSeccion && ($descriptionSeccion['enable_section'] && $descriptionSeccion['description']) ): ?>
				<div class="row row-xs center-vertical">
					<div class="col-12 col-sm-12 col-lg-12 col-xl-12 center">

						<article>
							<?php echo $descriptionSeccion['description']; ?>
						</article>
					</div>
				</div>
				<?php endif; ?>
			<hr />
			<div class="row row-xs center-vertical">
				<?php if( $logo ): ?>
					<div class="col-12 col-sm-5 col-lg-5 col-xl-5 center">
						<figure data-aos="fade-up"  data-aos-delay="300"  data-aos-duration="1500">
							<img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_url($logo['title']); ?>">
						</figure>
					</div>
				<?php endif; 
				if( $logoSeccion['description'] && !empty($logoSeccion['description']) ): ?>
					<div class="col-12 col-sm-7 col-lg-7 col-xl-7 center">
						<article>
							<?php echo $logoSeccion['description']; ?>
						</article>		
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section> 
<?php endif; 

/**
	 * * ***************
	* ACF Custom fields Accordion (Taxonomy)
	* ***************
	* @param ACF_fields 'seccion_accordion'
	* 
	*/
	$accordionSeccion = get_field( 'seccion_accordion' ); 

if( $accordionSeccion['accordion'] && (isset($accordionSeccion['accordion'])) && count($accordionSeccion['accordion']) > 0 ): $accordions = $accordionSeccion['accordion']; ?>
	<section class="time-line-toyota full clear-fix">
		<div class="accordion-detalles">
			<?php foreach( $accordions as $accordion ): ?>
				<?php if( $accordion['title'] ): ?>
					<h3><?php echo $accordion['title']; ?></h3>
				<?php endif; 
				if( $accordion['description'] ): ?>		
					<div class="cont-time">
						<?php echo $accordion['description']; ?>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
		</div>	
	</section>
<?php endif; ?>
<?php get_footer(); ?>