<?php 

get_header(); 

global $post;
$categories = get_the_terms($post->ID, 'blog_cats');

do_action( 'yokomotor_get_back_button' );
?>

<?php 
if( function_exists( 'get_field' ) ):
	/**
		 * * ***************
		* ACF Custom fields Blog - Banner (single)
		* ***************
		* @param ACF_fields 'yokomotor_banner'
			* @param ACF_subfields 'enable_section'
			* @param ACF_subfields 'image'
		* 
	*/
	$sectionBanner = get_field('yokomotor_banner');

	if($sectionBanner && $sectionBanner['enable_section']):
		$imgBanner = $sectionBanner['image'];
?>
<section class="banner-blog full clear-fix">
	<figure>
		<img src="<?php echo esc_url($imgBanner['url']); ?>" alt="<?php echo esc_attr($imgBanner['alt']); ?>">
	</figure>
</section>

<?php 
	endif; 
?>

<section class="main-blog-detalle full clear-fix">
	<div class="wrapper-main center">
	<?php echo the_title('<h1>', '</h1>');
		if( get_the_excerpt() ): ?>
		<?php if( $categories && count($categories) > 0 ): ?>
			<h6>
				<?php foreach($categories as $category): ?>
					<?php echo $category->name; ?>
				<?php endforeach; ?>
			</h6>
		<?php endif; ?>
			<article class="leyeng-blog">
				<p><?php echo the_excerpt();?></p>
			</article>
		<?php endif;?>
		<div class="the_content-blog full clear-fix">
			<?php echo the_content();?>
			<hr>
			<div class="cont-share">
				<span><?php _e('Compartir', 'yokomotor');?></span>
				<div class="redes-share">
					<ul>
						<li class="icon-fb"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(get_permalink());?>" target="_blank">facebook</a></li>
						<li class="icon-tw"><a href="https://twitter.com/share?text=<?php echo the_title(); ?>&url=<?php echo esc_url(get_permalink());?>" target="_blank">twitter</a></li>
						<!-- <li class="icon-it"><a href="" target="_blank">instagram</a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<?php 
	endif; 
?>
<?php get_footer(); ?>