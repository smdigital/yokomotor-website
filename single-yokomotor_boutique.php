<?php get_header(); the_post();

/**
 * 
 * Library Mobile Detect
 * @source 'library/includes' 'Mobile_Detect.php'
 * */
$detect = new Mobile_Detect;

/**
 * Get terms boutique_cats and Post type name
 */
$customType = get_post_type(get_the_ID());

$taxterms = wp_get_post_terms( get_the_ID(), 'boutique_cats', array('hide_empty' => false, 'fields' => 'names' ));

do_action( 'yokomotor_get_back_button' );
?>
<?php  if( function_exists( 'get_field' ) ):
	/**
		 * * ***************
		* ACF Custom fields Boutique (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_details'
		* 
		*/
		$detailsSection = get_field('yokomotor_details'); 
	if( $detailsSection && $detailsSection['enable_section']  ): ?>

		<div class="wrapper-main-sm center main-details-shop">
			<!-- Este h1 y h2 Aplica para mobile  -->
			<?php if ( $detect->isMobile() && !$detect->isTablet() ) : ?>
				<article class="hidden-lg">
					<?php the_title('<h1>', '</h1>'); 
					if( $taxterms && !empty($taxterms) ): ?>
						<h2>
							<?php foreach ( $taxterms as $key => $taxterm ): ?>
								<?php echo $taxterm; echo ( $index < count($taxterms) )? ", " : " "; ?>
							<?php $index ++; endforeach; ?> 
						</h2>
					<?php endif; ?>
				</article>
			<?php endif; ?>
			
			<div class="row row-xs">
				<div class="col-12 col-sm-6 col-lg-6 col-xl-6 thumbnail-clothes">
					<div class="row-swiper-ropa">
						<?php if( $detailsSection['gallery'] && (isset($detailsSection['gallery']) && count($detailsSection['gallery']) > 0) ): $galleries = $detailsSection['gallery'] ?>
							<div class="swiper swiper-ropa">
								<div class="swiper-wrapper">
									<?php foreach( $galleries as $gallery ): ?>
										<div class="swiper-slide">
											<figure>
												<img src="<?php echo esc_url($gallery['img']['url']); ?>" alt="<?php echo esc_url($gallery['img']['title']); ?>">
											</figure>
										</div>
									<?php endforeach; ?>
								</div>
							</div>    
							<div class="button-prev prev-white prev-ropa"></div>
							<div class="swiper-pagination pagination-ropa"></div>
							<div class="button-next next-white next-ropa"></div> 
						<?php elseif( has_post_thumbnail() ): ?>
							<figure>
								<?php the_post_thumbnail( 'full' ); ?>
							</figure>
						<?php else: ?>
							<figure>
								<img src="<?php echo get_stylesheet_directory_uri(). '/library/' ?>images/camisa-default.png" alt="">
							</figure>
						<?php endif; ?>		
					</div>	
				</div>
				<div class="col-12 col-sm-6 col-lg-6 col-xl-6 clothes-details">
					<article class="details-shop-ropa">
						<?php 
						if ( !$detect->isMobile() ) : 
							the_title('<h1>', '</h1>'); ?>
						<?php endif; 
							$index = 1; 
							if( $taxterms && !empty($taxterms) ): ?>
								<h2>
									<?php foreach ( $taxterms as $key => $taxterm ): ?>
										<?php echo $taxterm; echo ( $index < count($taxterms) )? ", " : " "; ?>
									<?php $index ++; endforeach; ?> 
								</h2>
							<?php endif; 
						if( $detailsSection['price'] && !empty($detailsSection['price']) ): ?>
							<h3>$<?php echo $detailsSection['price']; ?></h3>
						<?php endif; ?>
						<div class="input-shop input-cantidad">
							<?php if(  $detailsSection['title_amount'] && !empty($detailsSection['title_amount']) ): ?>
								<label><?php echo $detailsSection['title_amount']; ?></label>
							<?php endif; ?>
							<button type="button" class="minus qib-button">-</button>
							<input type="text" value="1" class="result">
							<button type="button" class="plus qib-button">+</button>
						</div>
						<?php if( $detailsSection['sizes'] && (isset($detailsSection['sizes']) && count($detailsSection['sizes']) > 0) ): $sizes =  $detailsSection['sizes']; ?>
						<div class="input-shop input-talla">
							<?php if(  $detailsSection['sizes_title'] && !empty($detailsSection['sizes_title']) ): ?>
								<label><?php echo $detailsSection['sizes_title']; ?></label>
							<?php endif; ?>
								<select name="" class="selected-size">
									<?php foreach( $sizes as $size ): ?>
										<option value="<?php echo str_replace(' ', '', $size['title']); ?>"><?php echo $size['title']; ?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<?php endif; ?>
						<div class="clr"></div>
							<div class="main-form-fieldset obsservacion-boutique">
							<fieldset>
								<legend>Observaciones</legend>
								<textarea name="message" class="message" maxlength="120"></textarea>
							</fieldset>

							</div>
							<?php /**
								 * * ***************
								* ACF Custom fields Página insurance (page)
								* ***************
								* @param ACF_fields 'yokomotor_adviser'
								* 
								*/

								$adviserSection = get_field( 'yokomotor_adviser', 'yokomotor_boutique'  );

								if( $adviserSection && ($adviserSection['enable_section'] &&count($adviserSection['contacs']) > 0) ): $advisers = $adviserSection['contacs']; ?>
									<div class="text-center-btn">
										<div class="btn-tooltip">
											<div class="btn-yokomotor icon-whatsapp"><?php echo $adviserSection['title']; ?></div>
												<ul class="tooltip-cities">
													<?php foreach( $advisers as $key => $adviser ): ?>
														<li data-city="city-<?php echo $key; ?>">
															<a href="javascript:;"><strong><?php echo $adviser['city']; ?> </strong></a>
														</li>
													<?php endforeach; ?>
												</ul>	
												<ul class="tooltip-shop" style="display: none;">
													<?php foreach( $advisers as $key => $adviser ): $shops = $adviser['shop']; 
														foreach( $shops as $shop ): ?>
															<li data-shop="city-<?php echo $key; ?>">
																<?php if( $shop['phone'] && !empty($shop['phone']) ): ?>
																	<a href="https://api.whatsapp.com/send?phone=57<?php echo esc_html($shop['phone']); ?>&text=¡Hola,%20mi%20nombre%20es%20_______! Estoy%20interesado%20en%20adquirir%20valorunidades%20unidades%20de%20<?php the_title(); ?>%20en%20talla%20valortalla%20NOTA:%20message" target="_blank">
																		<?php echo( $shop['shop'] ) ? '<strong>'.esc_html($shop['shop']).'</strong>': null; ?><?php echo esc_html($shop['phone']); ?>
																	</a>
																<?php endif; ?>
															</li>
														<?php endforeach; 
													endforeach; ?>
												</ul>	
											</div>
										</div>
									</div>
								<?php endif; ?>
						<div class="cont-share">
							<span>Compartir</span>
							<div class="redes-share">
								<ul>
								<li class="icon-fb"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(get_permalink());?>" target="_blank">facebook</a></li>
								<li class="icon-tw"><a href="https://twitter.com/share?text=<?php echo the_title(); ?>&url=<?php echo esc_url(get_permalink());?>" target="_blank">twitter</a></li>
									<!-- <li class="icon-it"><a href="" target="_blank">instagram</a></li> -->
								</ul>
							</div>
						</div>
					</article>
				</div>
			</div>
		</div>
  <?php endif; 
endif; 

$args = [
	'posts_per_page'   => -1,
	'post_type'        => $customType,
	'suppress_filters' => false,
	'post_status'      => 'publish',
	'order'            => 'ASC',
	'post__not_in' 		 => [get_the_ID()],
	'tax_query' 			 => [
		'relation' 			 => 'AND',
				[
					'taxonomy' => 'boutique_cats',
					'field'    => 'names',
					'terms'    => $taxterms
				]
	],
];
$relateBoutiques =  get_posts( $args );

if( $relateBoutiques && (isset($relateBoutiques) && count($relateBoutiques) > 0) ): ?>
	<section class="main-clothes full clear-fix">
		<div class="wrapper-main-sm center">
			<h2>También te puede interesar</h2>
			<div class="row-swiper-clothes">
			<?php get_template_part( 'page-parts/sliders/slider', 'related-boutique', $relateBoutiques ); ?>
			</div>
		</div>
	</section>
<?php endif; ?>


<?php get_footer(); ?>