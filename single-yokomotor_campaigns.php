<?php get_header(); 

/**
 * 
 * Library Mobile Detect
 * @source 'library/includes' 'Mobile_Detect.php'
 * */
$detect = new Mobile_Detect;

/**
 * Get terms boutique_cats and Post type name
 */
$customType = get_post_type(get_the_ID());

/* $taxterms = wp_get_post_terms( get_the_ID(), 'accesory_cats', array( 'hide_empty' => false, 'fields' => 'names' )); */ 
do_action( 'yokomotor_get_back_button' );
?>

<div class="banner-accesorios-detailed wrapper-main center">
	<?php the_title('<h1>', '</h1>'); 	
	
	if( function_exists( 'get_field' ) ):
		/**
			 * * ***************
			* ACF Custom fields Página campaings (page)
			* ***************
			* @param ACF_fields 'yokomotor_banner'
			* 
			*/

			$bannerSection = get_field( 'yokomotor_banner' );

		if( $bannerSection && $bannerSection['enable_section'] ): $banner = $bannerSection['image_banner']; ?>
			<figure>
				<img src="<?php echo esc_url($banner['url']); ?>" alt="<?php echo esc_attr($banner['title']); ?>">
			</figure>
		<?php endif; 
	endif; ?>
</div>
<section class="accesorios-detailed-shop full clear-fix">
	<div class="wrapper-main center">
	<?php if( get_the_content()) {
		echo the_content(); 
	}; ?>
		
	<?php	/**
			 * * ***************
				* ACF Custom fields Página financing (page)
				* ***************
				* @param ACF_fields 'yokomotor_adviser'
				* 
				*/

				$adviserSection = get_field( 'yokomotor_adviser', 'yokomotor_campaigns' );

			if( $adviserSection && ($adviserSection['enable_section'] &&count($adviserSection['contacs']) > 0) ): $advisers = $adviserSection['contacs']; ?>
				<div class="text-center-btn">
					<div class="btn-tooltip">
						<div class="btn-yokomotor icon-whatsapp"><?php echo $adviserSection['title']; ?></div>
						<ul class="tooltip-cities">
							<?php foreach( $advisers as $key => $adviser ): ?>
								<li data-city="city-<?php echo $key; ?>">
									<a href="javascript:;"><strong><?php echo $adviser['city']; ?> </strong></a>
								</li>
							<?php endforeach; ?>
						</ul>	
						<ul class="tooltip-shop" style="display: none;">
							<?php foreach( $advisers as $key => $adviser ): $shops = $adviser['shop']; 
								foreach( $shops as $shop ): ?>
									<li data-shop="city-<?php echo $key; ?>">
										<?php if( $shop['phone'] && !empty($shop['phone']) ): ?>
											<a href="https://api.whatsapp.com/send?phone=57<?php echo esc_html($shop['phone']); ?>&text=<?php echo( $adviserSection['whatsaap']) ? $adviserSection['whatsaap'] : 'Hola,%20quiero%20comunicarme%20con%20%Comercial%20Yokomotor';?>" target="_blank">
											<?php echo( $shop['shop'] ) ? '<strong>'.esc_html($shop['shop']).'</strong>': null; ?><?php echo esc_html($shop['phone']); ?>
											</a>
										<?php endif; ?>
									</li>
								<?php endforeach; 
							endforeach; ?>
						</ul>	
						</div>
					</div>
				</div>
			<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>