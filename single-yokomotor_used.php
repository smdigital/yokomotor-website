<?php 
	get_header(); 
	/**
	 * 
	 * Library Mobile Detect
	 * @source 'library/includes' 'Mobile_Detect.php'
	 * */
	$detect		= 	new Mobile_Detect;
	$post_id	=	get_the_ID();
	$termMatch	=	'';
	$cityTaxonomies 	= get_the_terms($post_id, 'city_cats');
	$brandTaxonomies 	= get_the_terms($post_id, 'brand_cats');

	do_action( 'yokomotor_get_back_button' );

	if( function_exists( 'get_field' ) ):
		/**
		 * * ***************
		* ACF Custom fields Usadoss (Single post)
		* ***************
		* @param ACF_fields 'price'
		* @param ACF_fields 'refer'
		* @param ACF_fields 'year'
		* @param ACF_fields 'color'
		* @param ACF_fields 'fuel_type'
		* @param ACF_fields 'doors'
		* @param ACF_fields 'transmition'
		* @param ACF_fields 'motor'
		* @param ACF_fields 'body_type'
		* @param ACF_fields 'km'
		* @param ACF_fields 'direction'
		* @param ACF_fields 'abs_brakes'
		* @param ACF_fields 'air_conditioning'
		* @param ACF_fields 'alarm'
		* @param ACF_fields 'bluetooth'
		* @param ACF_fields 'electric_mirrors'
		* @param ACF_fields 'gps'
		* @param ACF_fields 'mp3'
		* @param ACF_fields 'airbag'
		* @param ACF_fields 'usb'
		* @param ACF_fields 'dvd'
		* @param ACF_fields 'steering_wheel_height'
		* @param ACF_fields 'parking'
		* @param ACF_fields 'electric_glasses'
		* @param ACF_fields 'folding_rear_seat'
		* @param ACF_fields 'remote_trunk'
		* @param ACF_fields 'electric_seats'
		* @param ACF_fields 'front_fog_lights'
		* @param ACF_fields 'lights_automatics'
		* @param ACF_fields 'rear_fog_lights'
		* @param ACF_fields 'rear_defogger'
		* @param ACF_fields 'central_door_locking'
		* @param ACF_fields 'vendor_name'
		* @param ACF_fields 'vendor_email'
		* @param ACF_fields 'vendor_phone'
		* @param ACF_fields 'images' - Repeater
			* @param ACF_subfields 'img'
		* 
		*/

		$price 				= (!empty(get_field('price'))) ? get_field('price') : false;
		$refer 				= (!empty(get_field('refer'))) ? get_field('refer') : false;
		$year 				= (!empty(get_field('year'))) ? get_field('year') : false;
		$km 				= (!empty(get_field('km'))) ? get_field('km') : false;
		$doors 				= (!empty(get_field('doors'))) ? get_field('doors') : false;
		$color 				= (!empty(get_field('color'))) ? get_field('color') : false;
		$motor 				= (!empty(get_field('motor'))) ? get_field('motor') : false;
		$vendor_phone 		= (!empty(get_field('vendor_phone'))) ? get_field('vendor_phone') : false;
		$vendor_name 		= (!empty(get_field('vendor_name'))) ? get_field('vendor_name') : false;
		$vendor_email 		= (!empty(get_field('vendor_email'))) ? get_field('vendor_email') : false;
		$vendor_workplace 	= (!empty(get_field('workplace'))) ? get_field('workplace') : false;
		$images 			= (!empty(get_field('images'))) ? get_field('images') : false;
		$fuel_type 			= get_field('fuel_type');
		$transmition 		= get_field('transmition');
		$traction 			= get_field('traction');
		$body_type 			= get_field('body_type');
		$direction 			= get_field('direction');
		$abs_brakes 		= get_field('abs_brakes');
		$air_conditioning 	= get_field('air_conditioning');
		$alarm 				= get_field('alarm');
		$bluetooth 			= get_field('bluetooth');
		$electric_mirrors 	= get_field('electric_mirrors');
		$gps 				= get_field('gps');
		$mp3 				= get_field('mp3');
		$airbag 			= get_field('airbag');
		$usb 				= get_field('usb');
		$dvd 				= get_field('dvd');
		$steering_wheel_height = get_field('steering_wheel_height');
		$parking 			= get_field('parking');
		$electric_glasses 	= get_field('electric_glasses');
		$folding_rear_seat 	= get_field('folding_rear_seat');
		$remote_trunk 		= get_field('remote_trunk');
		$electric_seats 	= get_field('electric_seats');
		$front_fog_lights 	= get_field('front_fog_lights');
		$lights_automatics 	= get_field('lights_automatics');
		$rear_fog_lights 	= get_field('rear_fog_lights');
		$rear_defogger 		= get_field('rear_defogger');
		$central_door_locking = get_field('central_door_locking');
?>
<section class="wrapper-main-sm center main-shop-cart-old">
	<article class="hidden-lg">
		<h1><?php echo get_the_title($post_id); ?></h1>
		<ul class="modelo-km">
			<li><?php echo $year; ?></li>
			<li><?php echo number_format($km, 0, '.', '.');?>KM</li>
		</ul>		
	</article>
	<div class="row row-xs">
        <div class="col-12 col-sm-6 col-lg-6 col-xl-6 thumbnail-clothes">
        	<div class="row-swiper-old">
				<div class="swiper swiper-ropa">
					<div class="swiper-wrapper">
						<?php if($images && is_array($images)):
							foreach($images as $image): ?>
							<div class="swiper-slide">
								<figure>
									<img src="<?php echo esc_url($image['img']['url']); ?>" alt="<?php echo esc_attr($image['img']['alt']); ?>">
								</figure>
							</div>
						<?php endforeach; endif;?>
					</div>
				</div>    
				<div class="button-prev prev-white prev-ropa"></div>
				<div class="swiper-pagination pagination-ropa"></div>
				<div class="button-next next-white next-ropa"></div> 
        	</div>			
		</div>
		<div class="col-12 col-sm-6 col-lg-6 col-xl-6 clothes-details">
			<article class="details-cart-old">
				<h1><?php echo get_the_title($post_id);?></h1>
				<ul class="modelo-km">
					<li><h2><?php echo $year; ?></h2></li>
					<li><h2><?php echo number_format($km, 0, '.', '.');?>KM</h2></li>
				</ul>			
				<h3>$<?php echo number_format($price, 0, '.', '.');?></h3>
				<h4 class="city"><?php if(is_array($cityTaxonomies)){foreach($cityTaxonomies as $city){echo $city->name;} }?></h4>	
				<div class="clr"></div>
				<a href="javascript:;" class="btn-red-lg btn-form-asesor" data-toggle="modal" data-target="#modal-form-usados">PREGUNTAR</a>
				<a href="https://api.whatsapp.com/send?phone=57<?php echo $vendor_phone;?>" class="btn-whatsapp-white">WHATSAPP</a>
				<input type="hidden" id="email-asesor" value="<?php echo $vendor_email; ?>">
				<input type="hidden" id="sede-asesor" value="<?php echo $vendor_workplace; ?>">
				<div class="clr"></div>
				<!-- Compartir -->
				<div class="cont-share">
					<h2>Compartir</h2>
					<div class="redes-share">
            			<ul>
            				<li class="icon-fb"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_url(get_permalink());?>" target="_blank">facebook</a></li>
            				<li class="icon-tw"><a href="http://twitter.com/share?text=<?php echo the_title(); ?>&url=<?php echo esc_url(get_permalink());?>" target="_blank">twitter</a></li>
            				<!-- <li class="icon-it"><a href="" target="_blank">instagram</a></li> -->
            			</ul>				
            		</div>
				</div>
			</article>
		</div>
	</div>
</section>
<section class="caracteristicas-cart-old full clear-fix">
	<div class="wrapper-main-sm center">
		<h3>Características principales</h3>
		<div class="row-old-table">
			<div class="tr">
				<div class="td">Marca</div>
				<div class="td"><?php if(is_array($brandTaxonomies)){foreach($brandTaxonomies as $brand){echo $brand->name;}} ?></div>
			</div>
			<div class="tr">
				<div class="td">Modelo</div>
				<div class="td"><?php echo $refer;?></div>
			</div>
			<div class="tr">
				<div class="td">Año</div>
				<div class="td"><?php echo $year;?></div>
			</div>
			<div class="tr">
				<div class="td">Color</div>
				<div class="td"><?php echo $color;?></div>
			</div>
			<div class="tr">
				<div class="td">Tipo de combustible</div>
				<div class="td"><?php echo $fuel_type;?></div>
			</div>
			<div class="tr">
				<div class="td">Puertas</div>
				<div class="td"><?php echo $doors;?></div>
			</div>
			<div class="tr">
				<div class="td">Transmisión</div>
				<div class="td"><?php echo $transmition['label'];?></div>
			</div>
			<div class="tr">
				<div class="td">Motor</div>
				<div class="td"><?php echo $motor;?></div>
			</div>
			<div class="tr">
				<div class="td">Tipo de Carrocería</div>
				<div class="td"><?php echo $body_type;?></div>
			</div>
			<div class="tr">
				<div class="td">Kilómetros</div>
				<div class="td"><?php echo number_format($km, 0, '.', '.');?> km</div>
			</div>
		</div>
		<div class="text-center">
			<h4><a href="javascript:;" class="btn-arrow-right" data-toggle="modal" data-target="#modal-caracteristicas">Ver más características</a></h4>
		</div>
	</div>	
</section>	
<?php if(!empty(get_the_content($post_id))):?>
	<section class="main-mns-car full clear-fix">
		<div class="wrapper-main-sm center">
			<article>
			    <h3 class="descripcion-general">DESCRIPCIÓN GENERAL</h3>
				<p><?php echo get_the_content($post_id); ?></p>			
			</article>
		</div>
	</section>
<?php endif; ?>
<a href="javascript:;" class="btn-fixed-mns btn-form-asesor" data-toggle="modal" data-target="#modal-form-usados">
	<span>Preguntar al vendedor</span>	
</a>
<?php 
	$taxTerms	=	wp_get_post_terms($post_id, 'used_cats', array('hide_empty' => false, 'field' => 'slug', 'parent' => 0 ));
	foreach($taxTerms as $term){$termMatch = $term->slug;}
	$args = [
		'posts_per_page'   => -1,
		'post_type'        => 'yokomotor_used',
		'post_status'      => 'publish',
		'order'            => 'ASC',
		'post__not_in' 		 => [$post_id],
		'tax_query' 			 => [
			[
				'taxonomy' => 'used_cats',
				'field'    => 'slug',
				'terms'    => $termMatch
			]
		],
	];
	$relateUsedCars =  get_posts( $args ); 
	if(is_array($relateUsedCars) && count($relateUsedCars) > 0):
?>
	<section class="main-usados-destacados relacionados-car full clear-fix">
		<div class="wrapper-main-sm center">
			<h5>TAMBIÉN TE PUEDE INTERESAR</h5>		
			<div class="row-swiper-clothes">
				<div class="swiper swiper-clothes">
					<div class="swiper-wrapper">
						<?php 
							foreach($relateUsedCars as $related):
								$priceRelated	=	get_field('price', $related->ID);
						?>
						<div class="swiper-slide">
							<article class="cart-usado-destacado">
								<a href="<?php echo get_permalink($related->ID);?>">
								<?php if(has_post_thumbnail($related->ID)): ?>
									<figure>
										<img src="<?php echo esc_url(get_the_post_thumbnail_url($related->ID, 'full')) ?>" alt="<?php echo esc_attr(get_the_title($related->ID)); ?>">
									</figure>
								<?php endif; ?>
									<h6>$<?php echo number_format($priceRelated, 0, '.', '.');?></h6>
									<div class="figcaption">
										<p><?php echo get_the_title($related->ID); ?></p>
									</div>
								</a>
							</article>
						</div>
						<?php endforeach; ?>
					</div>
				</div>
				<div class="button-next next-black next-clothes"></div>
				<div class="button-prev prev-black prev-clothes"></div>
				<div class="swiper-pagination pagination-clothes"></div>
			</div>		
		</div>
	</section>
<?php endif; ?>
<!-- Lightbox Vehiculos usados -->
<?php 
	/**
	* ****************
	* @source ACF ARCHIVE PLUGIN
	* ACF Custom fields Usadoss (Single post)
	* ***************
	* @param ACF_fields 'yokomotor_form'
	* 
	*/
	$formVehicles = get_field( 'yokomotor_form' ); 
	if( $formVehicles['choice_form'] && !empty($formVehicles['choice_form']) ): 
		$form = $formVehicles['choice_form'];?>
			<div id="modal-form-usados" class="modal animate__animated animate__fadeInDown">
				<div class="flex-lightbox">
					<?php echo do_shortcode($form); ?>
				</div>
			</div>
<?php endif; ?>

<!-- Modal Caracteristicas Carro --> 
<div id="modal-caracteristicas" class="modal animate__animated animate__fadeInDown">
    <div class="flex-lightbox modal-center-xs">
		<section class="lightbox-usados full clear-fix">	
			<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
			<div class="modal-caracteristicas">
				<h3>DESCRIPCIÓN GENERAL</h3>
				<ul>
					<?php if(!empty($direction)): ?><li>Dirección: <?php echo $direction; ?></li><?php endif; ?>
					<?php if($traction != 'No aplica'): ?><li>Tipo de tracción: <?php echo $traction; ?></li><?php endif; ?>
					<?php if($abs_brakes != 'No aplica'): ?><li>Frenos ABS: <?php echo $abs_brakes; ?></li><?php endif; ?>
					<?php if($air_conditioning != 'No aplica'): ?><li>Aire acondicionado: <?php echo $air_conditioning; ?></li><?php endif; ?>
					<?php if($alarm != 'No aplica'): ?><li>Alarma: <?php echo $alarm; ?> </li><?php endif; ?>
					<?php if($bluetooth != 'No aplica'): ?><li>Bluetooth: <?php echo $bluetooth; ?></li><?php endif; ?>
					<?php if($electric_mirrors != 'No aplica'): ?><li>Retrovisores eléctricos: <?php echo $electric_mirrors; ?></li><?php endif; ?>
					<?php if($gps != 'No aplica'): ?><li>GPS: <?php echo $gps; ?></li><?php endif; ?>
					<?php if($mp3 != 'No aplica'): ?><li>Reproductor de MP3: <?php echo $mp3; ?></li><?php endif; ?>
					<?php if($airbag != 'No aplica'): ?><li>Airbag para conductor y pasajero: <?php echo $airbag; ?></li><?php endif; ?>
					<?php if($usb != 'No aplica'): ?><li>Entrada USB: <?php echo $usb; ?></li><?php endif; ?>
				</ul>
				<h3>CONFORT Y CONVENIENCIA</h3>
				<ul>
					<?php if($dvd != 'No aplica'): ?><li>DVD: <?php echo $dvd; ?></li><?php endif; ?>
					<?php if($steering_wheel_height != 'No aplica'): ?><li>Regulación de altura del volante: <?php echo $steering_wheel_height; ?></li><?php endif; ?>
					<?php if($parking != 'No aplica'): ?><li>Sensor de parqueo: <?php echo $parking; ?></li><?php endif; ?>
					<?php if($electric_glasses != 'No aplica'): ?><li>Vidrios eléctricos: <?php echo $electric_glasses; ?></li><?php endif; ?>
					<?php if($folding_rear_seat != 'No aplica'): ?><li>Asiento trasero abatible: <?php echo $folding_rear_seat; ?></li><?php endif; ?>
					<?php if($remote_trunk != 'No aplica'): ?><li>Apertura remota de baúl: <?php echo $remote_trunk; ?></li><?php endif; ?>
					<?php if($electric_seats != 'No aplica'): ?><li>Asientos eléctricos: <?php echo $electric_seats; ?> </li><?php endif; ?>
				</ul>
				<h3>SEGURIDAD</h3>
				<ul>
					<?php if($front_fog_lights != 'No aplica'): ?><li>Faros antinieblas delanteros: <?php echo $front_fog_lights; ?></li><?php endif; ?>
					<?php if($lights_automatics != 'No aplica'): ?><li>Luces con regulación automática: <?php echo $lights_automatics; ?></li><?php endif; ?>
					<?php if($rear_fog_lights != 'No aplica'): ?><li>Faros antinieblas traseros: <?php echo $rear_fog_lights; ?> </li><?php endif; ?>
					<?php if($rear_defogger != 'No aplica'): ?><li> Desempañador trasero: <?php echo $rear_defogger; ?></li><?php endif; ?>
					<?php if($central_door_locking != 'No aplica'): ?><li> Cierre centralizado de puertas: <?php echo $central_door_locking; ?></li><?php endif; ?>
				</ul>
				<h6>Características principales</h6>
				<div class="row-old-table">
					<div class="tr">
						<div class="td">Marca</div>
						<div class="td"><?php if(is_array($brandTaxonomies)){foreach($brandTaxonomies as $brand){echo $brand->name;}} ?></div>
					</div>
					<div class="tr">
						<div class="td">Modelo</div>
						<div class="td"><?php echo $refer;?></div>
					</div>
					<div class="tr">
						<div class="td">Año</div>
						<div class="td"><?php echo $year;?></div>
					</div>
					<div class="tr">
						<div class="td">Color</div>
						<div class="td"><?php echo $color;?></div>
					</div>
					<div class="tr">
						<div class="td">Tipo de combustible</div>
						<div class="td"><?php echo $fuel_type;?></div>
					</div>
					<div class="tr">
						<div class="td">Puertas</div>
						<div class="td"><?php echo $doors;?></div>
					</div>
					<div class="tr">
						<div class="td">Transmisión</div>
						<div class="td"><?php echo $transmition['label'];?></div>
					</div>
					<div class="tr">
						<div class="td">Motor</div>
						<div class="td"><?php echo $motor;?></div>
					</div>
					<div class="tr">
						<div class="td">Tipo de Carrocería</div>
						<div class="td"><?php echo $body_type;?></div>
					</div>
					<div class="tr">
						<div class="td">Kilómetros</div>
						<div class="td"><?php echo number_format($km, 0, '.', '.');;?> km</div>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>

<?php 
		endif;
	get_footer(); 
?>