<?php get_header(); 



/**
 * 
 * Library Mobile Detect
 * @source 'library/includes' 'Mobile_Detect.php'
 * */
$detect = new Mobile_Detect;


/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/' 'section-feature-banner.php'
	 * 
	 */
	get_template_part( 'page-parts/sections/section-feacture', 'banner', array('class' => 'banner-models'));

/**
 * 
 * @source 'page-parts/sliders/' 'slider-models.php'
 * @since 1.0.0
 * @requires Boostrap
 * 
 */
	
get_template_part( 'page-parts/sliders/slider', 'models', [
	'postID' => $post->ID 
]);


?>

<?php if( function_exists( 'get_field' ) ):
	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_color'
		* 
		*/
		$colorSection = get_field( 'yokomotor_color' ); 
	if( $colorSection && 	$colorSection['enable_section'] ): ?>
		<section class="main-modelos-color full clear-fix relative">
			<div class="wrapper-main center">
				
				<h1><?php echo( !empty($colorSection['title']) ) ? $colorSection['title'] : the_title(); 
				echo(!empty($colorSection['subtitle'])) ? '<span>'.$colorSection['subtitle'].'</span>': null;?> </h1>
				<!-- Item Price -->
				<?php if( !empty($colorSection['price']) ): ?>
				<span class="price-color"><?php echo $colorSection['price']; ?></span>
				<?php endif; ?>
				<!-- End Item Price -->
				<?php if( $colorSection['models'] && count($colorSection['models']) > 0 ): $models = $colorSection['models']; ?>
					<section class="col-modelos-color relative">
						<div class="swiper swiper-car-color">
							<div class="swiper-wrapper">
								<?php foreach( $models as $model ): ?>
									<div class="swiper-slide">
									  <?php if( $model['img'] ): ?>
											<figure class="img-color">
												<img src="<?php echo esc_url($model['img']['url']); ?>" alt="<?php echo esc_attr($model['img']['title']); ?>"> 
											</figure>
										<?php endif; 
										if( !empty($model['description']) ): ?>
											<h4 class="name-color"><?php echo $model['description']; ?></h4>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>			
						<div thumbsSlider="" class="swiper swiper-thums-color">
							<div class="swiper-wrapper">
								<?php foreach( $models as $model ): ?>
										<div class="swiper-slide">
											<div class="bullet-color" style="background-color: <?php echo $model['color']; ?>;"></div>
										</div>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="next-color"></div>
						<div class="prev-color"></div>
					</section>
				<?php endif; ?>
				<!-- Item Legal -->
				<?php if( !empty($colorSection['text_legal']) ): ?>
                <div class="col-texto-legal">
				<p><?php echo $colorSection['text_legal']; ?></p>
                </div>
				<?php endif; ?>
				<!-- End Item Legal -->
				<div class="clr"></div>
				<?php if( $colorSection['buttons'] && count($colorSection['buttons']) > 0 ): $buttons = $colorSection['buttons']; ?>
					<div class="col-btns full">
						<?php foreach( $buttons as $button ): 
							switch ($button['select']) {
								case 'pdf': ?>
									<a href="<?php echo $button['pdf']['url']; ?>" class="btn-ficha" download>FICHA TÉCNICA</a>
								<?php break;
									case 'popup': ?>
									<a href="#modal-form-vehicles" data-toggle="modal" data-target="#modal-form-vehicles" class="btn-cotizar">COTIZAR</a>
									<?php	break;
								default:  ;?>
									<a href="<?php echo(site_url('repuestos')); ?>" target="_self" class="btn-cotizar">REPUESTOS</a>
								<?php	break;
							}?>
							
							<?php /* ?><a href="<?php echo($button['select'] != 'popup') ? $button['pdf']['url'] : '#modal-form-vehicles'?>" data-toggle="<?php echo($button['select'] != 'popup') ? null : 'modal'?>" data-target="<?php echo($button['select'] != 'popup') ? null : '#modal-form-vehicles'?>" target="<?php echo($button['select'] != 'popup') ? '_blank' : '_self'?>" class="btn-ficha" style="background-color: <?php echo $button['back_color']; ?>; color: <?php echo $button['text_color']; ?>;" title="<?php echo $button['pdf']['filename']?>"><?php echo $button['text']; ?></a><?php */ ?>
							
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</section>
  <?php endif;

	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_details'
		* 
		*/
		$detailSection = get_field( 'yokomotor_details' ); 
	if( $detailSection && $detailSection['enable_section'] ): $tabs = $detailSection['tabs'];?>
		<section class="main-features-specs full clear-fix">
			<div class="wrapper-main center">
				<div id="tabs-features-specs" class="features-specs-tabs">
					<?php if( $tabs && (isset($tabs) && count($tabs) > 0) ): ?>
						<ul class="title-tabs-line">
							<?php 
							foreach( $tabs as $key => $tab ): ?>
								<li><h3><a href="#features<?php echo $key; ?>"><?php echo($tab['title_tab']); ?></a></h3></li>
								<!-- <li><a href="#specs">Especificaciones</a></li> -->
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<?php foreach( $tabs as $key=>$tab ): 
							if( $tab['select_tabs'] != 'specifications'): ?>
							<!-- Item Características -->
							<div id="features<?php echo $key; ?>" class="content-features">
								<div class="wrapper-main-sm center">
									<?php if( !empty($tab['title_some_characteristics']) ) : ?>
										<h2><?php echo $tab['title_some_characteristics']; ?></h2>
									<?php endif; 
										if( !empty($tab['description_some_characteristics']) ) {
											echo $tab['description_some_characteristics']; 
										}; 
										if( $tab['description_icon'] && count($tab['description_icon']) > 0 ): $icons = $tab['description_icon']; ?>
										<div class="logos-caracteristicas px-3 text-center d-flex justify-content-around align-items-center">	
										<div class="accordion-detalles">
											<?php foreach( $icons as $icon ): ?>
												<?php if( $icon['title'] ): ?>
													<h4><?php echo $icon['title']; ?></h4>
												<?php endif; ?>						
												<div class="content-detalles-tabs">
												<?php if( $icon['description'] ): ?>
													<?php echo $icon['description']; ?>
												<?php endif; ?>	
												</div>
											<?php endforeach; ?>
										</div>
										</div>
										<?php endif?>
									<?php if( !empty($tab['title_characteristics']) ): ?>
										<h2><?php echo $tab['title_characteristics']; ?></h2>
									<?php endif;
									if( $tab['tabs_characteristics'] && (isset($tab['tabs_characteristics']) && count($tab['tabs_characteristics']) > 0) ): $tabCharacteristics = $tab['tabs_characteristics']; ?>
										<div id="tabs-disenos">
											<ul class="title-tabs-line title-tabs-small">
												<?php foreach( $tabCharacteristics as $key=> $tabCharacteristic): ?>
													<li><a href="#exterior<?php echo $key; ?>"><?php echo $tabCharacteristic['title']; ?></a></li>
												<?php endforeach; ?>
											</ul>
											<?php foreach( $tabCharacteristics as $key => $tabCharacteristic ): 
												if( $tabCharacteristic['select_content'] == 'slider' && (isset($tabCharacteristic['slider']) && count($tabCharacteristic['slider']) > 0 ) ): $sliders = $tabCharacteristic['slider']; ?>
													<!-- Item Exterior -->
													<div id="exterior<?php echo $key; ?>" class="content-exterior">
														<div class="swiper swiper-exterior">
															
															<div class="swiper-wrapper">
																	<?php foreach( $sliders as $key => $slider ): ?>
																		<div class="swiper-slide">
																			<figure>
																				<img src="<?php echo esc_url($slider['img']['url']); ?>" alt="<?php echo esc_attr($slider['img']['title']); ?>"> 
																			</figure>
																		</div>
																	
																		<?php endforeach; ?>
																</div>
															</div>
															<div class="prev-exterior button-prev prev-white"></div>
															<div class="swiper-pagination pagination-diseno exterior-pagination exterior-pagination-caracteristicas"></div>
															<div class="next-exterior button-next next-white"></div>
													</div>
												<?php else: 
												if( $tabCharacteristic['select_content'] == 'video' ): $video = $tabCharacteristic['video']; 
													if( $video && !empty($video) ): ?>
														<div id="exterior<?php echo $key; ?>" class="tab-video">
															<iframe src="https://www.youtube.com/embed/<?php echo $video; ?>?rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
														</div>
													<?php endif; 
												endif; ?>
											<?php endif; ?>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>	
						<?php else: ?>
							<!-- Item Especificaciones -->
							<div id="features<?php echo $key; ?>" class="content-specs">
								<div class="wrapper-main-sm center">
								<?php if( !empty($tab['title_specifications']) ): ?>
										<h2><?php echo $tab['title_specifications']; ?></h2>
									<?php endif; 
									if( $tab['download_pdf'] ): ?>
										<a href="<?php echo esc_url($tab['download_pdf']['url']); ?>" download target="_blank" class="download-icon" style="background: url(<?php echo esc_url($tab['icon']['url']); ?>)	no-repeat center left; background-size: 19px;"><?php echo $tab['title_brochure']; ?></a>
									<?php endif; 
									if( $tab['accordion'] && (isset($tab['accordion'])) && count($tab['accordion']) > 0 ): $accordions = $tab['accordion'];?>
										<div class="accordion-detalles">
											<?php foreach( $accordions as $accordion ): ?>
												<?php if( $accordion['title'] ): ?>
													<h4><?php echo $accordion['title']; ?></h4>
												<?php endif; ?>						
												<div class="content-detalles-tabs">
												<?php if( $accordion['description'] ): ?>
													<?php echo $accordion['description']; ?>
												<?php endif; ?>	
												</div>
											<?php endforeach; ?>
										</div>
									<?php endif; ?>
								</div>
							</div>	
						<?php endif; 
					endforeach;?>
				</div>
			</div>
		</section>
	<?php endif; 
	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_tour'
		* 
		*/
		$tourSection = get_field( 'yokomotor_tour' ); 
	if( $tourSection && $tourSection['enable_section'] ): 
		$url = !empty( $tourSection['link_tour'] ) ? $tourSection['link_tour'] : false; ?>
		<div class="col-360 full clear-fix">
			<a href="<?php echo($url['url']) ? esc_url( $url['url']) : 'javascript:;'; ?>" target="<?php echo( $url['target'] ) ? esc_html( $url['target'] ) : '_self'; ?>" class="btn-360"><?php echo $url['title']; ?></a>
		</div>
	<?php endif;
	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_feacture'
		* 
		*/
		$feactureSection = get_field( 'yokomotor_feacture' ); 
	if( $feactureSection && $feactureSection['enable_section'] ): $tabs = $feactureSection['tabs'];?>
		<section class="main-destacado-car full clear-fix">
			<div class="wrapper-main center">
				<h2>Destacados del <?php the_title(); ?></h2>
				<div id="tabs-destacados">
				<?php if( $tabs && (isset($tabs) && count($tabs) > 0) ): ?>
						<ul class="title-tabs-line title-tabs-small">
							<?php foreach( $tabs as $key => $tab ): ?>
								<li><a href="#diseno<?php echo $key; ?>"><?php echo($tab['title_tab']); ?></a></li>
							<?php endforeach; ?>
						</ul>
					<?php endif; ?>
					<?php foreach( $tabs as $key => $tab ): ?>
						<div id="diseno<?php echo $key; ?>" class="content-destacados">
							<div class="row row-xs">
								<?php if( $tab['articles'] && (isset($tab['articles']) && count($tab['articles']) > 0)  ): $articles = $tab['articles'];
									$classColumn = (count($articles) > 1 ) ? 'col-12 col-sm-6 col-lg-6 col-xl-6': 'col-12 col-sm-6 col-lg-12 col-xl-12'; 
									foreach( $articles as $article ): ?>
										<div class="<?php echo $classColumn; ?>">
												<article class="card-destacado">
													<figure>
														<img src="<?php echo esc_url($article['img']['url']); ?>" alt="<?php echo esc_attr($article['img']['title']); ?>">
													</figure>
													<?php if( !empty($article['title']) ): ?>
														<h3><?php echo $article['title']; ?></h3>
													<?php endif; ?>
													<?php if( !empty($article['description']) ): ?>
														<p><?php echo $article['description']; ?></p>
													<?php endif; ?>
												</article>
										</div>
									<?php endforeach;
								endif; ?>
								
							</div>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</section>
	<?php endif; 
endif;
/**
	 * * ***************
	* ACF Custom fields Vehicles (post Type)
	* ***************
	* @param ACF_fields 'yokomotor_warranty'
	* 
	*/
	$warrantySection = get_field( 'yokomotor_warranty' ); 
if( $warrantySection && $warrantySection['enable_section'] ): 
	$logo = $warrantySection['logo']; ?>
	<section class="main-garantias py-4">
		<div class="wrapper-main center">
			<h2 class="text-center"><?php echo $warrantySection['title']; ?></h2>
			<div class="row row-xs center-vertical">
				<?php if( $logo ): ?>
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6 center">
						<figure class="text-center">
							<img src="<?php echo esc_url($logo['url']); ?>" alt="<?php echo esc_attr($logo['title']); ?>" class="figure-img img-fluid">
						</figure>
					</div>
				<?php endif; ?>
				<?php if( $warrantySection['description'] && !empty($warrantySection['description'] ) ): ?>
					<div class="col-12 col-sm-6 col-lg-6 col-xl-6 center">
						<article>
							<?php echo $warrantySection['description']; ?>
						</article>		
					</div>
				<?php endif; ?>
			</div>
		</div>
	</section>


<?php endif; 

/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_apply'
		* 
*/
		   $applySection = get_field( 'yokomotor_apply' ); 
		if( $applySection && $applySection['enable_section'] ): $camiones = $applySection['camiones'];?>
			<section class="main-aplicaciones">
			<div class="wrapper-main center">
			<h2 class="text-center"><?php echo $applySection['titulo_aplicaciones']; ?></h2>
			<p><?php echo $applySection['descripcion_aplicaciones']; ?></p>
			<!-- Item Aplicaciones -->
			<?php if( $camiones && (isset($camiones) && count($camiones) > 0) ): ?>
				<div class="section-apply-flex">
							<?php 
							foreach( $camiones as $key => $camion ): ?>
								<div class="content">
							    <img src="<?php echo esc_url($camion['img_camion']['url']); ?>" alt="<?php echo esc_attr($camion['img_camion']['title']); ?>"> 
								
								<h3><?php echo($camion['nombre_camion']); ?></h3>
								</div>
							<?php endforeach; ?>
				</div>
					<?php endif; ?>
			</div>
			</section>
		<?php endif; 

/**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/buttons/button-rate' 'us.php'
	 * 
	 */
	get_template_part( 'page-parts/buttons/button-rate', 'us');

/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_plan'
		* 
		*/
		$planSection = get_field( 'yokomotor_plan', 'yokomotor_vehicles' ); 
		if( $planSection && $planSection['enable_section'] ): $url =!empty( $planSection['link'] ) ? $planSection['link'] : false;?>
			<section class="full-ball-planes clear-fix">
				<?php if( $planSection['title'] && !empty($planSection['title']) ): ?>
					<h2><?php echo $planSection['title']; ?></h2>
				<?php endif; ?>
				<a href="<?php echo($url['url']) ? esc_url( $url['url']) : 'javascript:;'; ?>" target="<?php echo( $url['target'] ) ? esc_html( $url['target'] ) : '_self'; ?>" class="btn-yokomotor"><?php echo $url['title']; ?></a>
			</section>
	<?php endif;?>

<?php 
	/**
		 * * ***************
		* @source ACF ARCHIVE PLUGIN
		* ACF Custom fields Vehicles (Option page)
		* ***************
		* @param ACF_fields 'yokomotor_form'
		* 
		*/
		$formVehicles = get_field( 'yokomotor_form', 'yokomotor_vehicles' ); 
	if( $formVehicles['choice_form'] && !empty($formVehicles['choice_form']) ): 
			$form = $formVehicles['choice_form'];?>
		<div id="modal-form-vehicles" class="modal animate__animated animate__fadeInDown">
			<div class="flex-lightbox">
				<section class="form-yokomotor lightbox-form full clear-fix">	
					<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
					<hr>
					<h1>Preguntar por este vehículo <span><?php the_title(); ?></span></h1>
					<?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); ?>
				</section>
			</div>
		</div>
	<?php endif; ?>

<?php 
  /**	
	 * 
	 * Get Section Banner
	 * @source 'page-parts/sections/section-banner-hino', 'connect'
	 * 
	 */
	$choicePage = get_field( 'yokomotor_hino_connect', 'yokomotor_vehicles' ); 
	get_template_part( 'page-parts/sections/section-banner-hino', 'connect', array('choicePage' => $choicePage)); ?>
<?php get_footer(); 

	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_color'
		* 
		*/
		$colorSection = get_field( 'yokomotor_color' ); 
if( $colorSection && 	$colorSection['enable_section'] ): 
	if( $detect->isMobile() && !$detect->isTablet() ): ?>
		<?php if( $colorSection['buttons'] && count($colorSection['buttons']) > 0 ): $buttons = $colorSection['buttons']; ?>
			<!-- Sticky solo para mobile -->
			<section class="sticky-xs-cotizacion animate__animated animate__fadeInUp animate__delay-5s">
				<?php 
				$blogs = get_current_blog_id(); 

				?>
					<ul>
						<?php if( $blogs == 2 ): switch_to_blog(2);
						 foreach( $buttons as $button ): 
							switch ($button['select']) {
								case 'pdf': ?>
								<li><a href="<?php echo $button['pdf']['url']; ?>" class="btn-ficha" download>FICHA TÉCNICA</a></li>
								<?php break;
									case 'popup': ?>
									<li><a href="#modal-form-vehicles" data-toggle="modal" data-target="#modal-form-vehicles" class="btn-cotizar">COTIZAR</a></li>
									<?php	break;
								default: ?>
									<li><a href="<?php echo(site_url('repuestos')); ?>" target="_self" class="btn-cotizar">REPUESTOS</a></li>
								<?php	break;
							}; restore_current_blog();?>
						<?php /* ?>	<li class="<?php echo($button['select'] != 'popup') ? 'icon-car' : 'icon-cotizador'?>"><a href="<?php echo($button['select'] != 'popup') ? $button['pdf']['url'] : 'modal-form-vehicles'?>" data-toggle="<?php echo($button['select'] != 'popup') ? null : 'modal'?>" data-target="<?php echo($button['select'] != 'popup') ? null : '#modal-form-vehicles'?>" target="<?php echo($button['select'] != 'popup') ? '_blank' : '_self'?>"><?php echo $button['text']; ?></a></li>	<?php */ ?>	
						<?php endforeach; ?>
						<?php elseif( $blogs == 3 ): switch_to_blog(3);?>
						<li><a href="#modal-form-vehicles" data-toggle="modal" data-target="#modal-form-vehicles" class="btn-cotizar">COTIZAR</a></li>
						<li><a href="<?php echo(site_url('repuestos')); ?>" target="_self" class="btn-cotizar">REPUESTOS</a></li>
						<?php else: restore_current_blog();
					endif; restore_current_blog(); ?>
					</ul>
			</section>
		<?php endif;
	endif;
endif; ?>
