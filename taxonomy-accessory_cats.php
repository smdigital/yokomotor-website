<?php get_header(); 
/**
 * 
 * Library Mobile Detect
 * @source 'library/includes' 'Mobile_Detect.php'
 * */
$detect = new Mobile_Detect;

$customType = 'yokomotor_accessory';

	/**
	 * 
	 * Get Queried Object Context
	 * 
	 */
	$context = get_queried_object();
	$acfContext = "{$context->taxonomy}_{$context->term_id}";
if( function_exists( 'get_field' ) ): 
	/**
	 * * ***************
	* ACF Custom fields Accesories (Taxonomy)
	* ***************
	* @param ACF_fields 'yokomotor_color'
	* 
	*/
	$bannerSection = get_field( 'yokomotor_thumbnail', $acfContext ); 
	if( ($bannerSection && $bannerSection['enable_section']) && ($bannerSection['image_banner_mobile']['url'] || $bannerSection['image_banner']['url']) ): ?>
	<section class="banner-accesorios-marca full clear-fix">
		<figure>
			<img class="" src="<?php echo( $detect->isMobile() && !$detect->isTablet() ) ? $bannerSection['image_banner_mobile']['url'] : $bannerSection['image_banner']['url'] ; ?>" alt="Banner accesorios"> 
		</figure>
	</section>
	<?php endif; 
endif; 
	/**
	 * * ***************
	* ACF Custom fields Accesories (Taxonomy)
	* ***************
	* @param ACF_fields 'yokomor_accesories'
	* 
	*/
	$sectionAccesories = get_field( 'yokomor_accesories', $acfContext ); 

if( ($sectionAccesories && $sectionAccesories['enable_section']) && (isset($sectionAccesories) && count($sectionAccesories['items']) > 0) ): 
	$items = $sectionAccesories['items'];
	$formAccesories = get_field( 'yokomotor_form', $acfContext ); 
?>
	<section class="main-accesorios-marcas full clear-fix">
		<div class="wrapper-main center">
			<hr>
			<h2>Repuestos</h2>
			<h1><?php single_cat_title(); ?></h1>
			<div class="clr"></div>
				<div class="row-card-5">
					<?php foreach( $items as $item ):  	
						$image = $item['image']; ?>
						<div class="col-cards">
							<article class="card-marca-accesorios">
							<?php if ( $image ): ?>
									<figure>
										<a href="modal-form-accesories" data-toggle="modal" class="">
											<img src="<?php echo esc_url($image['url']); ?>" alt="">
										</a>
									</figure>
								<?php endif; ?>
							<?php if ($item['title']): ?>	
							   <h6><?php echo $item['title']; ?></h6>
							<?php endif; ?>
                             <?php if ($item['price']): ?>
								<p><?php echo $item['price']; ?></p>
							<?php endif; ?>
								<?php if( $formAccesories['choice_form'] && !empty($formAccesories['choice_form']) ): ?>
									<div class="figcaption">
										<a href="modal-form-accesories" data-toggle="modal"  class="btn-arrow-light">Adquirir</a>
									</div>
								<?php endif; ?>
							</article>
						</div>
					<?php endforeach; ?>		
				</div>
		</div>	
	</section>
	<!-- Lightbox Preguntar accesorios -->
	<?php if( $formAccesories['choice_form'] && !empty($formAccesories['choice_form']) ): 
				$form = $formAccesories['choice_form'];?>
		<div id="modal-form-accesories" class="modal animate__animated animate__fadeInDown">
			<div class="flex-lightbox">
				<section class="form-yokomotor lightbox-form full clear-fix">	
					<a href="" class="cerrar" data-dismiss="modal">Cerrar</a>
					<hr>
					<h1>Preguntar por el repuesto <span></span></h1>
					<?php echo do_shortcode('[contact-form-7 id="'.$form->ID.'" title="'.$form->post_title.'"]'); ?>
				</section>
			</div>
		</div>
	<?php endif;
endif; ?>
<?php get_footer(); ?>