<?php get_header(); 
  /**
     * 
     * Get Queried Object Context
     * 
     */
    $context = get_queried_object();
?>
<div class="main-boutique boutique-ropa full clear-fix">
<?php 
	/**
	 * Get terms boutique_cats and Post type name
	 */
	$boutiqueTerm = 'boutique_cats';

	if( taxonomy_exists( $boutiqueTerm ) && function_exists( 'get_field' ) ) : ?>
		<section class="row-boutique-categorias full clear-fix" style="border: none; padding: 5px 0 40px 0;">
			<div class="wrapper-main center">
				<h2>nuestras categorías</h2>

				<?php	$boutiqueTerms = get_terms( array (
							'taxonomy' => $boutiqueTerm,
							'hide_empty' => true, 
							'parent' => 0,
							'fields' => 'all',
					) ); 

					
					if( ($boutiqueTerms && !empty($boutiqueTerms)) && (is_array( $boutiqueTerms ) && count( $boutiqueTerms ) > 0) ) : 
						foreach( $boutiqueTerms as $term ): 
							$fields = get_field( 'yokomotor_icons', "{$boutiqueTerm}_{$term->term_id}" ); ?>
							<div class="icon-categoria">
								<a href="<?php echo get_term_link( $term ); ?>">
									<?php if( $fields['icon'] ): $img = $fields['icon']; ?>
										<i>
											<img src="<?php echo esc_url($img['url']); ?>" alt="">
										</i>
									<?php endif; ?>
									<h6><?php echo $term->name; ?></h6>
								</a>
							</div>
						<?php endforeach; 
					endif; ?>
				</div>
			</section>
	<?php endif; ?>
	<div class="wrapper-main center">
		<h1>Boutique <?php echo $context->name; ?></h1>			
	</div>	
	<div class="clr"></div>
	<section class="main-boutique-ropa full clear-fix">
		<div class="wrapper-main center">
				<?php echo do_shortcode('[ajax_load_more id="1078666071" paging="true" paging_controls="true" paging_show_at_most="6" css_classes="" theme_repeater="boutique-cats.php" post_type="yokomotor_boutique" taxonomy="boutique_cats" taxonomy_terms="'. $context->slug.'" scroll="false" posts_per_page="6" progress_bar="true" progress_bar_color="4b3126" transition_container_classes="row row-xs" images_loaded="true" button_label="Ver más" button_loading_label="Cargando..." no_results_text="<p><strong>No se encontro resultados.</strong></p>]', false);?>
			<div class="clr"></div>
		<!-- Pagination por defecto wordpress -->
			<!--<nav class="pagination">
				<ul class="page-numbers">
					<li>
						<span aria-current="page" class="page-numbers current">1</span>
					</li>
					<li>
						<a href="">2</a>
					</li>
					<li>
						<a href="">3</a>
					</li>
				</ul>
			</nav> -->
		</div>
	</section>
	
</div>



<?php 

get_footer(); ?>

