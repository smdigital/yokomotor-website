<?php get_header(); 
	/**
	 * 
	 * Library Mobile Detect
	 * @source 'library/includes' 'Mobile_Detect.php'
	 * */
	$detect = new Mobile_Detect;
	$context = get_queried_object();
	$postsUsed = get_posts( array('post_type' => 'yokomotor_used')); 
	$used_tax = 'used_cats';
	$city_tax = 'city_cats';
	$brand_tax = 'brand_cats';
	$models = [];	
	$usedTaxonomies = get_terms(
		array(
			'taxonomy'		=>	$used_tax,
			'hide_empty'	=>	false
		)
	);
	$cityTaxonomies = get_terms(
		array(
			'taxonomy'		=>	$city_tax,
			'hide_empty'	=>	false
		)
	);
	$brandTaxonomies = get_terms(
		array(
			'taxonomy'		=>	$brand_tax,
			'hide_empty'	=>	false
		)
	);
	if(function_exists('get_field')):
		/**
		 * * ***************
		* ACF Custom fields Usados (Taxonomia)
		* ***************
		* @param ACF_fields 'yokomotor_banner'
			* @param ACF_subfields 'enable_section'
			* @param ACF_subfields 'banners' - Repeater
				* @param ACF_subfields 'image'
				* @param ACF_subfields 'image_xs'
				* @param ACF_subfields 'title'
				* @param ACF_subfields 'model'
				* @param ACF_subfields 'km'
				* @param ACF_subfields 'url'
		* 
		*/
		foreach($postsUsed as $item){
			$model = get_field('year', $item->ID);
			if(!in_array($model, $models)){
				array_push($models, $model);
				rsort($models);
			}
		}
		$termName	= $used_tax.'_'.$context->term_id;
		$seccionBanner	= get_field('yokomotor_banner', $termName);	
		if($seccionBanner && $seccionBanner['enable_section']):
?>
		<section class="banner-home banner-usados clear-fix">
			<div class="swiper swiper-banner-home">
				<div class="swiper-wrapper">
				<?php 
					if($seccionBanner['banners'] && count($seccionBanner['banners']) > 0 && is_array($seccionBanner)):
						foreach($seccionBanner['banners'] as $banner):	
							$image		=	isset($banner['image']) && !empty($banner['image']) ? $banner['image'] : false;
							$imageXs	=	isset($banner['image_xs']) && !empty($banner['image_xs']) ? $banner['image_xs'] : false;
							$title		=	!empty($banner['title']) ? $banner['title'] : false;
							$url		=	!empty($banner['url']) ? $banner['url'] : false;
							$model		=	!empty($banner['model']) ? $banner['model'] : false;
							$km			=	!empty($banner['km']) ? $banner['km'] : false;
				?>
					<div class="swiper-slide">		
						<figure>
							<img src="<?php echo ($detect->isMobile() && !$detect->isTablet()) ? esc_url($imageXs['url']) : esc_url($image['url']); ?>" alt="<?php echo ($detect->isMobile() && !$detect->isTablet()) ? esc_attr($imageXs['title']) : esc_attr($image['title']); ?>"> 		
						</figure>
						<div class="banner-usados-all">
							<div class="details-banner-usados">				
								<h1><?php echo $title;?></h1>
								<h4><?php echo $model." | ".number_format($km, 0, '.', '.')."KM";?></h4>
								<a href="<?php echo $url;?>" class="btn-yokomotor transparent">VER MÁS</a>
							</div>
						</div>
					</div>	
				<?php 
						endforeach; 
					endif; 
				?>
				</div>
			</div>
			<div class="next-banner button-next next-white"></div>
			<div class="prev-banner button-prev prev-white"></div>
			<div class="swiper-pagination banner-pagination"></div>
		</section>
<?php 
		endif; 
	endif;
?>
<section class="main-cart-all full clear-fix">
	<div class="wrapper-main center">
		<div class="row-filter-cart-all full clear-fix">
			<!-- Content Filtro  Desktop -->
			<div class="filter-cart-left">
				<input type='hidden' id="taxName" value="<?php echo $context->slug;;?>">
				<div id="tabs-orderby" class="orderby-filter-tabs">
					<h3>ORDENAR POR</h3>
					<div class="content-tabs-filter">
						<ul class="items-orderby order-filter">
							<li data-filter="date" data-sort="DESC"><a href="javascript:;">Más nuevo</a></li>
							<li data-filter="price" data-sort="ASC"><a href="javascript:;">Menor precio</a></li>
							<li data-filter="price" data-sort="DESC"><a href="javascript:;">Mayor precio</a></li>
						</ul>
					</div>
				</div>
				<h3>FILTRAR POR</h3>
				<div id="tabs-filter-xl" class="tabs-filter-all">
					<!-- Filtro Ubicación -->
					<h4>Ubicación</h4>
					<div class="info-tabs-filter">
						<ul class="items-orderby city-filter">
							<?php foreach($cityTaxonomies as $city):?>
								<li data-filter="<?php echo $city->slug; ?>" ><a href="javascript:;"><?php echo $city->name; ?> <span><?php echo $city->count; ?></span></a></li>
							<?php endforeach; ?>
						</ul> 
					</div>

					<!-- Filtro Marca -->
					<h4>Marca</h4>
					<div class="info-tabs-filter">
						<select name="" id="" class="brand-filter">
							<option value="">Selecciona</option>
							<?php foreach($brandTaxonomies as $brand): ?>
								<option value="<?php echo $brand->slug; ?>"><?php echo $brand->name; ?></option> 
							<?php endforeach; ?>
						</select>
					</div>

					<!-- Filtro Modelo -->
					<h4>Modelo</h4>
					<div class="info-tabs-filter">
						<select name="" id="" class="year-filter">
							<option value="">Selecciona</option>
							<?php foreach($models as $model):?>
								<option value="<?php echo $model;?>"><?php echo $model;?></option>
							<?php endforeach; ?>
						</select>
					</div>

					<!-- Filtro Precio -->
					<h4>Precio</h4>
					<div class="info-tabs-filter">
						<div class="row-range">
							<div class="range-slide">
								<h6 class="val-min" id="val-min" ><span id="initprice">Desde </span>$15.000.000</h6>
								<h6 class="val-max" id="val-max"><span id="finalprice">Hasta </span>$50.000.000</h6>
								<div class="slider-range" id="slider-used"></div>
							</div>								
							<input type="hidden" class="val-min" id="val-min-input" placeholder="0">
							<input type="hidden" class="val-max" id="val-max-input" placeholder="0">
							<div class="clr"></div>
						</div>
					</div>

					<!-- Filtro Kilometraje -->
					<h4>Kilometraje</h4>
					<div class="info-tabs-filter">
						<select name="" id="" class="km-filter">
							<option value="">Selecciona</option>
							<option value="0,10000">0 - 10.000 KM</option>
							<option value="10000,50000">10.000 - 50.000 KM</option>
							<option value="50000,100000">50.000 - 100.000 KM</option>
							<option value="100000,1000000">Mayor a 100.000 KM</option>
						</select>
					</div>

					<!-- Filtro Transmisión -->
					<h4>Transmisión</h4>					
					<div class="info-tabs-filter">
						<ul class="items-orderby transmition-filter">
							<li data-filter="mecanica"><a href="javascript:;"">Mecánica</a></li>
							<li data-filter="automatica"><a href="javascript:;"">Automática</a></li>
						</ul>
					</div>
				</div>
				<a href="javascript:;"  class="btn-aplicar">Buscar</a>
				<a href="javascript:;" class="btn-clear">Limpiar Filtro</a>				
			</div>

			<!-- Conten Cart Right -->
			<div class="wrap-cart-right">
					<?php echo ($context->slug != 'ver-todos') 
						?
							do_shortcode('[ajax_load_more id="1738309920" theme_repeater="template-used.php" post_type="yokomotor_used" filters="True" posts_per_page="6" taxonomy="used_cats" taxonomy_terms="'.$context->slug.'" taxonomy_operator="IN" progress_bar="false" images_loaded="false" meta_key="disponibility" meta_value="Si" meta_compare="IN"  meta_type="CHAR" button_label="Ver más vehículos" transition_container_classes="row row-xs" button_loading_label="Cargando..." no_results_text="<h4>No hay vehículos disponibles en este momento</h4>"]')
						:
							do_shortcode('[ajax_load_more id="1738309920" theme_repeater="template-used.php" post_type="yokomotor_used" filters="true" posts_per_page="6" progress_bar="false" images_loaded="false" meta_key="disponibility" meta_value="Si" meta_compare="IN"  meta_type="CHAR" button_label="Ver más vehículos" transition_container_classes="row row-xs" button_loading_label="Cargando..." no_results_text="<h4>No hay vehículos disponibles en este momento</h4>"]');	
					?>

              <!--Seccion Nuestras Categorias Usados!-->

			  <?php  get_template_part('page-parts/sections/section-categories-used')?> 




			  <!--Fin Seccion Nuestras Categorias Usados!-->


		</div>
		</div>	
		
	</div>
</section>

<!-- Sticky / Filtro para Mobile -->
<section class="main-filter-usados-xs full clear-fix">
	<div class="wrapper-main center">
		<div class="sticky-bar-filter-red">
			<ul>
				<li id="btn-orderby-xs" class="icon-orderby">
					<a href="javascript:;">Ordenar</a>
				</li>
				<li id="btn-filter-all-xs" class="icon-filter">
					<a href="javascript:;">Filtrar</a>
				</li>
			</ul>
		</div>

		<!-- Filtro Ordenar -->
		<div class="content-tabs-filter modal-orderby-xs">
			<div class="cerrar">x</div>
			<ul class="items-orderby order-filter">
				<li data-filter="date" data-sort="DESC"><a href="javascript:;">Más nuevo</a></li>
				<li data-filter="price" data-sort="ASC"><a href="javascript:;">Menor precio</a></li>
				<li data-filter="price" data-sort="DESC"><a href="javascript:;">Mayor precio</a></li>
			</ul>
			<a href="javascript:;"  class="btn-aplicar">Buscar</a>
		</div>

		<!-- Filtro general accordion completo -->
		<div class="tabs-filter-all modal-filter-all-xs content-tabs-filter">
			<div class="cerrar">x</div>
			<div id="tabs-filter-xs">
				<!-- Filtro Ubicación -->
				<h4>Ubicación</h4>
				<div class="info-tabs-filter">
					<ul class="items-orderby city-filter">
						<?php foreach($cityTaxonomies as $city):?>
							<li data-filter="<?php echo $city->slug; ?>" ><a href="javascript:;"><?php echo $city->name; ?> <span><?php echo $city->count; ?></span></a></li>
						<?php endforeach; ?>
					</ul>
				</div>

				<!-- Filtro Marca -->
				<h4>Marca</h4>
				<div class="info-tabs-filter">
					<select name="" id="" class="brand-filter">
						<option value="">Selecciona</option>
						<?php foreach($brandTaxonomies as $brand): ?>
								<option value="<?php echo $brand->slug; ?>"><?php echo $brand->name; ?></option> 
							<?php endforeach; ?>
					</select>
				</div>

				<!-- Filtro Modelo -->
				<h4>Modelo</h4>
				<div class="info-tabs-filter">
					<select name="" id="" class="year-filter">
						<option value="">Selecciona</option>
						<?php foreach($models as $model):?>
							<option value="<?php echo $model;?>"><?php echo $model;?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<!-- Filtro Precio -->
				<h4>Precio</h4>
				<div class="info-tabs-filter">
					<div class="row-range">
						<div class="range-slide">
							<h6 class="val-min" id="val-min-xs" >$500.000</h6>
							<h6 class="val-max" id="val-max-xs">$50.000.000</h6>
							<div class="slider-range"></div>
						</div>								
						<input type="hidden" class="val-min" id="val-min-input-xs" placeholder="$0">
						<input type="hidden" class="val-max" id="val-max-input-xs" placeholder="$0">
						<div class="clr"></div>
					</div>
				</div>

				<!-- Filtro Kilometraje -->
				<h4>Kilometraje</h4>
				<div class="info-tabs-filter">
					<select name="" id="" class="km-filter">
						<option value="">Selecciona</option>
						<option value="0,10000">0 - 10.000 KM</option>
						<option value="10000,50000">10.000 - 50.000 KM</option>
						<option value="50000,100000">50.000 - 100.000 KM</option>
						<option value="100000,1000000">Mayor a 100.000 KM</option>
					</select>
				</div>

				<!-- Filtro Transmisión -->
				<h4>Transmisión</h4>					
				<div class="info-tabs-filter">
					<ul class="items-orderby transmition-filter">
						<li><a href="javascript:;"">Mecánica </a></li>
						<li><a href="javascript:;"">Automática </a></li>
					</ul>
				</div>
			</div>
			<a href="javascript:;"  class="btn-aplicar">Buscar</a>
			<a href="javascript:;" class="btn-clear" id="btn-clear">Limpiar Filtro</a>	
		</div>
	</div>
</section>

<?php get_footer(); ?>