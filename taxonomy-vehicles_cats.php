<?php
/*
 * CUSTOM POST TYPE TAXONOMY TEMPLATE
 *
 * This is the custom post type taxonomy template. If you edit the custom taxonomy name,
 * you've got to change the name of this template to reflect that name change.
 *
 * For Example, if your custom taxonomy is called "register_taxonomy('shoes')",
 * then your template name should be taxonomy-shoes.php
 *
 * For more info: http://codex.wordpress.org/Post_Type_Templates#Displaying_Custom_Taxonomies
*/
?>

<?php get_header(); 

    /**
     * 
     * Get Queried Object Context
     * 
     */
    $context = get_queried_object();
	
		$acfContext = "{$context->taxonomy}_{$context->term_id}";
  /**
	 * 
	 * @source 'page-parts/sliders/' 'slider-models.php'
	 * @since 1.0.0
	 * @requires Boostrap
	 * 
	 */
		
	get_template_part( 'page-parts/sliders/slider', 'models', [
		'categoryID' => $context->term_id
	]);
		
?>

<?php if( function_exists( 'get_field' ) ):
	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_color'
		* 
		*/
		$colorSection = get_field( 'yokomotor_color', $acfContext ); 
	if( $colorSection && 	$colorSection['enable_section'] ): ?>
		<section class="main-modelos-color full clear-fix relative">
			<div class="wrapper-main center">
				
				<h1><?php echo( !empty($colorSection['title']) ) ? $colorSection['title'] : $context->name; 
				echo(!empty($colorSection['subtitle'])) ? '<span>'.$colorSection['subtitle'].'</span>': null;?> </h1>
				<?php if( $colorSection['models'] && count($colorSection['models']) > 0 ): $models = $colorSection['models']; ?>
					<section class="col-modelos-color relative">
						<div class="swiper swiper-car-color">
							<div class="swiper-wrapper">
								<?php foreach( $models as $model ): ?>
									<div class="swiper-slide">
									  <?php if( $model['img'] ): ?>
											<figure class="img-color">
												<img src="<?php echo esc_url($model['img']['url']); ?>" alt="<?php echo esc_attr($model['img']['title']); ?>"> 
											</figure>
										<?php endif; 
										if( !empty($model['description']) ): ?>
											<h4 class="name-color"><?php echo $model['description']; ?></h4>
										<?php endif; ?>
									</div>
								<?php endforeach; ?>
							</div>
						</div>			
						<div thumbsSlider="" class="swiper swiper-thums-color">
							<div class="swiper-wrapper">
								<?php foreach( $models as $model ): ?>
										<div class="swiper-slide">
											<div class="bullet-color" style="background-color: <?php echo $model['color']; ?>;"></div>
										</div>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="next-color"></div>
						<div class="prev-color"></div>
					</section>
				<?php endif; ?>
				<div class="clr"></div>
				<?php if( $colorSection['buttons'] && count($colorSection['buttons']) > 0 ): $buttons = $colorSection['buttons']; ?>
					<div class="col-btns full">
						<?php foreach( $buttons as $button ): ?>
							<a href="<?php echo($button['select'] != 'popup') ? $button['pdf']['url'] : 'javacript:;'?>" target="<?php echo($button['select'] != 'popup') ? '_blank' : '_self'?>" class="btn-cotizar" style="background-color: <?php echo $button['back_color']; ?>; color: <?php echo $button['text_color']; ?>;" title="<?php echo $button['pdf']['filename']?>"><?php echo $button['text']; ?></a>
							<!-- <a href="javascript:;" class="btn-ficha">FICHA TÉCNICA</a> -->
						<?php endforeach; ?>
					</div>
				<?php endif; ?>
			</div>
		</section>
  <?php endif;
endif; ?>
<?php if( function_exists( 'get_field' ) ):
	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_color'
		* 
		*/
		$iconsSection = get_field( 'yokomotor_icons', $acfContext ); 
	if( $iconsSection && 	($iconsSection['enable_section'] && count($iconsSection['icons']) > 0) ): $icons = $iconsSection['icons']; ?>
		<section class="main-widgets-features full clear-fix">
			<?php foreach( $icons as $icon ): ?> 
				<article class="icon-features">
					<?php if( $icon['img']['url'] ): ?>
						<figure>
							<img src="<?php echo esc_url($icon['img']['url']); ?>" alt="<?php echo esc_url($icon['img']['title']); ?>">
						</figure>
					<?php endif; 
					if( $icon['title'] ): ?>
						<figcaption><?php echo $icon['title']; ?></figcaption>
					<?php endif; ?>
				</article>
			<?php endforeach; ?>
		</section>
	<?php endif; 
	/**
		 * * ***************
		* ACF Custom fields Vehicles (post Type)
		* ***************
		* @param ACF_fields 'yokomotor_plan'
		* 
		*/
		$planSection = get_field( 'yokomotor_plan', 'yokomotor_vehicles' ); 
		if( $planSection && $planSection['enable_section'] ): $url =!empty( $planSection['link'] ) ? $planSection['link'] : false;?>
			<section class="full-ball-planes clear-fix">
				<?php if( $planSection['title'] && !empty($planSection['title']) ): ?>
					<h2><?php echo $planSection['title']; ?></h2>
				<?php endif; ?>
				<a href="<?php echo($url['url']) ? esc_url( $url['url']) : 'javascript:;'; ?>" target="<?php echo( $url['target'] ) ? esc_html( $url['target'] ) : '_self'; ?>" class="btn-yokomotor"><?php echo $url['title']; ?></a>
			</section>
	<?php endif;
endif; ?>
<?php get_footer(); ?>

